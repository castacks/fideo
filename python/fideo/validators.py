
import logging
import math
import copy
from collections import OrderedDict

import numpy as np
import theano
import theano.tensor as T
from funcy import izip_values

from fideo.util import make_shared


log = logging.getLogger(__name__)


class ChunkedValidator(object):

    def __init__(self,
                 valid_inputs,
                 valid_outputs):

        self.valid_inputs = valid_inputs
        self.valid_outputs = valid_outputs

        self.shared_vars = {name: make_shared(var.ndim,
                                              var.dtype,
                                              var.name+'_shared')
                            for name, var in self.valid_inputs.items()}

        givens = dict(izip_values(self.valid_inputs, self.shared_vars))

        self.valid_fn = theano.function(inputs=[],
                                        outputs=self.valid_outputs.values(),
                                        givens=givens)


    def valid_chunk(self, chunk):
        log.info('validating chunk with size {}'.format(len(chunk)))

        for sname in self.shared_vars:
            if sname not in chunk.dtype.names:
                raise ValueError('missing variable in valid chunk: {}'.format(sname))

        for name in chunk.dtype.names:
            self.shared_vars[name].set_value(chunk[name])

        # [expr1 out, expr2 out, ...]
        out_lst = map(np.asarray, self.valid_fn())

        # TODO float32 assumption
        dtype = [(name, 'float32', out_lst[i].shape[1:])
                 for i,name in enumerate(self.valid_outputs)]
        out_arr = np.array(zip(*out_lst), dtype=dtype)
        #import ipdb; ipdb.set_trace()

        #dtype = [(name, 'float32') for name in self.valid_outputs.keys()]
        #out_arr = np.array(tuple(out_lst), dtype=dtype)

        return out_arr
