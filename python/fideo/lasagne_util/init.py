import numpy as np

import lasagne
from lasagne.utils import floatX


class Prelu(lasagne.init.Initializer):
    """ He's "prelu" initialization.
    Note: HeInit is now in Lasagne.
    """

    def sample(self, shape):
        # eg k^2 for conv2d
        receptive_field_size = np.prod(shape[2:])
        c = shape[1] # input channels
        nl = c*receptive_field_size
        std = np.sqrt(2.0/(nl))
        return floatX(np.random.normal(0, std, size=shape))


class Bilinear(lasagne.init.Initializer):

    def __init__(self, normalize=False):
        self.normalize = normalize

    def sample(self, shape):
        # from caffe docs
        #layer {
        #  name: "upsample", type: "Deconvolution"
        #  convolution_param {
        #    kernel_size: {{2 * factor - factor % 2}} stride: {{factor}}
        #    num_output: {{C}} group: {{C}}
        #    pad: {{ceil((factor - 1) / 2.)}}
        #    weight_filler: { type: "bilinear" } bias_term: false
        #  }
        #  param { lr_mult: 0 decay_mult: 0 }
        #}
        # according to caffe docs, bilinear kernel should be same as
        # out = skimage.transform.rescale(img, factor, mode='constant', cval=0)
        # http://warmspringwinds.github.io/tensorflow/tf-slim/2016/11/22/upsampling-and-image-segmentation-with-tensorflow-and-tf-slim/
        # if factor: stride of upsampling

        """ below adapted from FCN caffe code """

        # shape: output, input, height, width
        # inverse of eqn
        # kernel size =  2*factor - factor%2
        k = shape[2]
        factor = float((k + 1) // 2)
        if shape[2] % 2 == 1:
            center = factor - 1.0
        else:
            center = factor - 0.5
        og = np.ogrid[:shape[2], :shape[3]]
        kernel2d = (1 - abs(og[0] - center) / factor) * \
                   (1 - abs(og[1] - center) / factor)
        kernel2d = kernel2d.astype(np.float32)
        if self.normalize:
            # TODO why 4? (something/2)**2 ?
            kernel2d /= (kernel2d.sum()/4.)
        kernel4d = np.zeros(shape, dtype=np.float32)
        # TODO what if input > output?
        for n in xrange(shape[0]):
            if n < shape[1]:
                kernel4d[n, n] = kernel2d
        return kernel4d


class ConvIdentity(lasagne.init.Initializer):
    def sample(self, shape):
        kernel4d = np.zeros(shape, 'f4')
        center = tuple([i//2 for i in kernel4d.shape[2:]])
        for n in xrange(shape[0]):
            if n < shape[1]:
                kernel4d[(n, n) + center] = 1.
        return kernel4d


class NINIdentity(lasagne.init.Initializer):
    """ TODO should be good for Dense too? """
    def sample(self, shape):
        w = np.zeros(shape, 'f4')
        for n in xrange(shape[1]):
            if n < shape[0]:
                w[n, n] = 1.
        return w


if __name__ == "__main__":
    #kernel = Bilinear()
    #print(kernel.sample((1, 1, 3, 3)))
    #print(kernel.sample((1, 1, 4, 4)))
    #print(kernel.sample((1, 1, 5, 5)))

    #kerneln = Bilinear(normalize=True)
    #print(kerneln.sample((1, 1, 3, 3)))
    #print(kerneln.sample((1, 1, 4, 4)))
    #print(kerneln.sample((1, 1, 5, 5)))

    # kernel = ConvIdentity()
    #print(kernel.sample((4, 3, 3, 3)))
    # print(kernel.sample((4, 4, 4, 4)))

    print(NINIdentity().sample((4, 3)))
    print(NINIdentity().sample((3, 4)))
