"""
Some theano expressions.
"""

import theano
import theano.tensor as T


def rms(x, axis=None, epsilon=1e-12):
    return T.sqrt(T.mean(T.sqr(x), axis=axis) + epsilon)


def line_abc_sqr_error(yhat, y):
    """
    Let ax + by + c = 0.
    Let abnorm = l2norm([a,b])
    then a = a/norm, b = b/norm
    TODO c = c/norm?
    """
    ac = yhat[:,0]
    bc = yhat[:,1]
    cc = yhat[:,2]
    return ((ac-y[:,0])**2 + (bc-y[:,1])**2 + ((cc-y[:,2])/227.)**2).mean()


def line_norm_abc_sqr_error(yhat, y):
    """
    Let ax + by + c = 0.
    Let abnorm = l2norm([a,b])
    then a = a/norm, b = b/norm
    TODO c = c/norm?
    """
    ac = yhat[:,0]
    bc = yhat[:,1]
    cc = yhat[:,2]
    abnorm = T.sqrt(ac**2 + bc**2) + 1e-12
    ac2 = ac/abnorm
    bc2 = bc/abnorm
    return ((ac2-y[:,0])**2 + (bc2-y[:,1])**2 + ((cc-y[:,2])/227.)**2).mean()
    #return T.sqrt((yhat-y)**2).mean()
    #acoeff2 = acoeff/abnorm
    #bcoeff2 = bcoeff/abnorm
    #ccoeff = yhat[:,2]
    ##err = (acoeff2-y[:,0])**2 + (bcoeff2-y[:,1])**2 + (ccoeff-y[:,2])**2
    #err = ((acoeff2-y[:,0])**2 + (bcoeff2-y[:,1])**2)
    #return (T.sqrt(err+1e-12)).mean()
    #return T.sqrt((yhat-y)**2).mean()
    #abnorm = T.sqrt(acoeff**2 + bcoeff**2) + 1e-12
    #acoeff2 = acoeff/abnorm
    #bcoeff2 = bcoeff/abnorm
    #ccoeff = yhat[:,2]
    ##err = (acoeff2-y[:,0])**2 + (bcoeff2-y[:,1])**2 + (ccoeff-y[:,2])**2
    #err = ((acoeff2-y[:,0])**2 + (bcoeff2-y[:,1])**2)
    #return (T.sqrt(err+1e-12)).mean()


def log_softmax(x):
    xdev = x - x.max(1, keepdims=True)
    return xdev - T.log(T.sum(T.exp(xdev), axis=1, keepdims=True))


def categorical_crossentropy_logdomain_onehot(log_predictions, targets):
    return -T.sum(targets * log_predictions, axis=1)
