"""

Some Theano expressions for spatially 2D data.

Equivalent ones may be already in theano/lasagne, as they're
continually evolving; check regularly.

"""

import numpy as np
import theano
import theano.tensor as T


def logspace_binary_logloss(linear_out, y_target):
    """
    instead of binary crossentropy we do equivalent expression
    that is more stable because of theano quirks.

    :param: linear_out is output of a linear expression
    :param: y_target is binary target. same shape as linear_out
    """
    #ce = T.nnet.binary_crossentropy(T.nnet.sigmoid(linear_out), y_target).mean()
    eltwise_ce = -y_target*linear_out + T.nnet.softplus(linear_out)
    ce = eltwise_ce.mean()
    return T.cast(ce, 'float32')


def masked_logspace_binary_logloss(linear_out, y_target):
    """
    NOTE. mask for y is indicated with negative values (not 0, as usual).
    """

    y_target_flat = y_target.flatten()
    mask = (1-T.lt(y_target_flat, 0.)).nonzero()

    y_target_flat_masked = y_target_flat[mask]
    linear_out_flat_masked = linear_out.flatten()[mask]
    loss = logspace_binary_logloss(linear_out_flat_masked, y_target_flat_masked)
    return loss


def logspace_logloss_1d(linear_out, y_target):
    """ linear_out is NxK, y_target is 1-vector with ints.
    *note* range of labels is 0,...,K-1.
    """
    softmax_out = T.nnet.softmax(linear_out)
    ce = T.nnet.categorical_crossentropy(softmax_out, y_target)
    mce = T.mean(ce)
    loss = T.cast(mce, 'float32')
    return loss



def logspace_logloss_1d_stable(linear_out, y_target):
    """ linear_out is NxK, y_target is 1-vector with ints.
    *note* range of labels is 0,...,K-1.
    """
    linear_out2 = linear_out - linear_out.max(1, keepdims=True)
    q_denom = T.log(T.sum(T.exp(linear_out2), axis=1, keepdims=True))
    log_softmax = linear_out2 - q_denom
    # equivalent to -sum_x p(x)*q(x) when p(x) (true dist) is an int label
    ce = -log_softmax[T.arange(log_softmax.shape[0]), y_target]
    mce = T.mean(ce)
    loss = T.cast(mce, 'float32')
    return loss


def masked_logspace_logloss_1d(linear_out, y_target):
    """ linear_out is NxK, y_target is 1-vector with ints.
    *note* range of labels 1,...,K, not 0,...,K-1.
    IF YOU GET IT WRONG IT MAY BE HARD TO DEBUG.
    """
    nz_ix = y_target.nonzero()
    # note the minus 1!
    y_target_nz = y_target[nz_ix]-1
    linear_out_nz = linear_out[nz_ix]
    loss = logspace_logloss_1d(linear_out_nz, y_target_nz)
    return loss


def masked_logspace_logloss_1d_stable(linear_out, y_target):
    """ linear_out is NxK, y_target is 1-vector with ints.
    *note* range of labels 1,...,K, not 0,...,K-1.
    IF YOU GET IT WRONG IT MAY BE HARD TO DEBUG.
    """
    nz_ix = y_target.nonzero()
    # note the minus 1!
    y_target_nz = y_target[nz_ix]-1
    linear_out_nz = linear_out[nz_ix]
    loss = logspace_logloss_1d_stable(linear_out_nz, y_target_nz)
    return loss


def prediction2d_to_prediction1d(out2d):
    """ BxCxHxW to (BHW)xC. im2col-like
    """
    BHW = out2d.shape[0]*out2d.shape[2]*out2d.shape[3]
    K = out2d.shape[1]
    out2d_reshaped = T.reshape(out2d.dimshuffle(0, 2, 3, 1), (BHW, K), ndim=2)
    return out2d_reshaped


def logspace_logloss_2d(linear_out, y_target):
    """
    input is (b,c,h,w). b*w*h samples, and a K unnormalized pred for each
    assuming y_target (ground truth) is a label image with indexes in each pixel
    *note* range of labels 1,...,K, not 0,...,K-1.
    """
    out_reshaped = prediction2d_to_prediction1d(linear_out)
    y_target_reshaped = y_target.flatten()
    loss = logspace_logloss_1d(out_reshaped, y_target_reshaped)
    return loss


def logspace_logloss_2d_stable(linear_out, y_target):
    """
    input is (b,c,h,w). b*w*h samples, and a K unnormalized pred for each
    assuming y_target (ground truth) is a label image with indexes in each pixel.
    *note* range of labels 1,...,K, not 0,...,K-1.

    in theory, logspace_logloss_2d_stable and logspace_logloss_2d should always
    be equivalent, and they might be depending on what theano does under the
    hood. In practice, for current versions of theano, there are several cases
    where theano can't figure out the log(softmax()) situation. So here
    we just do it explicitly.
    """
    out_reshaped = prediction2d_to_prediction1d(linear_out)
    y_target_reshaped = y_target.flatten()
    loss = logspace_logloss_1d_stable(out_reshaped, y_target_reshaped)
    return loss


def masked_logspace_logloss_2d(linear_out, y_target):
    """
    # input is (b,c,h,w). b*w*h samples, and a K unnormalized for each
    by convention 0 means 'no label'.
    TODO case when mask != 0
    """
    y_target_reshaped = y_target.flatten()
    out_reshaped = prediction2d_to_prediction1d(linear_out)
    loss = masked_logspace_logloss_1d(out_reshaped, y_target_reshaped)
    return loss


def masked_logspace_logloss_2d_stable(linear_out, y_target):
    """
    # input is (b,c,h,w). b*w*h samples, and a K unnormalized for each
    by convention 0 means 'no label'.
    TODO case when mask != 0
    """
    y_target_reshaped = y_target.flatten()
    out_reshaped = prediction2d_to_prediction1d(linear_out)
    loss = masked_logspace_logloss_1d_stable(out_reshaped, y_target_reshaped)
    return loss


def error_rate_1d(output, y_target):
    pred = T.argmax(output, axis=1)
    errs = T.neq(pred, y_target)
    err_rate = T.mean(errs)
    return T.cast(err_rate, 'float32')


def error_rate_2d(output, y_target):
    """ same as 1d so maybe just use same """
    pred = T.argmax(output, axis=1, keepdims=True)
    errs = T.neq(pred, y_target)
    err_rate = T.mean(errs)
    return T.cast(err_rate, 'float32')


def accuracy_1d(output, y_target):
    pred = T.argmax(output, axis=1)
    hits = T.eq(pred, y_target)
    acc = T.mean(hits)
    return T.cast(acc, 'float32')


def accuracy_2d(output, y_target):
    """ same as 1d so maybe just use same """
    pred = T.argmax(output, axis=1, keepdims=True)
    hits = T.eq(pred, y_target)
    acc = T.mean(hits)
    return T.cast(acc, 'float32')


def binary_accuracy_1d(output, y_target):
    pred = T.gt(output, 0.5)
    hits = T.eq(pred, y_target)
    acc = T.mean(hits)
    return T.cast(acc, 'float32')


def binary_accuracy_2d(output, y_target):
    """ same as 1d so maybe just use same """
    pred = T.gt(output, 0.5)
    hits = T.eq(pred, y_target)
    acc = T.mean(hits)
    return T.cast(acc, 'float32')


def masked_error_rate_1d(output, y_target):
    nz_ix = y_target.nonzero()
    y_target = y_target[nz_ix]-1
    output = output[nz_ix]
    err_rate = error_rate_1d(output, y_target)
    return err_rate


def masked_accuracy_1d(output, y_target):
    nz_ix = y_target.nonzero()
    y_target = y_target[nz_ix]-1
    output = output[nz_ix]
    acc = accuracy_1d(output, y_target)
    return acc


def masked_error_rate_2d(output, y_target):
    """
    by convention 0 means 'no label'.
    """
    y_target_reshaped = y_target.flatten()
    out_reshaped = prediction2d_to_prediction1d(output)
    err_rate = masked_error_rate_1d(out_reshaped, y_target_reshaped)
    return err_rate


def masked_accuracy_2d(output, y_target):
    """
    by convention 0 means 'no label'.
    """
    y_target_reshaped = y_target.flatten()
    out_reshaped = prediction2d_to_prediction1d(output)
    acc = masked_accuracy_1d(out_reshaped, y_target_reshaped)
    return acc



def unitize_normals_2d(tensor_var, epsilon=1e-7):
    """ normalize 3-vectors along channel dimension, assuming BCHW.
    """
    assert(tensor_var.ndim == 4)
    dtype = np.dtype(theano.config.floatX).type
    norms = T.sqrt(T.sum(T.sqr(tensor_var), axis=1, keepdims=True))
    constrained_output = (tensor_var * (1./(dtype(epsilon)+norms)))
    return constrained_output


def normal_loss_2d(output, y_target):
    """
    input is (b,c,h,w). b*w*h samples, and a K unnormalized pred for each
    y_target (ground truth) should be HxWx3 image with normals

    from DNS paper:
        - normalize vector w l2 norm, backprop through this norm
        - elementwise loss formula
        L(N, N*) = -1/n sum_i N_i . N_i^* = -1/n N . N^*
        where . is dot product, i is the ith pixel (of n)
        so at the end of the day we can vectorize the whole image map
        and do dot prod
    """
    nnz = output.shape[0]*output.shape[2]*output.shape[3]
    norm_output = unitize_normals_2d(output)
    loss = -T.sum(norm_output * y_target)/nnz
    return loss


def masked_normal_loss_2d(output, y_target):
    """
    by convention 0,0,0 normal means 'no label'.
    that naturally leads to 0 loss.
    however, the mean has to be adjusted to account
    for different number of pixels.
    """
    # number of nonzero pixels
    nnz = T.all(T.neq(y_target, 0), axis=1).sum()
    norm_output = unitize_normals_2d(output)
    loss = -T.sum(norm_output * y_target)/nnz
    return loss



def masked_smooth_depth_loss_2d(output, y_target):
    """ uses eigen code.
    """
    nz_mask = T.neq(y_target, 0.)
    B = output.shape[0]
    HW = output.shape[2]*output.shape[3]

    y_target_reshaped = y_target.reshape((B, HW))
    out_reshaped = output.reshape((B, HW))
    nz_mask_reshaped = nz_mask.reshape((B, HW))

    p = out_reshaped * nz_mask_reshaped
    t = y_target_reshaped * nz_mask_reshaped
    d = (p - t)
    nnz = T.sum(nz_mask_reshaped, axis=1)
    num = T.sum(nnz*T.sum(d**2, axis=1))-0.5*T.sum(T.sum(d, axis=1)**2)
    den = T.maximum(T.sum(nnz**2), 1)
    depth_loss =  num / den

    h = 1
    p_di = (output[:,0,h:,:] - output[:,0,:-h,:]) * (1./np.float32(h))
    p_dj = (output[:,0,:,h:] - output[:,0,:,:-h]) * (1./np.float32(h))
    t_di = (y_target[:,0,h:,:] - y_target[:,0,:-h,:]) * (1./np.float32(h))
    t_dj = (y_target[:,0,:,h:] - y_target[:,0,:,:-h]) * (1./np.float32(h))
    m_di = nz_mask[:,0,h:,:] & nz_mask[:,0,:-h,:]
    m_dj = nz_mask[:,0,:,h:] & nz_mask[:,0,:,:-h]

    grad_loss_i = T.sum(m_di * (p_di - t_di)**2) / T.sum(m_di)
    grad_loss_j = T.sum(m_dj * (p_dj - t_dj)**2) / T.sum(m_dj)

    loss = depth_loss + grad_loss_i + grad_loss_j
    return loss/B


def masked_depth_loss_2d(output, y_target):
    """
    """

    nz_mask = T.neq(y_target, 0.)
    B = output.shape[0]
    HW = output.shape[2]*output.shape[3]

    y_target_reshaped = y_target.reshape((B, HW))
    out_reshaped = output.reshape((B, HW))
    nz_mask_reshaped = nz_mask.reshape((B, HW))

    p = out_reshaped * nz_mask_reshaped
    t = y_target_reshaped * nz_mask_reshaped
    d = (p - t)
    nnz = T.sum(nz_mask_reshaped, axis=1)
    num = T.sum(nnz*T.sum(d**2, axis=1))-0.5*T.sum(T.sum(d, axis=1)**2)
    den = T.maximum(T.sum(nnz**2), 1)
    depth_loss =  num / den
    return depth_loss/B


def masked_mse_loss_2d(output, y_target):
    """
    """

    nz_mask = T.neq(y_target, 0.)
    B = output.shape[0]
    HW = output.shape[2]*output.shape[3]

    y_target_reshaped = y_target.reshape((B, HW))
    out_reshaped = output.reshape((B, HW))
    nz_mask_reshaped = nz_mask.reshape((B, HW))

    p = out_reshaped * nz_mask_reshaped
    t = y_target_reshaped * nz_mask_reshaped
    d = (p - t)**2.
    nnz = T.sum(nz_mask_reshaped, axis=1)
    loss = T.mean(T.sum(d, axis=1)/(T.maximum(nnz, 1)))
    return loss


def masked_smooth_mse_loss_2d(output, y_target):
    """
    """

    nz_mask = T.neq(y_target, 0.)
    B = output.shape[0]
    HW = output.shape[2]*output.shape[3]

    y_target_reshaped = y_target.reshape((B, HW))
    out_reshaped = output.reshape((B, HW))
    nz_mask_reshaped = nz_mask.reshape((B, HW))

    p = out_reshaped * nz_mask_reshaped
    t = y_target_reshaped * nz_mask_reshaped
    d = (p - t)**2.
    nnz = T.sum(nz_mask_reshaped, axis=1)
    depth_loss = T.sum(T.sum(d, axis=1)/(T.maximum(nnz, 1)))

    h = 1
    p_di = (output[:,0,h:,:] - output[:,0,:-h,:]) * (1./np.float32(h))
    p_dj = (output[:,0,:,h:] - output[:,0,:,:-h]) * (1./np.float32(h))
    t_di = (y_target[:,0,h:,:] - y_target[:,0,:-h,:]) * (1./np.float32(h))
    t_dj = (y_target[:,0,:,h:] - y_target[:,0,:,:-h]) * (1./np.float32(h))
    m_di = nz_mask[:,0,h:,:] & nz_mask[:,0,:-h,:]
    m_dj = nz_mask[:,0,:,h:] & nz_mask[:,0,:,:-h]

    grad_loss_i = T.sum(m_di * (p_di - t_di)**2) / T.sum(m_di)
    grad_loss_j = T.sum(m_dj * (p_dj - t_dj)**2) / T.sum(m_dj)

    loss = depth_loss + grad_loss_i + grad_loss_j

    return loss/B


def masked_exp_mse_loss_2d(output, y_target):
    """
    """
    output = T.exp(output)
    y_target = T.exp(y_target)

    nz_mask = T.neq(y_target, 0.)
    B = output.shape[0]
    HW = output.shape[2]*output.shape[3]

    y_target_reshaped = y_target.reshape((B, HW))
    out_reshaped = output.reshape((B, HW))
    nz_mask_reshaped = nz_mask.reshape((B, HW))

    p = out_reshaped * nz_mask_reshaped
    t = y_target_reshaped * nz_mask_reshaped
    d = (p - t)**2.
    nnz = T.sum(nz_mask_reshaped, axis=1)
    loss = T.mean(T.sum(d, axis=1)/(T.maximum(nnz, 1)))
    return loss


#def masked_onehot_logspace_binary_logloss(linear_out, y_target):
#    """
#    NOTE: not finished: no masking
#    """
#    B, K, H, W = linear_out.shape
#    yhat_reshaped = T.reshape(linear_out.dimshuffle((0, 2, 3, 1)), (B*H*W, K))
#    y_target_flat = T.flatten(y_target)
#    # note how we discard first column of one hot, since it's mask
#    yflat_onehot = T.extra_ops.to_one_hot(T.cast(y_target_flat, 'int32'), K+1)[:, 1:]
#    cost = T.nnet.binary_crossentropy(T.nnet.sigmoid(yhat_reshaped), yflat_onehot).mean()
#    return cost
