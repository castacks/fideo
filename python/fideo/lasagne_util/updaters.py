
import numpy as np
import theano
import lasagne


def norm_constrainer(norm, updates):
    # TODO use tagging or better way to detect conv layers
    # TODO make this optional
    for param, update in updates.iteritems():
        if ((param.name is not None) and
            (param.name.startswith('conv') or
             param.name.startswith('deconv') or
             param.name.startswith('nin')) and
             param.name.endswith('.W')):
            # TODO make this a param. hinton says 3-4. but that's 2d
            updates[param] = lasagne.updates.norm_constraint(update, norm)
    return updates


class SgdUpdater(object):

    def __init__(self, init_lr, momentum, norm_constraint=0.):
        self.momentum = momentum
        self.norm_constraint = norm_constraint
        self.init_lr = init_lr
        # dynamically adjustable
        self.lr_var = theano.shared(np.float32(init_lr))

    def __call__(self, loss, params):
        updates = lasagne.updates.momentum(loss, params, self.lr_var, self.momentum)
        if self.norm_constraint > 0.:
            updates = norm_constrainer(self.norm_constraint, updates)
        return updates


class AdamUpdater(object):
    def __init__(self, init_lr, **kwargs):
        # TODO
        # lr = 0.001, beta1 = 0.9, beta2=0.999, eps=1e-8
        self.init_lr = init_lr
        self.extra_args = kwargs

    def __call__(self, loss, params):
        updates = lasagne.updates.adam(loss, params, self.init_lr, **self.extra_args)
        # updates = lasagne.updates.adam(loss, params, self.init_lr)
        return updates
