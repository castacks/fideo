import lasagne
import theano.tensor as T
import numpy as np

__all__ = [\
        'ParallelProj2dLayer',
        'ParallelProj3dLayer',
        ]


class ParallelProj2dLayer(lasagne.layers.Layer):
    """
    3d -> 2d by pooling

    projects as
    (b,c,t,0,1) -> (b,c,0,1)

    TODO: was it bct01 or bc012?
    """
    def __init__(self, input_layer, pooling='max'):
        super(ParallelProj2dLayer, self).__init__(input_layer)
        self.pooling=pooling

    def get_output_shape_for(self, input_shape):
        # b c remain same. note c is the number of classes.
        return input_shape[0:2] + input_shape[3:5]

    def get_output_for(self, input, *args, **kwargs):
        if self.pooling=='max':
            #pixelwise = input.max(axis=2, keepdims=True)
            pixelwise = input.max(axis=2)
            #pixelwise = input.max(axis=1)
        elif self.pooling=='sum':
            #pixelwise = input.sum(axis=2, keepdims=True)
            pixelwise = input.sum(axis=2)
        else:
            raise ValueError('pooling is max or sum')
        return pixelwise


class ParallelProj3dLayer(lasagne.layers.Layer):
    """
    2d -> 3d by tiling
    """

    def __init__(self, input_layer, new_dims):
        super(ParallelProj3dLayer, self).__init__(input_layer)
        self.new_dims = new_dims

    def get_output_shape_for(self, input_shape):
        # b c remain same. note c is the number of classes.
        return tuple(input_shape[0:2]) + (self.new_dims,) + tuple(input_shape[2:4])

    def get_output_for(self, input, *args, **kwargs):
        #x = T.fmatrix()
        #x3 = x.dimshuffle('x', 0, 1)
        input2 = input.dimshuffle(0, 1, 'x', 2, 3)
        rep = T.tile(input2, (1,1,self.new_dims,1,1), ndim=5)
        return rep


