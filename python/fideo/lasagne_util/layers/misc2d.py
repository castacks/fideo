
import lasagne
import theano
import theano.tensor as T
import numpy as np


__all__ = [\
        'Upsample2dLayer',
        'SparseUpsample2dLayer',
        'RepeatUpsample2dLayer',
        'Softmax2dLayer',
        'Crop2dLayer',
        'TiedDropoutLayer',
        'Deconv2dLayer',
        'NormalizeScale2dLayer',
        ]


floatX = theano.config.floatX

from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams
_srng = RandomStreams()



class Deconv2dLayer(lasagne.layers.Layer):
    """ from https://groups.google.com/forum/#!msg/lasagne-users/KB4SmoODtkg/FKCaR5wlCwAJ
    requires theano w/ convolution placeholder Op

    NOTE. Lasagne now has TransposeConv2d.
    """

    def __init__(self, incoming, num_filters, filter_size, stride=1, pad=0,
            nonlinearity=lasagne.nonlinearities.rectify, **kwargs):
        super(Deconv2DLayer, self).__init__(incoming, **kwargs)
        self.num_filters = num_filters
        self.filter_size = lasagne.utils.as_tuple(filter_size, 2, int)
        self.stride = lasagne.utils.as_tuple(stride, 2, int)
        self.pad = lasagne.utils.as_tuple(pad, 2, int)
        self.W = self.add_param(lasagne.init.Orthogonal(),
                (self.input_shape[1], num_filters) + self.filter_size,
                name='W')
        self.b = self.add_param(lasagne.init.Constant(0),
                (num_filters,),
                name='b')
        if nonlinearity is None:
            nonlinearity = lasagne.nonlinearities.identity
        self.nonlinearity = nonlinearity

    def get_output_shape_for(self, input_shape):
        shape = tuple(i*s - 2*p + f - 1
                for i, s, p, f in zip(input_shape[2:],
                                      self.stride,
                                      self.pad,
                                      self.filter_size))
        return (input_shape[0], self.num_filters) + shape

    def get_output_for(self, input, **kwargs):
        op = T.nnet.abstract_conv.AbstractConv2d_gradInputs(
            imshp=self.output_shape,
            kshp=(self.input_shape[1], self.num_filters) + self.filter_size,
            subsample=self.stride, border_mode=self.pad)
        conved = op(self.W, input, self.output_shape[2:])
        if self.b is not None:
            conved += self.b.dimshuffle('x', 0, 'x', 'x')
        return self.nonlinearity(conved)


class NormalizeScale2dLayer(lasagne.layers.Layer):
    """ Normalize and scale, inspired by parsenet.
    NOTE: lasagne has scalelayer, biaslayer, as well as 'standardize'.
    """
    def __init__(self, incoming, scales=lasagne.init.Constant(1.), **kwargs):
        super(NormalizeScale2dLayer, self).__init__(incoming, **kwargs)

        self.shared_axes = (0, 2, 3)
        shape = [self.input_shape[1]]
        self.scales = self.add_param(scales, shape, 'scales', regularizable=False)

    def get_output_for(self, input, *args, **kwargs):
        input_sqr = input**2.
        input_norm_sqr = input_sqr.sum(1, keepdims=True)
        input_norm = T.sqrt(input_norm_sqr + 1e-12)
        input_unit = input/input_norm

        axes = iter(range(self.scales.ndim))
        pattern = ['x' if input_axis in self.shared_axes
                   else next(axes) for input_axis in range(input.ndim)]

        #return input_unit * self.scales.dimshuffle('x', 0, 'x', 'x')
        return input_unit * self.scales.dimshuffle('x', 0, 'x', 'x')


class Upsample2dLayer(lasagne.layers.Layer):
    """
    NOTE: older, less flexible implementation!
    aka unpooling. this is nonadaptive.

    there is a anna repository with deconv layers.
    this also works:
    upsampler = T.repeat( T.repeat( outputs[3], 4, axis=2 ), 4, axis=3 )
    is it faster? GPU support?
    if no gpu, two tilers may be faster.

    padding is very naive: it simply adds zeros in the bottom and right.
    it's for those annoying off-by-a-pixel situations.
    if you need nicer padding consider the lasagne padding layer.
    """

    def __init__(self, input_layer, times=1, pad=0, **kwargs):
        """ for now assuming always factor of 2.
        """
        super(Upsample2dLayer, self).__init__(input_layer, **kwargs)
        self.times = times
        self.pad = pad

    def get_output_shape_for(self, input_shape):
        # b c remain same
        return input_shape[:2]+tuple( d*(2**self.times)+self.pad for d in input_shape[2:] )

    def get_output_for(self, input, *args, **kwargs):
        # TODO is this any faster than less stupid versions?
        if self.times==1:
            out = T.zeros((input.shape[0], input.shape[1], 2*input.shape[2]+self.pad, 2*input.shape[3]+self.pad), dtype=floatX)
            # set_subtensor( lvalue, rvalue )
            out = T.set_subtensor(out[:, :, 0::2, 0::2], input)
            out = T.set_subtensor(out[:, :, 1::2, 1::2], input)
            out = T.set_subtensor(out[:, :, 0::2, 1::2], input)
            out = T.set_subtensor(out[:, :, 1::2, 0::2], input)
        elif self.times==2:
            # ugh im sorry
            out = T.zeros((input.shape[0], input.shape[1], 4*input.shape[2]+self.pad, 4*input.shape[3]+self.pad), dtype=floatX)
            out = T.set_subtensor(out[:, :, 0::4, 0::4], input)
            out = T.set_subtensor(out[:, :, 0::4, 1::4], input)
            out = T.set_subtensor(out[:, :, 0::4, 2::4], input)
            out = T.set_subtensor(out[:, :, 0::4, 3::4], input)

            out = T.set_subtensor(out[:, :, 1::4, 0::4], input)
            out = T.set_subtensor(out[:, :, 1::4, 1::4], input)
            out = T.set_subtensor(out[:, :, 1::4, 2::4], input)
            out = T.set_subtensor(out[:, :, 1::4, 3::4], input)

            out = T.set_subtensor(out[:, :, 2::4, 0::4], input)
            out = T.set_subtensor(out[:, :, 2::4, 1::4], input)
            out = T.set_subtensor(out[:, :, 2::4, 2::4], input)
            out = T.set_subtensor(out[:, :, 2::4, 3::4], input)

            out = T.set_subtensor(out[:, :, 3::4, 0::4], input)
            out = T.set_subtensor(out[:, :, 3::4, 1::4], input)
            out = T.set_subtensor(out[:, :, 3::4, 2::4], input)
            out = T.set_subtensor(out[:, :, 3::4, 3::4], input)
        else:
            raise NotImplementedError()

        return out


class RepeatUpsample2dLayer(lasagne.layers.Layer):
    """
    This layer performs unpooling over the last two dimensions
    of a 4D tensor.
    It uses the theano repeat() expression.
    It may or may not be faster, depending on GPU/Theano version/etc.
    update: seems slightly slower.
    """
    def __init__(self, incoming, times=1, **kwargs):
        super(RepeatUpsample2dLayer, self).__init__(incoming, **kwargs)
        self.times = times

    def get_output_shape_for(self, input_shape):
        output_shape = list(input_shape)
        output_shape[2] = input_shape[2] * (2**self.times)
        output_shape[3] = input_shape[3] * (2**self.times)
        return tuple(output_shape)

    def get_output_for(self, input, **kwargs):
        if self.times==1:
            return input.repeat(2, axis=2).repeat(2, axis=3)
        elif self.times==2:
            return input.repeat(4, axis=2).repeat(4, axis=3)
        elif self.times==4:
            return input.repeat(8, axis=2).repeat(8, axis=3)
        else:
            raise NotImplementedError()


class SparseUpsample2dLayer(lasagne.layers.Layer):
    """
    Like Upsample2dLayer, but only fills in upper
    left pixels. Rest of them are zeros.
    I think this is more sensible from a signals processing
    perspective.
    """

    def __init__(self, input_layer, times=1, factor=None, pad=0, **kwargs):
        """
        times specifies powers of two, ie factor = 2**times.
        factor is assumed to be int.
        """
        super(SparseUpsample2dLayer, self).__init__(input_layer, **kwargs)
        if times is not None and factor is not None:
            raise ValueError('only times or factor must be specified, not both')
        if times is None and factor is None:
            raise ValueError('must specify one of times or factor')
        self.times = times
        self.factor = factor
        self.pad = pad

    def get_output_shape_for(self, input_shape):
        # b c remain same
        assert((self.times is not None) or (self.factor is not None))
        if self.times is not None:
            return input_shape[:2]+tuple( d*(2**self.times)+self.pad for d in input_shape[2:] )
        return input_shape[:2]+tuple( d*(self.factor)+self.pad for d in input_shape[2:] )

    def get_output_for(self, input, *args, **kwargs):
        assert((self.times is not None) or (self.factor is not None))
        # TODO is this any faster than less stupid versions?
        if self.times is not None:
            factor = 2**self.times
        else:
            factor = self.factor
        out = T.zeros((input.shape[0], input.shape[1], factor*input.shape[2]+self.pad, factor*input.shape[3]+self.pad), dtype=floatX)
        out = T.set_subtensor(out[:, :, 0::factor, 0::factor], input)
        return out


class Softmax2dLayer(lasagne.layers.Layer):
    """ a layer that applies softmax transform along
    the channel axis for each pixel in an image.
    tries to be numerically stable, emphasis on tries.
    """
    def __init__(self, input_layer):
        super(Softmax2dLayer, self).__init__(input_layer)

    def get_output_shape_for(self, input_shape):
        # b c remain same. note c is the number of classes.
        return input_shape[0:2] + tuple(d for d in input_shape[2:])

    def get_output_for(self, input, *args, **kwargs):
        pixelwise_max = input.max(axis=1, keepdims=True)
        pixelwise_exp = theano.tensor.exp(input-pixelwise_max)
        pixelwise_sum = pixelwise_exp.sum(axis=1, keepdims=True)
        out = pixelwise_exp/(pixelwise_sum)
        return out



class Crop2dLayer(lasagne.layers.Layer):
    """ Crop spatial borders in 2D.
    """
    def __init__(self, input_layer, output_shape, **kwargs):
        super(Crop2dLayer, self).__init__(input_layer, **kwargs)
        # not including batch
        self._output_shape = output_shape

    def get_output_shape_for(self, input_shape):
        for i in (0,1):
            if input_shape[2+i] < self._output_shape[i]:
                raise ValueError('input shape {} is smaller ({}) than output shape ({})'.format(i,input_shape[i],self._output_shape[i]))

        return input_shape[0:2]+self._output_shape

    def get_output_for(self, input, *args, **kwargs):
        row0 = (input.shape[2]-self._output_shape[0])//2
        col0 = (input.shape[3]-self._output_shape[1])//2
        row1 = row0+self._output_shape[0]
        col1 = col0+self._output_shape[1]
        return input[:,:,row0:row1,col0:col1]


class TiedDropoutLayer(lasagne.layers.Layer):
    """
    from kaggle-ndsb
    Dropout layer that broadcasts the mask across all axes beyond the first
    two.
    NOTE: lasagne now supports this by tying together axes in dropout.
    """
    def __init__(self, input_layer, p=0.5, rescale=True, **kwargs):
        super(TiedDropoutLayer, self).__init__(input_layer, **kwargs)
        self.p = p
        self.rescale = rescale

    def get_output_for(self, input, deterministic=False, *args, **kwargs):
        if deterministic or self.p == 0:
            return input
        else:
            retain_prob = 1 - self.p
            if self.rescale:
                input /= retain_prob
            mask = _srng.binomial(input.shape[:2], p=retain_prob,
                                      dtype=floatX)
            axes = [0, 1] + (['x'] * (input.ndim - 2))
            mask = mask.dimshuffle(*axes)
            return input * mask
