#
# @author  Daniel Maturana
# @year    2015
#
# @attention Copyright (c) 2015
# @attention Carnegie Mellon University
# @attention All rights reserved.
#
# @=


import numpy as np
import theano
import theano.tensor as T

import lasagne
from lasagne.layers import Layer
from lasagne.utils import as_tuple

floatX = theano.config.floatX


__all__ = [\
        'SpatialDimReductionLayer',
        'ReshapeLayer',
        'IdentityLayer',
        ]


class SpatialDimReductionLayer(Layer):
    """
    from kaggle-ndsb
    Spatial dimension reduction layer. (b, c, 0, 1) -> (b, c, n)
    """
    def __init__(self,
                 input_layer,
                 num_units,
                 W=lasagne.init.Uniform(),
                 b=lasagne.init.Constant(0.),
                 nonlinearity=lasagne.nonlinearities.rectify):
        super(SpatialDimReductionLayer, self).__init__(input_layer)
        if nonlinearity is None:
            self.nonlinearity = lasagne.nonlinearities.identity
        else:
            self.nonlinearity = nonlinearity

        self.num_units = num_units

        output_shape = self.input_layer.get_output_shape()
        num_inputs = int(np.prod(output_shape[2:]))

        self.W = self.create_param(W, (num_inputs, num_units))
        self.b = self.create_param(b, (num_units,)) if b is not None else None

    def get_params(self):
        return [self.W] + self.get_bias_params()

    def get_bias_params(self):
        return [self.b] if self.b is not None else []

    def get_output_shape_for(self, input_shape):
        return (input_shape[0], input_shape[1], np.prod(input_shape[2:]))

    def get_output_for(self, input, *args, **kwargs):
        input_grouped = input.reshape((input.shape[0] * input.shape[1], T.prod(input.shape[2:]))) # fold b, c and fold 0, 1, ...
        activation = T.dot(input_grouped, self.W)

        if self.b is not None:
            activation = activation + self.b.dimshuffle('x', 0)

        activation = activation.reshape((input.shape[0], input.shape[1], activation.shape[1])) # unfold b, c
        return self.nonlinearity(activation)



class ReshapeLayer(Layer):
    """ note lasagne has one too now
    """
    def __init__(self, input_layer, output_shape, **kwargs):
        super(ReshapeLayer, self).__init__(input_layer, **kwargs)
        # not including batch
        self.output_shape = output_shape
        self.ndim = len(output_shape)

    def get_output_shape_for(self, input_shape):
        return (input_shape[0],)+self.output_shape

    def get_output_for(self, input, *args, **kwargs):
        shape_with_batch = (input.shape[0],)+self.output_shape
        # TODO verify this makes sense
        return T.reshape(input, shape_with_batch, ndim=(self.ndim+1))


class IdentityLayer(Layer):
    """ A no-op layer that simply passes along the input.
    """
    def __init__(self, incoming, **kwargs):
        super(IdentityLayer, self).__init__(incoming, **kwargs)

    def get_output_for(self, input, **kwargs):
        return input
