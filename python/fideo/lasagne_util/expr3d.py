"""

Some Theano expressions for spatially 3D / volumetric data.

"""

import numpy as np
import theano
import theano.tensor as T


def logloss_3d(pre_softmax_output, y_batch):
    """
    input is (b,c,h,w). b*w*h samples, and a K unnormalized pred for each
    assuming y_batch (ground truth) is a label image with indexes in each pixel
    """
    B, K, T_, H, W = pre_softmax_output.shape
    out_reshaped = T.reshape(pre_softmax_output.dimshuffle(0, 2, 3, 4, 1),
                             (B * T_ * H * W, K),
                             ndim=2)
    softmax_out = T.nnet.softmax(out_reshaped)
    y_batch_reshaped = y_batch.flatten()
    loss = T.cast(T.mean(T.nnet.categorical_crossentropy(
        softmax_out, y_batch_reshaped)), 'float32')
    return loss


def masked_logloss_3d(pre_softmax_output, y_batch):
    """
    input is (b,c,h,w). b*w*h samples, and a K unnormalized pred for each
    assuming y_batch (ground truth) is a label image with indexes in each pixel
    """
    y_batch_reshaped = y_batch.flatten()
    nz_ix = y_batch_reshaped.nonzero()
    y_batch_reshaped = y_batch_reshaped[nz_ix] - 1

    B, K, T_, H, W = pre_softmax_output.shape
    out_reshaped = T.reshape(pre_softmax_output.dimshuffle(0, 2, 3, 4, 1),
                             (B * T_ * H * W, K),
                             ndim=2)
    out_reshaped = out_reshaped[nz_ix]
    softmax_out = T.nnet.softmax(out_reshaped)
    loss = T.cast(T.mean(T.nnet.categorical_crossentropy(
        softmax_out, y_batch_reshaped)), 'float32')
    return loss


def masked_binary_cross_entropy_3d(pre_sigmoid_output, y_batch):
    """
    input is (b,c,h,w). b*w*h samples, and a K unnormalized pred for each
    assuming y_batch (ground truth) is a label image with indexes in each pixel
    """
    B, K, T_, H, W = pre_softmax_output.shape
    y_batch_reshaped = T.reshape(y_batch.dimshuffle(0, 2, 3, 4, 1),
                                 (B * T_ * H * W, K),
                                 ndim=2)

    # TODO how to calc mask
    nz_ix = y_batch_reshaped.sum(1).nonzero()
    y_batch_reshaped = y_batch_reshaped[nz_ix]

    out_reshaped = T.reshape(pre_softmax_output.dimshuffle(0, 2, 3, 4, 1),
                             (B * T_ * H * W, K),
                             ndim=2)
    out_reshaped = out_reshaped[nz_ix]

    sigmoid_out = T.nnet.sigmoid(out_reshaped)
    loss = T.cast(T.mean(
        T.nnet.binary_cross_entropy(sigmoid_out, y_batch_reshaped)), 'float32')
    return loss


def masked_binary_cross_entropy_3d_fast(pre_sigmoid_output, y_batch):
    """
    uses one hot encoding.
    differs by constant of factor of K (num labels) with other theano
    """
    nz = T.cast((y_batch.sum(1) > 0), 'float32')
    nnz = nz.sum()  #*y_batch.shape[1]
    sigmoid_out = T.nnet.sigmoid(pre_sigmoid_output)
    #masked = nz.dimshuffle([0,'x',1,2,3])*sigmoid_out
    loss = T.cast(
        T.sum(nz.dimshuffle([0, 'x', 1, 2, 3]) * T.nnet.binary_crossentropy(
            sigmoid_out, y_batch)) / nnz, 'float32')
    return loss


def masked_error_rate_3d(output, y_batch):
    """
    TODO case when mask != 0
    """
    # because the theano magic for numerical stability
    # is not working after this reshape.
    y_batch_reshaped = y_batch.flatten()
    nz_ix = y_batch_reshaped.nonzero()
    y_batch_reshaped = y_batch_reshaped[nz_ix] - 1
    # input is (b,c,t,h,w). b*w*t*h samples, and a K unnormalized for each
    # labels are in 0-K range inclusive but 0 is invalid/missing.
    B, K, T_, H, W = output.shape
    out_reshaped = T.reshape(output.dimshuffle(0, 2, 3, 4, 1),
                             (B * T_ * H * W, K),
                             ndim=2)
    out_reshaped = out_reshaped[nz_ix]
    #pred = T.argmax( out_reshaped, axis=1, keepdims=True )
    pred = T.argmax(out_reshaped, axis=1)
    err = T.cast(T.mean(T.neq(pred, y_batch_reshaped)), 'float32')
    return err


def masked_multilabel_error_rate_3d(pre_sigmoid_output, y_batch):
    nz = T.cast((y_batch.sum(1) > 0), 'float32')
    nnz = nz.sum() * y_batch.shape[1]
    # note pre-sigmoid the threshold is 0
    errors = T.cast(T.neq((pre_sigmoid_output > 0.), y_batch), 'float32')
    loss = T.cast(T.sum(
        (nz.dimshuffle([0, 'x', 1, 2, 3]) * errors)) / nnz, 'float32')
    return loss


def max_pool_3d(input, ds, ignore_border=False):
    """
    Adapted from lpigou (public domain). TODO link

    TODO this is obsolete.

    Takes as input a N-D tensor, where N >= 3. It downscales the input video by
    the specified factor, by keeping only the maximum value of non-overlapping
    patches of size (ds[0],ds[1],ds[2]) (time, height, width)

    :type input: N-D theano tensor of input images.
    :param input: input images. Max pooling will be done over the 3 last dimensions.
    :type ds: tuple of length 3
    :param ds: factor by which to downscale. (2,2,2) will halve the video in each dimension.
    :param ignore_border: boolean value. When True, (5,5,5) input with ds=(2,2,2) will generate a
      (2,2,2) output. (3,3,3) otherwise.
    """

    if input.ndim < 3:
        raise NotImplementedError('max_pool_3d requires a dimension >= 3')

    # extract nr dimensions
    vid_dim = input.ndim
    # max pool in two different steps, so we can use the 2d implementation of
    # downsamplefactormax. First maxpool frames as usual.
    # Then maxpool the time dimension. Shift the time dimension to the third
    # position, so rows and cols are in the back

    # extract dimensions
    frame_shape = input.shape[-2:]

    # count the number of "leading" dimensions, store as dmatrix
    batch_size = T.prod(input.shape[:-2])
    batch_size = T.shape_padright(batch_size, 1)

    # store as 4D tensor with shape: (batch_size,1,height,width)
    new_shape = T.cast(T.join(0, batch_size, T.as_tensor([1, ]), frame_shape),
                       'int32')
    input_4D = T.reshape(input, new_shape, ndim=4)

    # downsample mini-batch of videos in rows and cols
    op = T.signal.downsample.DownsampleFactorMax((ds[1], ds[2]), ignore_border)
    output = op(input_4D)
    # restore to original shape
    outshape = T.join(0, input.shape[:-2], output.shape[-2:])
    out = T.reshape(output, outshape, ndim=input.ndim)

    # now maxpool time

    # output (time, rows, cols), reshape so that time is in the back
    shufl = (list(range(vid_dim - 3)) + [vid_dim - 2] + [vid_dim - 1] +
             [vid_dim - 3])
    input_time = out.dimshuffle(shufl)
    # reset dimensions
    vid_shape = input_time.shape[-2:]

    # count the number of "leading" dimensions, store as dmatrix
    batch_size = T.prod(input_time.shape[:-2])
    batch_size = T.shape_padright(batch_size, 1)

    # store as 4D tensor with shape: (batch_size,1,width,time)
    new_shape = T.cast(T.join(0, batch_size, T.as_tensor([1, ]), vid_shape),
                       'int32')
    input_4D_time = T.reshape(input_time, new_shape, ndim=4)
    # downsample mini-batch of videos in time
    op = T.signal.downsample.DownsampleFactorMax((1, ds[0]), ignore_border)
    outtime = op(input_4D_time)
    # output
    # restore to original shape (xxx, rows, cols, time)
    outshape = T.join(0, input_time.shape[:-2], outtime.shape[-2:])
    shufl = (list(range(vid_dim - 3)) + [vid_dim - 1] + [vid_dim - 3] +
             [vid_dim - 2])
    return T.reshape(outtime, outshape, ndim=input.ndim).dimshuffle(shufl)
