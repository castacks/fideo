from __future__ import print_function

import sys
import argparse
import os
import logging

from path import Path

from .. import dump_info
from .train import SupervisedTrainerApp

log = logging.getLogger(__name__)


class SupervisedTrainerAppCli(SupervisedTrainerApp):
    """ cli driver for SupervisedTrainerApp """

    def __init__(self, expt_fname):
        super(SupervisedTrainerAppCli, self).__init__(expt_fname)


    def update_cfg(self, args):
        """ configure runtime options, considering env variables and cli
        parameters.
        """

        # first set up from env
        self.env['data_base_dir'] = os.environ.get('DATA_BASE_DIR')
        self.env['results_base_dir'] = os.environ.get('RESULTS_BASE_DIR')
        self.env['models_base_dir'] = os.environ.get('MODELS_BASE_DIR')

        # cli may override env
        for cli_k, cli_v in args.iteritems():
            if cli_k=='func':
                continue
            if cli_v is None and self.env.get(cli_k, None) is not None:
                # let the env value as-is
                pass
            else:
                # override
                self.env[cli_k] = cli_v

        # default is cwd for dirs, if not set yet
        for k in 'data_base_dir', 'results_base_dir', 'models_base_dir':
            if self.env.get(k, None) is None:
                self.env[k] = '.'
            # ensure type path
            self.env[k] = Path(self.env[k])

        self.env['results_dir'] = Path(self.env['results_base_dir'])/self.expt.exp_id

        # override expt settings
        if self.env['extra_options']:
            argstring = self.env['extra_options'].split(',')
            for arg in argstring:
                key, val = arg.split('=')
                key = key.strip()
                val = eval(val.strip())
                print('setting {} to {}'.format(key, val))
                setattr(self.expt, key, val)


    def train(self, args):
        self.update_cfg(args)
        self.train_init()
        for epoch in xrange(self.expt.MAX_EPOCHS):
            self.train_epoch(epoch)


    def info(self, args):
        self.model = self.expt.build_model()
        dump_info.dump_expt_info(self, console_only=True)


    def time(self, args):
        import time
        import numpy as np
        self.model = self.expt.build_model()
        dout = self.model.compiled_dout
        xdata = np.random.random((1,) + self.model.x_shape[1:]).astype('f4')
        times = []
        for _ in range(10):
            tic = time.time()
            pred = dout(xdata)
            toc = time.time()
            times.append(toc-tic)
        print('median time: %f s'%np.median(times))

    def clean(self, args):
        self.clean_results_dir()


    def cli(self):
        parser = argparse.ArgumentParser()
        subparsers = parser.add_subparsers()

        ### train parser
        train_parser = subparsers.add_parser('train')
        train_parser.add_argument('--device',
                                  type=str,
                                  help='theano device')

        train_parser.add_argument('--weights-fname',
                                  type=str,
                                  help='weights for initialization')

        train_parser.add_argument('--scratch-clean',
                                  action='store_true',
                                  help='train from scratch after deleting previous checkpoints')

        train_parser.add_argument('-r', '--resume',
                                  action='store_true',
                                  help='resume from checkpoint')

        train_parser.add_argument('--pretrain-from',
                                  type=str,
                                  help='resume from different model checkpt')

        train_parser.add_argument('-d', '--debug',
                                  action='store_true',
                                  help='activate debugging')

        train_parser.add_argument('--data-dir',
                                  default='/data',
                                  dest='data_base_dir',
                                  help='data base directory')

        train_parser.add_argument('--results-dir',
                                  default='/results',
                                  dest='results_base_dir',
                                  help='output base directory')

        train_parser.add_argument('--models-dir',
                                  default='/models',
                                  dest='models_base_dir',
                                  help='model base directory')

        train_parser.add_argument('-j', '--workers',
                                  type=int,
                                  default=1,
                                  help='number of workers')

        train_parser.add_argument('-q', '--qsize',
                                  type=int,
                                  default=128,
                                  help='queue size')

        train_parser.add_argument('--extra-options',
                                  type=str,
                                  default=None,
                                  help='override options in expt. dangerous!')

        train_parser.set_defaults(func=self.train)

        ### info parser

        info_parser = subparsers.add_parser('info')
        info_parser.set_defaults(func=self.info)

        ### time parser

        time_parser = subparsers.add_parser('time')
        time_parser.set_defaults(func=self.time)

        args = parser.parse_args()
        args.func(vars(args))
