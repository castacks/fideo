from __future__ import print_function

import sys
import imp
import os
import logging

from funcy import first, cached_property
import numpy as np

from path import Path

import fideo
from fideo import checkpoints
from fideo import monitoring
from fideo import dump_info
from fideo.util import timing as fut

from parafina.pipeline import SerialPump
from parafina.pipeline import ZmqPump


log = logging.getLogger(__name__)


class SupervisedTrainerApp(object):

    def __init__(self, expt_fname):
        self.expt_fname = Path(expt_fname)
        self.expt = imp.load_source('expt', expt_fname)
        self.env = {}
        self.state = {}
        self.restored_itr = 0
        self.train_callbacks = []
        self.valid_callbacks = []


    def init_weights(self):
        """ initialize weights from various possible sources,
        depending on settings in env.
        """

        if self.env['scratch_clean']:
            log.info('initalizing from scratch')
            self.restored_itr = 0
        elif self.env['resume']:
            log.info('resuming, weights from latest')
            cpt = self.checkptr.load_latest()
            self.model.weights_from_checkpoint(cpt)
            self.state = cpt['state'].copy()
            self.restored_itr = cpt['state']['itr_ctr']
        elif self.env['pretrain_from']:
            log.info('resuming from pretrained checkpt')
            tmp_checkptr = checkpoints.PickleGzDir(self.env['pretrain_from'])
            cpt = tmp_checkptr.load_latest()
            self.state = cpt['state'].copy()
            self.model.weights_from_checkpoint(cpt)
            self.restored_itr = 0
        elif self.env['weights_fname'] is not None:
            # TODO models_base_dir
            weights_fname = Path(self.env['models_base_dir'])/self.env['weights_fname']
            log.info('weights from preinit %s', str(weights_fname))
            cpt = checkpoints.PickleGzDir.load_fname(Path(weights_fname))
            self.state = cpt['state'].copy()
            self.model.weights_from_checkpoint(cpt)
            self.restored_itr = cpt['state']['itr_ctr']
        else:
            log.info('trying to resume')
            try:
                cpt = self.checkptr.load_latest()
                log.info('resuming, loading weights from latest')
                self.state = cpt['state'].copy()
                self.model.weights_from_checkpoint(cpt)
                self.restored_itr = cpt['state']['itr_ctr']
            except IOError, ex:
                log.info('no weight files found. initalizing from scratch')
                self.restored_itr = 0
        log.info('init_weights done')


    def clean_results_dir(self):
        """ clean results directory """
        log.warn('cleaning %s', str(self.env['results_dir']))
        files = self.env['results_dir'].files()
        while True:
            yesno = raw_input('remove %d files (y/n)? '%yesno).strip().lower()
            if yesno in 'yn':
                break

        if yesno.strip().lower()=='y':
            for fname in files:
                log.info('removing %s', fname)
                fname.remove()
        log.info('not removing files')


    def train_epoch(self, epoch):
        """ a single training epoch. """

        def _train(chunk):
            with fut.OneShotTimer() as timer:
                outs = self.trainer.train_chunk(chunk)
            # TODO should timing be in a callback
            instances_per_s = len(chunk)/timer.elapsed
            batches_per_s = instances_per_s/self.expt.TRAIN_BATCH_SIZE
            log.info('%f instances/s, %f batches/s', instances_per_s, batches_per_s)
            for cb in self.train_callbacks:
                cb.chunk(self, self.state, chunk, outs)


        def _validation():
            if not self.validation_enabled:
                return

            # are we due for validation yet?
            if (self.state['itr_ctr'] - self.state['last_valid_itr']) < self.expt.VALID_EVERY_NTH:
                return

            log.info('itr %d, validating', self.state['itr_ctr'])
            self.state['last_valid_itr'] = self.state['itr_ctr']

            valid_chunk_gen = self.valid_pump.chunked_array_gen(self.data_spec,
                                                                self.expt.VALID_CHUNK_SIZE)

            for cb in self.valid_callbacks:
                cb.start(self, self.state)

            for chunk in valid_chunk_gen:
                with fut.OneShotTimer() as timer:
                    vouts = self.validator.valid_chunk(chunk)
                log.info('valid %f instances/s', len(chunk)/timer.elapsed)
                for cb in self.valid_callbacks:
                    cb.chunk(self, self.state, chunk, vouts)

            for cb in self.valid_callbacks:
                cb.end(self, self.state)


        def _checkpoint():
            if self.state['itr_ctr'] == self.restored_itr:
                return

            if (self.state['itr_ctr'] - self.state['last_cpt_itr']) < self.expt.SAVE_EVERY_NTH:
                return

            log.info('itr %d, checkpointing', self.state['itr_ctr'])
            self.state['last_cpt_itr'] = self.state['itr_ctr']

            cpt = self.model.weights_to_checkpoint()
            self.checkptr.save(cpt, self.state)


        def _check_cancel():
            stopfile = self.env['results_dir']/'STOP'
            if stopfile.exists():
                log.warn('STOP file found. stopping.')
                stopfile.remove()
                sys.exit(0)


        log.info('<EPOCH %d>', epoch)
        # create a new stream for each epoch

        chunk_size = self.expt.TRAIN_BATCH_SIZE*self.expt.TRAIN_BATCHES_PER_CHUNK
        train_chunk_gen = self.train_pump.chunked_array_gen(self.data_spec, chunk_size)

        for cb in self.train_callbacks:
            cb.start(self, self.state)

        for chunk in train_chunk_gen:
            # TODO logic for epoch?
            # TODO logic for aligning batch?
            _train(chunk)
            _validation()
            _checkpoint()
            _check_cancel()
            self.state['itr_ctr'] += chunk.shape[0]

        for cb in self.train_callbacks:
            cb.end(self, self.state)

        # TODO recover epochs
        #self.state['epoch'] = epoch
        log.info('</EPOCH %d>', epoch)


    @cached_property
    def data_spec(self):
        """ for data loading purposes, specifies type of
        buffers data will be kept in
        """
        spec = [(self.expt.X_KEY, 'float32', self.expt.X_SHAPE[1:]),
                (self.expt.Y_KEY, 'float32', self.expt.Y_SHAPE[1:])]
        return spec


    def train_init(self):
        self.env['results_dir'].makedirs_p()
        if self.env['scratch_clean']:
            self.clean_results_dir()

        self.checkptr = checkpoints.PickleGzDir(self.env['results_dir'])
        fideo.logging_config(self.env['results_dir']/('train.log'))

        self.model = self.expt.build_model()

        dump_info.dump_expt_info(self, console_only=False)

        self.init_weights()

        # TODO decide whether to pass model or isolate more
        self.trainer = self.expt.build_trainer(self.model)

        # get training pipeline
        # TODO api for reset/looping/epoch events
        self.pipeline = self.expt.build_data_pipeline(self.env['data_base_dir'])

        if self.env['workers'] == 1:
            self.train_pump = SerialPump(self.pipeline.build_train_source(),
                                         self.pipeline.build_train_ops())
        else:
            log.info('initializing zmq pump')
            self.train_pump = ZmqPump(self.pipeline.build_train_source(),
                                      self.pipeline.build_train_ops(),
                                      workers=self.env['workers'],
                                      queue_size=self.env['qsize'])

        if hasattr(self.expt, 'build_validator'):
            log.info('building validator')
            self.validation_enabled = True
            self.validator = self.expt.build_validator(self.model)
            # TODO should we allow parallel validation as well?
            self.valid_pump = SerialPump(self.pipeline.build_valid_source(),
                                         self.pipeline.build_valid_ops())
        else:
            log.warn('no validation')
            self.validation_enabled = False

        # default callbacks
        self.train_callbacks.extend([monitoring.PrintCb('train'),
                                     monitoring.TrainingLoggerCb(self.env['results_dir']/'trainlog.pkl')])

        #self.valid_callbacks.extend([monitoring.PrintCb('valid'),
        #                             monitoring.TrainingLoggerCb(self.env['results_dir']/'validlog.pkl')])


        if hasattr(self.expt, 'build_train_callbacks'):
            self.train_callbacks.extend(self.expt.build_train_callbacks())

        if hasattr(self.expt, 'build_valid_callbacks'):
            self.valid_callbacks.extend(self.expt.build_valid_callbacks())

        # ready to run
        self.state['itr_ctr'] = self.restored_itr
        self.state['last_valid_itr'] = self.state['itr_ctr']
        self.state['last_cpt_itr'] = self.state['itr_ctr']
        log.info('starting at itr_ctr = %d', self.state['itr_ctr'])
