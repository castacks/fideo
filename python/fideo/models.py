
import cPickle as pickle
import logging
from collections import OrderedDict
from itertools import izip
from funcy import cached_property, memoize


import theano
import theano.tensor as T
import lasagne
from lasagne.regularization import regularize_network_params
from lasagne.updates import norm_constraint as norm_constraint_update
from lasagne.objectives import binary_crossentropy
from lasagne.objectives import categorical_crossentropy
from lasagne.objectives import binary_accuracy
from lasagne.objectives import categorical_accuracy
from lasagne.objectives import aggregate


from fideo.lasagne_util.expr2d import accuracy_2d
from fideo.lasagne_util.expr2d import binary_accuracy_2d
from fideo.lasagne_util.expr2d import logspace_binary_logloss
from fideo.lasagne_util.expr2d import logspace_logloss_2d_stable

from fideo.lasagne_util.expr2d import masked_accuracy_2d
from fideo.lasagne_util.expr2d import masked_logspace_binary_logloss
from fideo.lasagne_util.expr2d import masked_logspace_logloss_2d_stable


log = logging.getLogger(__name__)


class SupervisedModel(object):
    """
    Model holds network topology and loss, including regularization.
    Holds theano variables.
    Provides output expression.
    Provides updates expression.
    """


    @cached_property
    def compiled_out(self):
        return theano.function([self.x_var], self.out_expr)


    @cached_property
    def compiled_dout(self):
        return theano.function([self.x_var], self.dout_expr)


    @cached_property
    def compiled_dump(self):
        dumpdict = {}
        for layer in self.net.get_all_layers():
            dumpdict[layer.name] = lasagne.layers.get_output(layer, self.x_var, deterministic=True)
        out = theano.function([self.x_var], dumpdict)
        def ordered_dump(x):
            unordered = out(x)
            ordered = OrderedDict()
            for k in self.net.layers.keys():
                ordered[k] = unordered[k]
            return ordered
        return ordered_dump


    def weights_from_checkpoint(self, checkpt):
        weights = checkpt['weights']
        self.net.set_weights(weights)


    def weights_to_checkpoint(self):
        return {'weights' : self.net.get_weights()}


    @cached_property
    def losses(self):
        loss_dict = self._get_losses_dict()

        loss_weights, loss_exprs = zip(*loss_dict.values())

        losses = OrderedDict()
        for name, lw, le in izip(loss_dict.iterkeys(),
                                 loss_weights,
                                 loss_exprs):
            losses[name] = lw*le

        total_loss_expr = T.cast(T.dot(loss_weights, loss_exprs), 'float32')
        losses['loss'] = total_loss_expr
        #losses['loss'] = loss_dict['ce'][1] + loss_dict['l2'][1]*self.regularization_weight
        return losses


    @cached_property
    def updates(self):
        """ TODO probably move updater stuff elsewhere?
        """
        total_loss_expr = self.losses['loss']
        params = self.net.get_all_params(trainable=True)
        updates = self.updater(total_loss_expr, params)
        return updates


    def dump_info(self, info_cb):
        info_cb('net_tbl_ascii', self.net.repr_net_table(), console=True)
        info_cb('net_tbl', self.net.repr_net_table(tablefmt=None), console=False)
        info_cb('num_params', self.net.count_params(), console=True)




class Classification2d(SupervisedModel):
    """ 1-of-K classification, input 2D output 1D
    """

    def __init__(self,
                 x_shape,
                 num_classes,
                 net,
                 updater,
                 regularization_weight=0.0001):

        self.x_shape = x_shape
        self.num_classes = num_classes
        assert(self.num_classes > 0)

        self.net = net
        self.updater = updater

        self.regularization_weight = regularization_weight
        self._init_theano()


    def _init_theano(self):
        """
        Init theano variables.
        """

        self.x_var = T.ftensor4('x')
        self.y_var = T.fmatrix('y') # B x 1 matrix

        self.out_expr = lasagne.layers.get_output(self.net.output_layer, self.x_var)
        self.dout_expr = lasagne.layers.get_output(self.net.output_layer, self.x_var, deterministic=True)


    def _get_losses_dict(self):
        if self.num_classes==1:
            out_loss = aggregate(binary_crossentropy(self.out_expr, T.cast(self.y_var, 'int32').flatten()))
        else:
            out_loss = aggregate(categorical_crossentropy(self.out_expr, T.cast(self.y_var, 'int32').flatten()))
            # out_loss = fideo.expr.log_softmax(self.out_expr, T.cast(self.y_var, 'int32'))
            # out_loss = T.nnet.categorical_crossentropy(self.out_expr, T.cast(self.y_var.squeeze(), 'int32')).mean()

        l2_norm = regularize_network_params(self.net.output_layer, lasagne.regularization.l2)
        loss_dict = OrderedDict()
        loss_dict['ce'] = (1.0, out_loss)
        # TODO regularization_weight
        loss_dict['l2'] = (self.regularization_weight, l2_norm)
        return loss_dict


    @cached_property
    def accuracy(self):
        if self.num_classes==1:
            acc = aggregate(binary_accuracy(self.out_expr, T.cast(self.y_var, 'int32').flatten()))
        else:
            acc = aggregate(categorical_accuracy(self.out_expr, T.cast(self.y_var, 'int32').flatten()))
        return acc




class SemanticSegmentation2d(SupervisedModel):
    """
    1-of-K pixelwise classification, input 2D, output 2D.
    NOTE: if you say 1 class, different loss
    function will be used. It will be binary cross entropy.
    This is theoretically same as 2-class multiclass CE
    but might be faster.

    """

    def __init__(self,
                 x_shape,
                 yhat_shape,
                 num_classes,
                 masked,
                 net,
                 updater,
                 regularization_weight=0.0001
                 ):

        self.x_shape = x_shape
        self.yhat_shape = yhat_shape
        self.num_classes = num_classes
        assert(self.num_classes > 0)
        self.masked = masked

        self.net = net
        self.updater = updater

        assert(len(self.x_shape)==4)
        assert(len(self.yhat_shape)==4)

        self.regularization_weight = regularization_weight

        self._init_theano()


    def _init_theano(self):
        """
        Init theano variables.
        """

        self.x_var = T.ftensor4('x')
        #self.y = T.itensor4('y')
        self.y_var = T.ftensor4('y')

        self.out_expr = lasagne.layers.get_output(self.net.output_layer, self.x_var)
        self.dout_expr = lasagne.layers.get_output(self.net.output_layer, self.x_var, deterministic=True)


    def _get_losses_dict(self):
        if self.num_classes==1:
            if self.masked:
                out_loss = masked_logspace_binary_logloss(self.out_expr, T.cast(self.y_var, 'int32'))
            else:
                out_loss = logspace_binary_logloss(self.out_expr, T.cast(self.y_var, 'int32'))
        else:
            if self.masked:
                out_loss = masked_logspace_logloss_2d_stable(self.out_expr, T.cast(self.y_var, 'int32'))
            else:
                out_loss = logspace_logloss_2d_stable(self.out_expr, T.cast(self.y_var, 'int32'))

        l2_norm = regularize_network_params(self.net.output_layer, lasagne.regularization.l2)
        loss_dict = OrderedDict()
        loss_dict['ce'] = (1.0, out_loss)
        # TODO regularization_weight
        loss_dict['l2'] = (self.regularization_weight, l2_norm)
        return loss_dict


    @cached_property
    def losses(self):
        loss_dict = self._get_losses_dict()

        loss_weights, loss_exprs = zip(*loss_dict.values())

        losses = OrderedDict()
        for name, lw, le in izip(loss_dict.iterkeys(),
                                 loss_weights,
                                 loss_exprs):
            losses[name] = lw*le

        total_loss_expr = T.cast(T.dot(loss_weights, loss_exprs), 'float32')
        losses['loss'] = total_loss_expr
        #losses['loss'] = loss_dict['ce'][1] + loss_dict['l2'][1]*self.regularization_weight
        return losses


    @cached_property
    def updates(self):
        """ TODO probably move updater stuff elsewhere?
        """
        total_loss_expr = self.losses['loss']
        params = self.net.get_all_params(trainable=True)
        updates = self.updater(total_loss_expr, params)
        return updates


    @cached_property
    def accuracy(self):
        if self.num_classes==1:
            if self.masked:
                raise Exception('no binary masked accuracy')
            else:
                acc = binary_accuracy_2d(self.out_expr, T.cast(self.y_var, 'int32'))
        else:
            if self.masked:
                acc = masked_accuracy_2d(self.out_expr, T.cast(self.y_var, 'int32'))
            else:
                acc = accuracy_2d(self.out_expr, T.cast(self.y_var, 'int32'))
        return acc
