
import logging
import math
import copy
from collections import OrderedDict

from funcy import first, izip_values
import numpy as np
import theano
import theano.tensor as T

from fideo.util import make_shared
import fideo.util.timing as fut


log = logging.getLogger(__name__)


class ChunkedTrainer(object):
    """

    Manages updates using data chunks, where each chunk has various batches.

    Creates a train_chunk() method that accepts a dictionary mapping a variable
    name to an array.

    Inputs:
    - names and types input variables
    - theano update expression
    - names and expression for outputs (loss)
    - batch size, batches per chunk

    train_inputs: input vars, name -> expr dictionary
    train_outputs is a name -> expr dictionary
    train_updates is a param -> expr dictionary
    """


    def __init__(self,
                 batch_size,
                 batches_per_chunk,
                 train_inputs,
                 train_outputs,
                 train_updates):

        self.batch_size = batch_size
        self.batches_per_chunk = batches_per_chunk

        # create shared variables for chunk inputs
        self.train_inputs = train_inputs

        self.shared_vars = {name: make_shared(var.ndim,
                                              var.dtype,
                                              var.name+'_shared')
                            for name, var in self.train_inputs.items()}

        # keep names for when we do output
        self.train_outputs = train_outputs

        # create aux variables for batching
        b_ix = T.iscalar('batch_index')
        b_slice = slice(b_ix*batch_size, (b_ix+1)*batch_size)

        givens = {var: svar[b_slice] \
                  for var, svar in \
                  izip_values(self.train_inputs, self.shared_vars)}

        self.train_fn = theano.function(inputs=[b_ix],
                                        outputs=train_outputs.values(),
                                        updates=train_updates,
                                        givens=givens)


    def _chunk_sanity_checks(self, chunk):
        for name in self.train_inputs.iterkeys():
            assert(name in chunk.dtype.names)
            # TODO shape checks


    def train_chunk(self, chunk):
        self._chunk_sanity_checks(chunk)

        # iterate over batches. TODO nonzero remainder case
        num_batches = int(math.floor(chunk.shape[0]/float(self.batch_size)))

        num_inst = self.batches_per_chunk*num_batches
        desired_inst = self.batches_per_chunk*self.batch_size

        if num_inst < desired_inst:
            extra_instances = desired_inst - num_inst
            new_chunk = np.zeros(desired_inst, dtype=chunk.dtype)
            for i in range(desired_inst):
                ix = np.random.randint(len(chunk))
                new_chunk[i] = chunk[ix]
            chunk = new_chunk

        num_batches = int(math.floor(chunk.shape[0]/float(self.batch_size)))
        assert(self.batches_per_chunk == num_batches)

        for name in chunk.dtype.names:
            # print(name, self.shared_vars[name].get_value().shape)
            self.shared_vars[name].set_value(chunk[name])

        #log.info('instances = %d, batches = %d', len(chunk), num_batches)

        # do the training, output as list of lists
        out_lst = map(np.asarray, map(self.train_fn, range(num_batches)))

        #with fut.OneShotTimer() as timer:
        #log.info('clf only timing: %f inst/s', len(chunk)/timer.elapsed)

        # convert to structured array. TODO assuming float32 for now
        dtype = [(name, 'float32') for name in self.train_outputs.keys()]

        out_arr = np.array(map(tuple, out_lst), dtype=dtype)
        return out_arr
