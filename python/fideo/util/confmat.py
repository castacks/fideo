"""
utilities for confusion matrices of 2D outputs.
"""

#from collections import OrderedDict

import warnings

import pandas as pd
import numpy as np

try:
    from fideo.accum_confmat import (_accum_confmat,
                                     _accum_confmat_batch)
except ImportError:
    warnings.warn('no cython version of accum_confmat')
    _accum_confmat = None
    _accum_confmat_batch = None


def accum_confmat_py(gt, pred, cm):
    """ gt, pred, cm
    gt, pred shapes are HxW (no channels)
    By definition a confusion matrix :math:`C` is such that :math:`C_{i, j}`
    is equal to the number of observations known to be in group :math:`i` but
    predicted to be in group :math:`j`.
    """
    K = cm.shape[0]
    for gt_lbl in range(K):
        for pred_lbl in range(K):
            cmij = ((gt==gt_lbl) & (pred==pred_lbl)).sum()
            cm[gt_lbl, pred_lbl] += cmij


def accum_confmat_batch_py(gt, pred, cm):
    """ gt, pred, cm
    gt, pred shapes are BxHxW (no channels)
    By definition a confusion matrix :math:`C` is such that :math:`C_{i, j}`
    is equal to the number of observations known to be in group :math:`i` but
    predicted to be in group :math:`j`.
    Same as accum_confmat_py, but gt and pred are now in a batch (so e.g.
    ith instance is gt[i]).

    note: actually same as accum_confmat_py
    """
    K = cm.shape[0]
    for gt_lbl in range(K):
        for pred_lbl in range(K):
            cmij = ((gt==gt_lbl) & (pred==pred_lbl)).sum()
            cm[gt_lbl, pred_lbl] += cmij


def metrics_from_confmat(cm, masked):
    if masked:
        offset = 1
    else:
        offset = 0

    df = pd.DataFrame()

    total = cm[offset:, offset:].sum()

    for k in range(masked, cm.shape[0]):
        # 'binarize' cm
        tp = cm[k, k]

        # false positives: predicted as k but not k
        fp = cm[offset:, k].sum() - cm[k, k]

        # false negatives: actually k but not predicted as k
        fn = cm[k, offset:].sum() - cm[k, k]

        # true negatives: not k and not predicted as k
        tn = total - (tp + fp + fn)

        if tp + fp + fn == 0:
            iou = 0.
        else:
            iou = float(tp)/float(tp + fp + fn)

        if tp + fn == 0:
            recall = 0.
        else:
            recall = float(tp)/(tp + fn)

        if tp + fp == 0:
            precision = 0.
        else:
            precision = float(tp)/(tp + fp)

        if tp + tn + fp + fn == 0:
            acc = 0.
        else:
            acc = float(tp + tn)/(tp + tn + fp + fn)

        support = cm[k, offset:].sum()

        #out.append([k, iou, precision, recall, acc, support])
        df[k] = [iou, precision, recall, acc, support]

    df = df.T
    df.columns = ['iou', 'precision', 'recall', 'accuracy', 'support']
    return df


def metrics_from_confmat2(cm, masked):
    """ TODO. consider formulas from fcn paper instead:

    n_cl = num of classes in GT
    n_ij = num of pixels of class i predicted to belong to class j
    t_i = total number of pixels of class i in GT

    * pixel acc.
    (sum_i n_ii) / (sum_i t_i)

    * mean acc.
    (1/n_cl) * (sum_i (n_ii/t_i))

    * mean iu
    (1/n_cl) * sum_i [ n_ii / (t_i + (sum_j n_ji) - n_ii) ]

    * freq weighted iu
    1/(sum_k t_k) * sum_i[(t_i * n_ii)/(t_i + (sum_j n_ji) - n_ii)]

    """

    if masked:
        offset = 0
    else:
        offset = 1

    df = pd.DataFrame()

    for k in range(masked, cm.shape[0]):

        # 'binarize' cm
        tp = cm[k, k]

        # false positives: predicted as k but not k
        fp = cm[offset:, k].sum() - cm[k, k]

        # false negatives: actually k but not predicted as k
        fn = cm[k, offset:].sum() - cm[k, k]

        # true negatives: not k and not predicted as k
        tn = cm[offset:, offset:].sum() - cm[offset:, k].sum() - cm[k, offset:].sum() + cm[k, k]

        if tp + fp + fn == 0:
            iou = 0.
        else:
            iou = float(tp)/float(tp + fp + fn)

        if tp + fn == 0:
            recall = 0.
        else:
            recall = float(tp)/(tp + fn)

        if tp + fp == 0:
            precision = 0.
        else:
            precision = float(tp)/(tp + fp)

        if tp + tn + fp + fn == 0:
            acc = 0.
        else:
            acc = float(tp + tn)/(tp + tn + fp + fn)

        support = cm[k, offset:].sum()

        #out.append([k, iou, precision, recall, acc, support])
        df[k] = [iou, precision, recall, acc, support]

    df = df.T
    df.columns = ['iou', 'precision', 'recall', 'accuracy', 'support']
    return df


if _accum_confmat is None:
    accum_confmat = accum_confmat_py
else:
    accum_confmat = _accum_confmat

if _accum_confmat_batch is None:
    accum_confmat_batch = accum_confmat_batch_py
else:
    accum_confmat_batch = _accum_confmat_batch
