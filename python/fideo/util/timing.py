from __future__ import print_function

import time
import logging
from collections import defaultdict

import pandas as pd


__all__ = ['OneShotTimer',
           'AggregatedTimer']


class OneShotTimer(object):

    def __init__(self, name='', printfn=None, callback=None):
        self.name = name
        self.printfn = printfn
        self.callback = callback


    def __enter__(self):
        self.tic = time.time()
        return self


    def __exit__(self, exctype, excvalue, traceback):
        self.toc = time.time()
        self.elapsed = (self.toc - self.tic)
        if self.printfn is not None:
            self.printfn('%s (s): %f'%(self.name, self.elapsed))
        if self.callback is not None:
            self.callback(self.name, self.elapsed)




class AggregatedTimer(object):

    def __init__(self):
        self.times = defaultdict(lambda: list())


    def _callback(self, name, elapsed):
        self.times[name].append(elapsed)


    def timer(self, name):
        return OneShotTimer(name,
                            printfn=None,
                            callback=self._callback)


    def report(self, printfn=print):
        self.summary = pd.DataFrame()
        for name, tlist in self.times.iteritems():
            # TODO consider using index w/ start time
            ts = pd.Series(tlist)
            #summary[name] = [ts.mean(), ts.std(), ts.min(), ts.median(), ts.max()]
            self.summary[name] = ts.describe()
        # TODO is this ok
        pd.set_option('display.width', 200)
        printfn(self.summary.T)
        pd.reset_option('display.width')

if __name__ == "__main__":
    with OneShotTimer() as timer:
        time.sleep(0.2)
    print(timer.elapsed)

