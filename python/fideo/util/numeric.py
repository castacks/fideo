
import numpy as np
import sklearn.metrics as skm


def confmat(output, y_target):
    outputf = output.argmax(axis=1).flatten()
    y_targetf = y_target.flatten()
    cm = skm.confusion_matrix(y_targetf, outputf)
    return cm


def masked_confmat(output, y_target):
    y_targetf = y_target.flatten()
    nz_ix = np.nonzero(y_targetf)
    nz_ytargetf = y_targetf[nz_ix]-1
    nz_outputf = output.argmax(axis=1).flatten()[nz_ix]
    cm = skm.confusion_matrix(nz_ytargetf, nz_outputf)
    return cm
