
import numpy as np
import theano

__all__ = ['make_shared']


def make_shared(ndim, dtype, name):
    shp = tuple([1] * ndim)
    return theano.shared(np.zeros(shp, dtype=dtype), name=name)
