from __future__ import print_function

import logging
import sys
import numpy as np
from funcy import first

import pandas as pd

from .cb import MonitorCb


class PrintCb(MonitorCb):

    def __init__(self, name):
        self.name = name
        self.log = logging.getLogger(name)


    def chunk(self, app, state, chunk, out):
        df = pd.DataFrame(out).mean()
        s = ', '.join(['%s: %.4f'%(x, df[x]) for x in df.keys()])
        self.log.info('itr %d| %s', state['itr_ctr'], s)
