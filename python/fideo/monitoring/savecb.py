from __future__ import print_function

from collections import OrderedDict
import logging
import sys
import numpy as np
from funcy import first

from pycolor.distinct_colors import distinct_colors
from pycolor import colour
from pyimg2 import remapping
import cv2
from PIL import Image

from .cb import MonitorCb


class SaveImageLabelCb(MonitorCb):

    def __init__(self,
                 name,
                 x_key=None,
                 y_key=None,
                 masked=True,
                 num_classes=None,
                 palette=None,
                 every_nth=1,
                 ext='jpg'):

        self.name = name
        if palette is None:
            if num_classes is None:
                # reasonable upper bound?
                num_classes = 256
            palette = OrderedDict()
            # class 0 is invalid/black; we add 1
            for ix in xrange(num_classes+1):
                hex_color = distinct_colors[ix]
                rgb_color = colour.hex2rgb(hex_color)
                palette[ix] = [int(rgb_color[0]*255),
                               int(rgb_color[1]*255),
                               int(rgb_color[2]*255)]

        self.x_key = x_key
        self.y_key = y_key
        self.masked = masked
        self.palette = palette
        self.every_nth = every_nth
        self.ext = ext
        assert(self.ext in ('jpg', 'png'))


    def start(self, app, state):
        self.ctr = 0


    def chunk(self, app, state, chunk, out):
        import fideo.viz_util

        if (self.ctr % self.every_nth) != 0:
            self.ctr += 1
            return

        x_data = chunk[self.x_key]
        y_data = chunk[self.y_key]

        # sample an image
        r_ix = np.random.randint(len(x_data))
        y = y_data[r_ix, 0].astype('int32')
        x = x_data[r_ix:r_ix+1]
        yhat = app.model.compiled_dout(x)[0]

        #yhat = fideo.util.softmax(yhat)

        yhat_argmax = np.argmax(yhat, 0)
        if self.masked:
            yhat_argmax += 1 # network has no notion of 0

        xviz = fideo.viz_util.unpreprocess(x_data[r_ix])

        # colorize images
        yviz = remapping.label_image_to_rgb_image(y, self.palette)
        yhat_viz = remapping.label_image_to_rgb_image(yhat_argmax, self.palette)

        # yviz = cv2.cvtColor(yviz, cv2.COLOR_BGR2RGB)
        # yhat_viz = cv2.cvtColor(yhat_viz, cv2.COLOR_BGR2RGB)

        # resize if necessary
        if yviz.shape != xviz.shape:
            yviz = cv2.resize(yviz, (xviz.shape[1], xviz.shape[0]), None, 0, 0, cv2.INTER_NEAREST)

        if yhat_viz.shape != xviz.shape:
            yhat_viz = cv2.resize(yhat_viz, (xviz.shape[1], xviz.shape[0]), None, 0, 0, cv2.INTER_NEAREST)

        # blendy blends
        yviz_blend = cv2.addWeighted(yviz, 0.5, xviz, 0.5, 0.)
        yhat_viz_blend = cv2.addWeighted(yhat_viz, 0.5, xviz, 0.5, 0.)

        # witout blend row
        xyviz0 = np.concatenate((xviz, yviz, yhat_viz), 1)
        # with blend row
        xyviz1 = np.concatenate((xviz, yviz_blend, yhat_viz_blend), 1)
        # cat
        xyviz = np.concatenate((xyviz0, xyviz1), 0)

        #pydisp.image(xyviz, win=self.name, title=self.name)

        out_fname = app.env['results_dir']/('%s_imglbl_%09d_%05d.%s'%(self.name, state['itr_ctr'], self.ctr, self.ext))
        Image.fromarray(xyviz.astype('u1')).save(str(out_fname))

        #cv2.imwrite(out_fname, xyviz.astype('u1'))
        self.ctr += 1



class SaveBinaryImageLabelCb(MonitorCb):

    def __init__(self,
                 name,
                 x_key=None,
                 y_key=None,
                 every_nth=1,
                 ext='jpg'):

        self.name = name
        self.x_key = x_key
        self.y_key = y_key
        self.every_nth = every_nth
        self.ext = ext
        assert(self.ext in ('jpg', 'png'))


    def start(self, app, state):
        self.ctr = 0


    def chunk(self, app, state, chunk, out):
        import fideo.viz_util

        if (self.ctr % self.every_nth) != 0:
            self.ctr += 1
            return

        x_data = chunk[self.x_key]
        y_data = chunk[self.y_key]

        channels = y_data.shape[1]

        # sample an image from batch
        r_ix = np.random.randint(len(x_data))
        x = x_data[r_ix:r_ix+1]

        # classify and squeeze out batch dim
        all_yhat = app.model.compiled_dout(x).squeeze(0)
        #all_yhat = app.model.compiled_dout(x).squeeze((0, 1))

        # TODO more stable sigmoid
        all_yhat = 1./(1 + np.exp(-all_yhat))

        for channel in range(channels):
            y = y_data[r_ix, channel].astype('int32')
            yhat = all_yhat[channel]

            xviz = fideo.viz_util.unpreprocess(x_data[r_ix])
            # TODO other colormaps
            yviz = (y*255).clip(0, 255).astype('u1')
            yhat_viz = (yhat*255).clip(0, 255).astype('u1')

            # three-channel
            yviz = np.dstack((yviz,)*3)
            yhat_viz = np.dstack((yhat_viz,)*3)

            # resize if necessary
            if yviz.shape != xviz.shape:
                yviz = cv2.resize(yviz, (xviz.shape[1], xviz.shape[0]), None, 0, 0, cv2.INTER_NEAREST)

            if yhat_viz.shape != xviz.shape:
                yhat_viz = cv2.resize(yhat_viz, (xviz.shape[1], xviz.shape[0]), None, 0, 0, cv2.INTER_NEAREST)

            # blendy blends
            yviz_blend = cv2.addWeighted(yviz, 0.5, xviz, 0.5, 0.)
            yhat_viz_blend = cv2.addWeighted(yhat_viz, 0.5, xviz, 0.5, 0.)

            # witout blend row
            xyviz0 = np.concatenate((xviz, yviz, yhat_viz), 1)
            # with blend row
            xyviz1 = np.concatenate((xviz, yviz_blend, yhat_viz_blend), 1)
            # cat
            xyviz = np.concatenate((xyviz0, xyviz1), 0)

            #pydisp.image(xyviz, win=self.name, title=self.name)

            out_fname = app.env['results_dir']/('%s_imglbl_%09d_%05d_%02d.%s'%(self.name, state['itr_ctr'], self.ctr, channel, self.ext))
            Image.fromarray(xyviz.astype('u1')).save(str(out_fname))

        #cv2.imwrite(out_fname, xyviz.astype('u1'))
        self.ctr += 1


