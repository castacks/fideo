
import logging
from collections import OrderedDict
import time
import cPickle as pickle

from spath import Path

import pandas as pd

from .cb import MonitorCb

log = logging.getLogger(__name__)

# TODO option to use msgpack backend
from .training_log_pickle import TrainingLogger


class TrainingLoggerCb(MonitorCb):

    def __init__(self, fname, reinitialize=False):
        self.tlogger = TrainingLogger(fname, reinitialize)


    def chunk(self, app, state, chunk, out):
        df = pd.DataFrame(out).mean()
        record = OrderedDict()
        record['itr_ctr'] = state['itr_ctr']
        for k in df.keys():
            record[k] = df[k]
        self.tlogger.log(record)
