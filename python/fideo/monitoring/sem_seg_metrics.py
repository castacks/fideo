
import cPickle as pickle
from collections import OrderedDict
import numpy as np

from .cb import MonitorCb

from ..util.confmat import (accum_confmat_batch,
                            metrics_from_confmat)

import tabulate
#import pickleshare

class SemSegMetrics(MonitorCb):

    def __init__(self,
                 fname,
                 x_key,
                 y_key,
                 yhat_key,
                 num_classes,
                 masked=False):
        self.fname = fname
        self.x_key = x_key
        self.y_key = y_key
        self.yhat_key = yhat_key
        self.num_classes = num_classes
        # for binary problems we sometimes use num_classes=1
        self.binary = (num_classes==1)
        self.masked = masked


    def start(self, app, state):
        if self.masked:
            self.confmat = np.zeros((self.num_classes+1, self.num_classes+1), 'f8')
        elif self.binary:
            self.confmat = np.zeros((2, 2), 'f8')
        else:
            self.confmat = np.zeros((self.num_classes, self.num_classes), 'f8')


    def chunk(self, app, state, chunk, out):
        x = chunk[self.x_key]
        y = chunk[self.y_key].astype('i4').squeeze(1)
        yhat = out[self.yhat_key]

        #yhat_softmax = yhat/np.sum(np.exp(yhat), 1)

        if self.binary:
            yhat_argmax = (yhat > 0.5).astype('i4').squeeze(1)
        else:
            yhat_argmax = np.argmax(yhat, 1).astype('i4')
            if self.masked:
                yhat_argmax += 1

        accum_confmat_batch(y, yhat_argmax, self.confmat)


    def end(self, app, state):
        df = metrics_from_confmat(self.confmat, self.masked)
        print(tabulate.tabulate(df, df.columns))

        # db = pickleshare.PickleShareDB(app.env['results_dir']/self.fname)

        record = {}
        record['metrics'] = df
        record['cm'] = self.confmat.copy()

        out_fname = app.env['results_dir']/(self.fname)
        with open(out_fname, 'a') as f:
            pickle.dump(record, f, protocol=pickle.HIGHEST_PROTOCOL)
