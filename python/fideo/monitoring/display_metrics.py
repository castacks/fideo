"""
TODO not sure if this is not in a functional state;
version in limbo/ is.
"""

import numpy as np
import pandas as pd

try:
    import pydisp
except ImportError, ex:
    warnings.warn('pydisp is not installed')
    pydisp = None


class DisplayMetrics(object):
    """ show metrics from training log in fname with pydisplay
    """
    def __init__(self, fname, keys, last_start=False, use_mpl=False):
        self.fname = fname
        self.keys = keys
        self.last_start = last_start
        self.use_mpl = use_mpl


    def set_matplotlib_params(self):
        from matplotlib import rcParams
        rcParams['axes.labelsize'] = 12
        rcParams['xtick.labelsize'] = 12
        rcParams['ytick.labelsize'] = 12
        rcParams['legend.fontsize'] = 12
        #rcParams['font.family'] = 'serif'
        #rcParams['font.serif'] = ['Computer Modern Roman']
        #rcParams['text.usetex'] = True
        #rcParams['figure.figsize'] = 7.3, 4.2
        golden_ratio = (np.sqrt(5)-1.0)/2.0
        fig_width_in = 8.
        fig_height_in = fig_width_in * golden_ratio
        #fig_dims = [fig_width_in, fig_height_in]
        rcParams['figure.figsize'] = [fig_width_in, fig_height_in]


    def __call__(self, app, state, out):
        # note we ignore args; just read out logs.
        # TODO should we grab config from app instead?

        # TODO figure out how to do the import so it'll work regardless of order
        # OR, look at flaskplotlib example in snippets
        import matplotlib; matplotlib.use('Agg')
        import matplotlib.pyplot as pl

        if pydisp is None:
            log.error('pydisplay is not available and required for DisplayMetrics')
            return
        recs = list(fideo.training_log.read_records(self.fname))
        log.info('found {} recs'.format(len(recs)))
        to_stamp = lambda r: np.datetime64(datetime.fromtimestamp(r['_stamp']))
        stamps = map(to_stamp, recs)
        df = pd.DataFrame(recs, index=stamps)
        del df['_stamp']
        for k in self.keys:
            df[k] = df[k].astype(np.float)
        df = df.sort_index()

        # TODO this loses resolution on loss

        if last_start:
            ix0 = df[(df['itr_ctr'].diff() < 0)].index[-1]
            df = df.loc[ix0:]

        #loss = df['loss'].sort_index()
        #loss.index = df.sort_index()['itr_ctr']

        df = df.dropna(axis=0)
        # df = df[df['loss'] < 10.]
        if self.use_mpl:
            for k in self.keys:
                smoothing_window = 32
                self.set_matplotlib_params()
                fig = pl.figure()
                val = df[k].astype(np.float)
                val.plot(label='raw')
                pd.rolling_mean(val, smoothing_window).plot(label='smoothed')
                pl.xlabel('Iter')
                pl.ylabel(k)
                pl.legend()
                fig.tight_layout(pad=0.1)
                pydisp.pylab(fig)

        else:
            for k in self.keys:
                data = df[['itr_ctr', k]].dropna(axis=0).astype(np.float32)
                log.info('plotting {}, {} values'.format(k, data.shape))
                # TODO update for pydisp API
                pydisp.dyplot(data.values, labels=['itr', k], win=k, title=k)
