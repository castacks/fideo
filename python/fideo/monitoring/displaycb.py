from __future__ import print_function

from collections import OrderedDict
import logging
import sys
import numpy as np
from funcy import first

import pydisp
from pycolor.distinct_colors import distinct_colors
from pycolor import colour
from pyimg2 import remapping
import cv2


class DisplayImageLabelCb(object):

    def __init__(self,
                 name,
                 x_key=None,
                 y_key=None,
                 num_classes=None,
                 palette=None):

        self.name = name
        if palette is None:
            if num_classes is None:
                # reasonable upper bound?
                num_classes = 256
            palette = OrderedDict()
            # class 0 is invalid/black; we add 1
            for ix in xrange(num_classes+1):
                hex_color = distinct_colors[ix]
                rgb_color = colour.hex2rgb(hex_color)
                palette[ix] = [int(rgb_color[0]*255),
                               int(rgb_color[1]*255),
                               int(rgb_color[2]*255)]

        self.palette = palette
        self.x_key = x_key
        self.y_key = y_key
        # TODO masked


    def __call__(self, app, state, chunk, out):
        import fideo.viz_util

        x_data = chunk[self.x_key]
        y_data = chunk[self.y_key]

        # sample an image
        r_ix = np.random.randint(len(x_data))
        y = y_data[r_ix, 0].astype('int32')
        x = x_data[r_ix:r_ix+1]
        yhat = app.model.compiled_dout(x)[0]

        # yhat = fideo.util.softmax(yhat)
        yhat_argmax = np.argmax(yhat, 0)+1 # network has no notion of 0

        xviz = fideo.viz_util.unpreprocess(x_data[r_ix])

        # colorize images
        #yviz = remapping.label_image_to_rgb_image(y, self.palette)
        #yhat_viz = remapping.label_image_to_rgb_image(yhat_argmax, self.palette)

        yviz = remapping.label_image_to_rgb_image(y, self.palette)
        yhat_viz = remapping.label_image_to_rgb_image(yhat_argmax, self.palette)

        # resize if necessary
        if yviz.shape != xviz.shape:
            yviz = cv2.resize(yviz, (xviz.shape[1], xviz.shape[0]), None, 0, 0, cv2.INTER_NEAREST)

        if yhat_viz.shape != xviz.shape:
            yhat_viz = cv2.resize(yhat_viz, (xviz.shape[1], xviz.shape[0]), None, 0, 0, cv2.INTER_NEAREST)

        # blendy blends
        yviz_blend = cv2.addWeighted(yviz, 0.5, xviz, 0.5, 0.)
        yhat_viz_blend = cv2.addWeighted(yhat_viz, 0.5, xviz, 0.5, 0.)

        # witout blend row
        xyviz0 = np.concatenate((xviz, yviz, yhat_viz), 1)
        # with blend row
        xyviz1 = np.concatenate((xviz, yviz_blend, yhat_viz_blend), 1)
        # cat
        xyviz = np.concatenate((xyviz0, xyviz1), 0)

        pydisp.image(xyviz, win=self.name, title=self.name)
