
import logging


log = logging.getLogger(__name__)


class PastalogMonitoring(object):
    """ callback to post on pastalog
    """

    def __init__(self, name, metrics=['lv'], port=8120):
        import pastalog
        self.metrics = metrics
        self.port = port
        self.pastalog_logger = pastalog.Log('http://localhost:{}'.format(port), name)


    def __call__(self, trainer, msg_dict):
        itr_ctr = int(msg_dict['itr_ctr'])
        try:
            # self.pastalog_logger.post('lv', value=float(msg_dict['lv']), step=itr_ctr)
            for k, v in msg_dict.items():
                if k in self.metrics:
                    self.pastalog_logger.post(k, value=float(v), step=itr_ctr)

            # self.pastalog_logger.post(mname, value=float(mval), step=itr_ctr)
        except IOError, ex:
            log.error('error with pastalog: {}'.format(ex))
