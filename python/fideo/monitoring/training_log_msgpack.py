
import logging
from collections import OrderedDict
import time

import msgpack
import msgpack_numpy; msgpack_numpy.patch()

from spath import Path

import pandas as pd

from .cb import MonitorCb

log = logging.getLogger(__name__)


class TrainingLogger(object):

    def __init__(self, fname, reinitialize=False):
        self.fname = Path(fname)
        self.reinitialize = reinitialize
        if self.fname.exists() and self.reinitialize:
            log.warn('{} exists, deleting'.format(self.fname))
            self.fname.remove()


    def log(self, record=None, **kwargs):
        if self.fname is None:
            raise Exception('fname not set in TrainingLogger')

        if record is None:
            record = OrderedDict()
        record.update(kwargs)
        record['_stamp'] = time.time()

        with open(self.fname, 'a') as f:
            f.write(msgpack.packb(record))


    def read_records(self):
        with open(self.fname, 'rb') as f:
            unpacker = msgpack.Unpacker(f)
            for msg in unpacker:
                yield msg
