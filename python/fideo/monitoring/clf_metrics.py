import cPickle as pickle
from collections import OrderedDict
import numpy as np

from .cb import MonitorCb

import tabulate
#import pickleshare


class ClassificationMetrics(MonitorCb):

    def __init__(self,
                 fname,
                 x_key,
                 y_key,
                 yhat_key,
                 num_classes):
        self.fname = fname
        self.x_key = x_key
        self.y_key = y_key
        self.yhat_key = yhat_key
        self.num_classes = num_classes


    def start(self, app, state):
        self.num_correct = 0
        self.num_instances = 0


    def chunk(self, app, state, chunk, out):
        x = chunk[self.x_key]
        y = chunk[self.y_key].astype('i4')
        yhat = out[self.yhat_key]

        assert(len(x)==len(y))
        assert(len(y)==len(yhat))

        #yhat_softmax = yhat/np.sum(np.exp(yhat), 1)
        yhat_argmax = np.argmax(yhat, 1).astype('i4')

        assert(len(yhat_argmax)==len(yhat))

        self.num_correct += (y == yhat_argmax).sum()
        self.num_instances += len(x)


    def end(self, app, state):
        accuracy = float(self.num_correct)/self.num_instances
        print('accuracy:', accuracy)

        # df = metrics_from_confmat(self.confmat, self.masked)
        # print(tabulate.tabulate(df, df.columns))
        # db = pickleshare.PickleShareDB(app.env['results_dir']/self.fname)

        record = {}
        record['itr_ctr'] = state['itr_ctr']
        record['metrics'] = {'accuracy': accuracy}
        # record['cm'] = self.confmat.copy()

        out_fname = app.env['results_dir']/(self.fname)
        with open(out_fname, 'a') as f:
            pickle.dump(record, f, protocol=pickle.HIGHEST_PROTOCOL)
