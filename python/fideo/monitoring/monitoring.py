


class DisplayBinaryLabels(object):

    def __init__(self):
        pass

    def __call__(self, trainer):
        import fideo.viz_util
        x_shared_tmp = trainer.x_shared_tmp
        y_shared_tmp = trainer.y_shared_tmp
        r_ix = np.random.randint(len(x_shared_tmp))
        yhat = np.asarray(trainer.tfunc['dout'](x_shared_tmp[r_ix][np.newaxis,...]))
        yhat = 1./(1.+np.exp(-yhat))

        xviz = fideo.viz_util.unpreprocess(x_shared_tmp[r_ix])
        yviz = pl.cm.jet(y_shared_tmp[r_ix,0], bytes=True)[...,:3]
        if yviz.shape != xviz.shape:
            yviz = cv2.resize(yviz, (xviz.shape[1], xviz.shape[0]), None, 0, 0, cv2.INTER_NEAREST)

        yhat_viz = pl.cm.jet(yhat[0,0], bytes=True)[...,:3]
        if yhat_viz.shape != xviz.shape:
            yhat_viz = cv2.resize(yhat_viz, (xviz.shape[1], xviz.shape[0]), None, 0, 0, cv2.INTER_NEAREST)

        yviz_blend = cv2.addWeighted(yviz, 0.5, xviz, 0.5, 0.)
        yhat_viz_blend = cv2.addWeighted(yhat_viz, 0.5, xviz, 0.5, 0.)
        # zero_img = np.zeros_like(xviz)

        xyviz0 = np.concatenate((xviz, yviz, yhat_viz), 1)
        xyviz1 = np.concatenate((xviz, yviz_blend, yhat_viz_blend), 1)
        xyviz = np.concatenate((xyviz0, xyviz1), 0)

        disp.image(xyviz, win='xyviz', title='xyviz')


class DisplayLabels(object):

    def __init__(self, num_classes=None, palette=None):
        if palette is None:
            if num_classes is None:
                # reasonable upper bound?
                num_classes = 256
            palette = OrderedDict()
            # class 0 is invalid/black; we add 1
            for ix in xrange(num_classes+1):
                hex_color = distinct_colors[ix]
                palette[ix] = colour.hex2rgb(hex_color, to_float=False)
        self.palette = palette

    def __call__(self, trainer):
        import fideo.viz_util
        x_shared_tmp = trainer.x_shared_tmp
        y_shared_tmp = trainer.y_shared_tmp
        # sample an image
        r_ix = np.random.randint(len(x_shared_tmp))
        y = y_shared_tmp[r_ix, 0].astype('int32')
        yhat = np.asarray(trainer.tfunc['dout'](x_shared_tmp[r_ix][None,...]))
        # yhat = fideo.util.softmax(yhat)
        yhat_argmax = np.argmax(yhat, 1)[0]+1 # network has no notion of 0

        xviz = fideo.viz_util.unpreprocess(x_shared_tmp[r_ix])
        # colorize images
        # import pdb; pdb.set_trace()
        yviz = remapping.label_image_to_rgb_image(y, self.palette)
        yhat_viz = remapping.label_image_to_rgb_image(yhat_argmax, self.palette)

        # resize if necessary
        if yviz.shape != xviz.shape:
            yviz = cv2.resize(yviz, (xviz.shape[1], xviz.shape[0]), None, 0, 0, cv2.INTER_NEAREST)
        if yhat_viz.shape != xviz.shape:
            yhat_viz = cv2.resize(yhat_viz, (xviz.shape[1], xviz.shape[0]), None, 0, 0, cv2.INTER_NEAREST)

        # blendy blends
        yviz_blend = cv2.addWeighted(yviz, 0.5, xviz, 0.5, 0.)
        yhat_viz_blend = cv2.addWeighted(yhat_viz, 0.5, xviz, 0.5, 0.)

        # witout blend row
        xyviz0 = np.concatenate((xviz, yviz, yhat_viz), 1)
        # with blend row
        xyviz1 = np.concatenate((xviz, yviz_blend, yhat_viz_blend), 1)
        # cat
        xyviz = np.concatenate((xyviz0, xyviz1), 0)

        disp.image(xyviz, win='xyviz', title='xyviz')



class DisplayImage(object):

    def __init__(self):
        pass

    def __call__(self, trainer):
        import fideo.viz_util
        x_shared_tmp = trainer.x_shared_tmp
        r_ix = np.random.randint(len(x_shared_tmp))
        xviz = fideo.viz_util.unpreprocess(x_shared_tmp[r_ix])
        disp.image(xviz, win='xviz', title='xviz')


class DisplayImageAndLabelVec(object):

    def __init__(self):
        pass

    def __call__(self, trainer):
        import fideo.viz_util
        x_shared_tmp = trainer.x_shared_tmp
        r_ix = np.random.randint(len(x_shared_tmp))
        xviz = fideo.viz_util.unpreprocess(x_shared_tmp[r_ix])
        y = trainer.y_shared_tmp[r_ix]
        yhat = trainer.tfunc['dout'](trainer.x_shared_tmp[r_ix][np.newaxis,...])
        # ytxt = str(trainer.y_shared_tmp[r_ix])
        ytxt = 'y = ' + str(y) + '<br> yhat = ' + str(yhat)
        disp.image(xviz, win='xviz', title='xviz')
        disp.text(ytxt, win='yhat_vec', title='yhat_vec')
