"""
adapted from lasagne/recipes
"""

from collections import OrderedDict

from lasagne.layers import InputLayer
from lasagne.layers import DenseLayer
from lasagne.layers import NonlinearityLayer
from lasagne.layers import DropoutLayer
from lasagne.layers import Pool2DLayer as PoolLayer
from lasagne.layers.dnn import Conv2DDNNLayer as ConvLayer
from lasagne.nonlinearities import softmax

from fideo.net import Net

def build_net(num_classes=1000, trainable=True, add_softmax=False):
    net = Net()
    net['input'] = InputLayer((None, 3, 224, 224))
    net['conv1_1'] = ConvLayer(net.tail, 64, 3, pad=1, flip_filters=False)
    net['conv1_2'] = ConvLayer(net.tail, 64, 3, pad=1, flip_filters=False)
    net['pool1'] = PoolLayer(net.tail, 2)
    net['conv2_1'] = ConvLayer(net.tail, 128, 3, pad=1, flip_filters=False)
    net['conv2_2'] = ConvLayer(net.tail, 128, 3, pad=1, flip_filters=False)
    net['pool2'] = PoolLayer(net.tail, 2)
    net['conv3_1'] = ConvLayer(net.tail, 256, 3, pad=1, flip_filters=False)
    net['conv3_2'] = ConvLayer(net.tail, 256, 3, pad=1, flip_filters=False)
    net['conv3_3'] = ConvLayer(net.tail, 256, 3, pad=1, flip_filters=False)
    net['pool3'] = PoolLayer(net.tail, 2)
    net['conv4_1'] = ConvLayer(net.tail, 512, 3, pad=1, flip_filters=False)
    net['conv4_2'] = ConvLayer(net.tail, 512, 3, pad=1, flip_filters=False)
    net['conv4_3'] = ConvLayer(net.tail, 512, 3, pad=1, flip_filters=False)
    net['pool4'] = PoolLayer(net.tail, 2)
    net['conv5_1'] = ConvLayer(net.tail, 512, 3, pad=1, flip_filters=False)
    net['conv5_2'] = ConvLayer(net.tail, 512, 3, pad=1, flip_filters=False)
    net['conv5_3'] = ConvLayer(net.tail, 512, 3, pad=1, flip_filters=False)
    net['pool5'] = PoolLayer(net.tail, 2)
    net['fc6'] = DenseLayer(net.tail, num_units=4096)
    net['fc6_dropout'] = DropoutLayer(net.tail, p=0.5)
    net['fc7'] = DenseLayer(net.tail, num_units=4096)
    if num_classes is not None:
        net['fc7_dropout'] = DropoutLayer(net.tail, p=0.5)
        net['fc8'] = DenseLayer(net.tail, num_units=num_classes, nonlinearity=None)
        if add_softmax:
            net['prob'] = NonlinearityLayer(net['fc8'], softmax)

    if not trainable:
        """ TODO I think most of the time, if we don't want to train the param
        we don't want to regularize it either. We will assume it's the case.
        """
        for layer in net.values():
            for param in layer.get_params():
                layer.params[param] = layer.params[param].difference(['trainable', 'regularizable'])

    return net
