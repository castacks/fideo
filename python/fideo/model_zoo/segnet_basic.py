
from lasagne.layers import InverseLayer
from lasagne.init import Normal as NormalInit
from lasagne.init import HeNormal as HeNormalInit
from lasagne.init import Constant as ConstantInit
from lasagne.layers import NINLayer

def build_model3():
    net = OrderedDict()
    net['input'] = InputLayer((None, 3, 227, 227), name='input')
    #net['norm1'] = NormLayer(net['conv1'], alpha=0.0001, name='norm1')
    net['conv1'] = ConvLayer(net['input'], num_filters=64, filter_size=7, pad=3, W=HeNormalInit(), name='conv1')
    net['pool1'] = PoolLayer(net['conv1'], pool_size=2, stride=2, name='pool1')
    net['conv2'] = ConvLayer(net['pool1'], num_filters=64, filter_size=7, pad=3, W=HeNormalInit(), name='conv2')
    net['pool2'] = PoolLayer(net['conv2'], pool_size=2, stride=2, name='pool2')
    net['conv3'] = ConvLayer(net['pool2'], num_filters=64, filter_size=7, pad=3, W=HeNormalInit(), name='conv3')
    net['pool3'] = PoolLayer(net['conv3'], pool_size=2, stride=2, name='pool3')
    net['conv4'] = ConvLayer(net['pool3'], num_filters=64, filter_size=7, pad=3, W=HeNormalInit(), name='conv4')
    net['pool4'] = PoolLayer(net['conv4'], pool_size=2, stride=2, name='pool4')
    net['conv5'] = ConvLayer(net['pool4'], num_filters=64, filter_size=7, pad=3, W=HeNormalInit(), name='conv5')

    net['deconv5'] = ConvLayer(net['conv5'], num_filters=64, filter_size=7, pad=3, W=HeNormalInit(), name='deconv5')
    net['upsample4'] = InverseLayer(net['deconv5'], net['pool4'], name='upsample4')
    net['deconv4'] = ConvLayer(net['upsample4'], num_filters=64, filter_size=7, pad=3, W=HeNormalInit(), name='deconv4')
    net['upsample3'] = InverseLayer(net['deconv4'], net['pool3'], name='upsample3')
    net['deconv3'] = ConvLayer(net['upsample3'], num_filters=64, filter_size=7, pad=3, W=HeNormalInit(), name='deconv3')
    net['upsample2'] = InverseLayer(net['deconv3'], net['pool2'], name='upsample2')
    net['deconv2'] = ConvLayer(net['upsample2'], num_filters=64, filter_size=7, pad=3, W=HeNormalInit(), name='deconv2')

