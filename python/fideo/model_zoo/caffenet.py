"""
adapted from lasagne/recipes
"""

from collections import OrderedDict

from lasagne.layers import InputLayer
from lasagne.layers.dnn import Conv2DDNNLayer as Conv2DLayer
from lasagne.layers import MaxPool2DLayer, LocalResponseNormalization2DLayer
from lasagne.layers import SliceLayer, concat, DenseLayer
import lasagne.nonlinearities

from fideo.net import Net

def build_model(pred_layer=False):
    net = Net()
    net.add_layer('data', InputLayer(shape=(None, 3, 227, 227)))
    # conv1
    net.add_layer('conv1', Conv2DLayer(net.last_layer, num_filters=96, filter_size=(11, 11), stride = 4, nonlinearity=lasagne.nonlinearities.rectify))
    net.add_layer('pool1', MaxPool2DLayer(net.last_layer, pool_size=(3, 3), stride=2))
    net.add_layer('norm1', LocalResponseNormalization2DLayer(net.last_layer, n=5, alpha=0.0001/5.0, beta = 0.75, k=1))
    # conv2
    # The caffe reference model uses a parameter called group.
    # This parameter splits input to the convolutional layer.
    # The first half of the filters operate on the first half
    # of the input from the previous layer. Similarly, the
    # second half operate on the second half of the input.
    #
    # Lasagne does not have this group parameter, but we can
    # do it ourselves.
    #
    # see https://github.com/BVLC/caffe/issues/778
    # also see https://code.google.com/p/cuda-convnet/wiki/LayerParams

    # before conv2 split the data
    net['conv2_data1'] = SliceLayer(net['norm1'], indices=slice(0, 48), axis=1, name='conv2_data1')
    net['conv2_data2'] = SliceLayer(net['norm1'], indices=slice(48,96), axis=1, name='conv2_data2')
    net['conv2_part1'] = Conv2DLayer(net['conv2_data1'], num_filters=128, filter_size=(5, 5), pad = 2, name='conv2_part1')
    net['conv2_part2'] = Conv2DLayer(net['conv2_data2'], num_filters=128, filter_size=(5,5), pad = 2, name='conv2_part2')
    net['conv2'] = concat((net['conv2_part1'],net['conv2_part2']),axis=1, name='conv2')

    net['pool2'] = MaxPool2DLayer(net['conv2'], pool_size=(3, 3), stride = 2, name='pool2')
    net['norm2'] = LocalResponseNormalization2DLayer(net['pool2'], n=5, alpha=0.0001/5.0, beta = 0.75, k=1, name='norm2')
    net['conv3'] = Conv2DLayer(net['norm2'], num_filters=384, filter_size=(3, 3), pad = 1, name='conv3')

    net['conv4_data1'] = SliceLayer(net['conv3'], indices=slice(0, 192), axis=1, name='conv4_data1')
    net['conv4_data2'] = SliceLayer(net['conv3'], indices=slice(192,384), axis=1, name='conv4_data2')
    net['conv4_part1'] = Conv2DLayer(net['conv4_data1'], num_filters=192, filter_size=(3, 3), pad = 1, name='conv4_part1')
    net['conv4_part2'] = Conv2DLayer(net['conv4_data2'], num_filters=192, filter_size=(3,3), pad = 1, name='conv4_part2')
    net['conv4'] = concat((net['conv4_part1'],net['conv4_part2']),axis=1, name='conv4')

    net['conv5_data1'] = SliceLayer(net['conv4'], indices=slice(0, 192), axis=1, name='conv5_data1')
    net['conv5_data2'] = SliceLayer(net['conv4'], indices=slice(192,384), axis=1, name='conv5_data2')
    net['conv5_part1'] = Conv2DLayer(net['conv5_data1'], num_filters=128, filter_size=(3, 3), pad = 1, name='conv5_part1')

    net['conv5_part2'] = Conv2DLayer(net['conv5_data2'], num_filters=128, filter_size=(3,3), pad = 1, name='conv5_part2')
    net['conv5'] = concat((net['conv5_part1'],net['conv5_part2']),axis=1, name='conv5')

    net['pool5'] = MaxPool2DLayer(net['conv5'], pool_size=(3, 3), stride = 2, name='pool5')
    net['fc6'] = DenseLayer(net['pool5'],num_units=4096, nonlinearity=lasagne.nonlinearities.rectify, name='fc6')
    net['fc7'] = DenseLayer(net['fc6'], num_units=4096, nonlinearity=lasagne.nonlinearities.rectify, name='fc7')

    if pred_layer:
        net['fc8'] = DenseLayer(net['fc7'], num_units=1000, nonlinearity=lasagne.nonlinearities.softmax, name='fc8')

   #assign_names(net)

    return net
