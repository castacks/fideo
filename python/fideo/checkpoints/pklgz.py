import sys
import json
import time
import logging
import cPickle as pickle
import gzip
from collections import OrderedDict

import numpy as np

from spath import Path


log = logging.getLogger(__name__)


class PickleGzDir(object):

    def __init__(self, base_dir, mkdir=True):
        self.base_dir = Path(base_dir)

        if mkdir and not self.base_dir.exists():
            log.info('Making directory {}'.format(self.base_dir))
            self.base_dir.makedirs_p()


    def save(self, cpt, state=None, overwrite=True):
        if state is None:
            state = OrderedDict()
        itr_ctr = int(state.get('itr_ctr', 0))
        weights_fname = self.base_dir/('weights_{:07d}.pkl.gz'.format(itr_ctr))

        if not overwrite and weights_fname.exists():
            raise IOError('{} exists'.format(weights_fname))

        log.info('checkpointing to {}'.format(weights_fname))

        save_dict = {}
        save_dict.update(cpt)
        # save_dict['weights'] = weights
        save_dict['state'] = state
        save_dict['timestamp'] = float(time.time())

        with gzip.open(weights_fname, 'wb') as f:
            pickle.dump(save_dict, f, protocol=pickle.HIGHEST_PROTOCOL)


    def find_checkpoint_fnames(self, sort_by_mtime=False):
        weights_fnames = self.base_dir.files('weights_*.pkl.gz')
        if sort_by_mtime:
            key = lambda x: x.mtime
        else:
            # twice stripext because there are two extensions
            #key = lambda x: int(x.basename().stripext().stripext().split('_')[1])
            key = lambda x: int(x.basename().replace('.pkl.gz', '').replace('weights_', ''))
        return sorted(weights_fnames, key=key)


    def last_checkpoint_fname(self, sort_by_mtime=False):
        """ Find most recent checkpoint file.
        Sorts by itr_ctr in filename; if use_mtime is True, uses modified time.
        """

        log.info('searching for latest weights filename in {}'.format(self.base_dir))

        weight_fnames = self.find_checkpoint_fnames(sort_by_mtime=sort_by_mtime)

        if len(weight_fnames)==0:
            raise IOError('No weight files found in {}'.format(self.base_dir))
        return weight_fnames[-1]


    def load_latest(self, sort_by_mtime=False):
        weights_fname = self.last_checkpoint_fname(sort_by_mtime=sort_by_mtime)
        log.info('loading weights from {}'.format(weights_fname))
        with gzip.open(weights_fname, 'rb') as f:
            out_dict = pickle.load(f)
        return out_dict


    @staticmethod
    def load_fname(weights_fname):
        with gzip.open(weights_fname, 'rb') as f:
            out_dict = pickle.load(f)
        if not 'weights' in out_dict:
            return {'weights' : out_dict, 'state': {'itr_ctr': 0}}
        return out_dict
