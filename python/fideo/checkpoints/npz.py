import sys
import json
import time
import logging

import numpy as np

from spath import Path


log = logging.getLogger(__name__)


def save_checkpoint(weights, state, fname, overwrite=False):
    if not overwrite and fname.exists():

        raise IOError('{} exists'.format(fname))
    log.info('checkpointing to {}'.format(fname))
    np.savez_compressed(fname, **weights)


def restore_checkpoint(net, out_dir):
    """ Restore weights to net from a directory
    containing potentially many checkpoints.
    """
    weights_fname = find_last_checkpoint(out_dir)
    return restore_checkpoint_from_file(net, weights_fname)


def find_last_checkpoint(out_dir, use_mtime=False):
    """ Find most recent checkpoint file.
    Sorts by itr_ctr in filename; if use_mtime is True, uses modified time.
    """

    log.info('searching for latest weights filename in {}'.format(out_dir))
    weight_fnames = out_dir.files('weights_*.npz')

    if len(weight_fnames)==0:
        log.error('No weight files found in {}; exiting'.format(out_dir))
        sys.exit(1)

    if use_mtime:
        key = lambda x: -x.mtime
    else:
        key = lambda x: -int(x.basename().stripext().split('_')[1])

    weights_fname = sorted(weight_fnames, key=key)[0]
    return weights_fname


def load_weights_from_file(weights_fname):
    """ Wrapper around np.load that restores parameter order.
    """
    npz = np.load(weights_fname)
    out = OrderedDict()

    # ensure parameter ordering is restored
    if '_param_names' in npz:
        param_names = map(str, npz['_param_names'])
        for param_name in param_names:
            out[param_name] = npz[param_name]
    else:
        for k, v in npz.iteritems():
            if k.startswith('_'):
                continue
            out[k] = v

    return out


def restore_checkpoint_from_file(net, weights_fname):
    """ Given weights filename, will restore weights to net.
    """

    log.info('loading weights from {}'.format(weights_fname))
    weights = load_weights_from_file(weights_fname)

    net.set_weights(weights)

    state_fname = weights_fname.stripext()+'.json'
    if state_fname.exists():
        state = json.loads(state_fname.bytes())
    else:
        log.warn('No state json file found.')
        state = Dict()
    if 'itr_ctr' in state:
        log.info('state[itr_ctr] is {}'.format(state[itr_ctr]))
    return state


