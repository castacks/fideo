
# TODO

- allow models and layers using dnn to use non-dnn equivalents for platforms without dnn
- remove cruft, see what has been superseded by lasagne upstream
- update convnet2d/convnet3d interface
- bring in convenience functions scattered throughout other packages:
  - (de)serialization using h5py and names
  - configuration with pylearn2/yaml
  - configuration with python modules
  - visualization layer in 2d/3d
- benchmark upsample implemenations
- trainable upsampling layer
- proper deconvolution layer
- IoU (Jaccard index) metric, possibly as layer
- debugging layers?
- more complete jittering for images and volumes
- streamable/threaded data loading and jittering
- more out-of-the-box models
- incorporate new lasagne functionality: tags, names, init, obj, regularization
- add parameter counting
- more logging/monitoring (though see `pyml_util` as well for this)
- incorporate hyperparam stuff?
