# fideo

The `fideo` package implements core functionality related to neural networks,
using Theano and Lasagne. Most of the code is only used for training.
`fideo` is a successor to `pydnn` and `mavs_dnn2`, and the name is a reference
to pasta (following the `lasagne` theme).


## Concepts

### `Net`

A `Net` is a container of Lasagne layers. It is a little more than
an `OrderedDict`.

### `Model`

A `Model` is a net, plus a loss function, and some other hyperparameters.

### `Trainer`

An object that trains a `Model`.


## Contact

`dimatura@cmu.edu`
