
from collections import OrderedDict


class NetBuilder(OrderedDict):

    def __init__(self):
        pass


    def add(self, layer):
        pass


    def __iadd__(self, other):
        pass

class Net(object):

    def __init__(self, *args, **kwargs):
        super(Net, self).__init__(*args, **kwargs)
        self._input_layer = None
        self._output_layer = None


    def add_layer(self, name, layer):
        """ add unnamed layer, update layer to reflect given name.
        be careful when adding a named layer; existing name will
        be overwritten.
        """
        layer.name = name
        for param in layer.get_params():
            param.name = "%s.%s"%(layer.name, param.name)
        self[layer.name] = layer


    def add_layer_batch_norm(self, name, layer, **kwargs):
        """
        let layer = (dot -> nonlin)
        we transform
        input -> dot -> nonlin -> output
        to
        input -> dot -> batchnorm -> nonlin -> output

        Names will be changed. We add postfixes +bn, +dot, +nl.
        """

        nonlinearity = getattr(layer, 'nonlinearity', None)
        # remove nonlinearity
        if nonlinearity is not None:
            layer.nonlinearity = nonlinearities.identity

        # get rid of bias - it's in batch norm now
        if hasattr(layer, 'b') and layer.b is not None:
            del layer.params[layer.b]
            layer.b = None

        # original layer, 'dot' is dense or conv
        self.add_layer(name+'+dot', layer)

        # batch norm layer
        self.add_named_layer(BatchNormLayer(self.last_layer, name=name+'+bn', **kwargs))

        # nonlin layer
        if nonlinearity is not None:
            self.add_named_layer(NonlinearityLayer(self.last_layer, nonlinearity, name=name+'+nl'))
        return layer


    def add_layer_dropout(self, name, layer, **kwargs):
        """ Adds layer followed by dropout with postfix +drop
        """
        self.add_layer(name, layer)
        self.add_layer(name+'+drop', DropoutLayer(layer, **kwargs))


    def add_layer_dropout_channels(self, name, layer, **kwargs):
        """ Adds layer followed by spatial/channelwise dropout with postfix +dropc
        """
        self.add_layer(name, layer)
        ndim = len(getattr(layer, 'output_shape', layer))
        kwargs['shared_axes'] = tuple(range(2, ndim))
        self.add_layer(name+'+cdrop', DropoutLayer(layer, **kwargs))


    def add_layer_gaussian_noise(self, name, layer, **kwargs):
        """ Adds layer followed by gaussian noise with postfix +gn
        """
        self.add_layer(name, layer)
        self.add_layer(name+'+gn', GaussianNoiseLayer(layer, **kwargs))


    def add_named_layer(self, layer):
        """ add a layer which has a name,
        using layer name as the key
        """
        self[layer.name] = layer


    @property
    def last_layer(self):
        """ last layer by order of addition
        """
        if len(self)==0:
            return None
        return self.values()[-1]


    @property
    def input_layer(self):
        if self._input_layer is not None:
            return self._input_layer
        if len(self)==0:
            return None
        return self.values()[0]


    @input_layer.setter
    def input_layer(self, value):
        assert(value in self.values())
        self._input_layer = value


    @property
    def output_layer(self):
        if self._output_layer is not None:
            return self._output_layer
        return self.last_layer


    @output_layer.setter
    def output_layer(self, value):
        assert(value in self.values())
        self._output_layer = value


    def get_all_params(self, layer=None, **tags):
        if layer is None:
            layer = self.output_layer
        if layer is None:
            return []
        return lasagne.layers.get_all_params(layer, **tags)


    def get_all_layers(self, layer=None):
        if layer is None:
            layer = self.output_layer
        if layer is None:
            return []
        return lasagne.layers.get_all_layers(layer)


    def params_l2_norm(self, layer=None):
        if layer is None:
            layer = self.output_layer
        if layer is None:
            return 0.
        return regularize_network_params(layer, lasagne.regularization.l2)


    def params_l1_norm(self, layer=None):
        if layer is None:
            layer = self.output_layer
        if layer is None:
            return 0.
        return regularize_network_params(layer, lasagne.regularization.l1)


    def repr_net_shapes(self):
        layer_shape = '\n'.join(
                ['{} : {}'.format(layer.name, layer.output_shape)
                    for layer in self.get_all_layers()])
        return layer_shape


    def make_net_diagram(self, fname):
        from . import draw_net
        if len(self)==0:
            raise ValueError('no layers in net')
        draw_net.draw_to_file(lasagne.layers.get_all_layers(self['l_out']),
                              fname,
                              verbose=True)


    def _sanity_check(self):
        params = self.get_all_params()
        names = [par.name for par in params]
        if len(names)!=len(set(names)):
            raise ValueError('net needs unique param names')
        for name, layer in self.items():
            # l_in and l_out are special; aliasing is allowed.
            if name != 'l_in' and name != 'l_out' and name != layer.name:
                raise ValueError('net layer names and keys should be the same')


    def regenerated(self):
        """
        There a few functions in theano that add implicit layers (e.g.
        batch_norm), so some layers are missing in the mapping. This should fix
        it. Call after done with net construction.  Returns new Net.
        Note: all layers must hae unique name.
        Alternative: use add_layer_batch_norm and friends.
        """
        net2 = Net()
        for layer in lasagne.layers.get_all_layers(self.last_layer):
            net2[layer.name] = layer
        return net2


    def set_weights(self, weights):
        """ weights has a dictionary interface.

        TODO: policy for excess/too few parameters.
        """

        self._sanity_check()

        params = self.get_all_params()
        if len(params)==0:
            log.warn('set_weights called, but no parameters in model')
            return

        # note we ignore any other stuff that may be in weights
        for param in params:
            if param.name in weights:
                stored_shape = np.asarray(weights[param.name].shape)
                param_shape = np.asarray(param.get_value().shape)
                if not np.all(stored_shape == param_shape):
                    # TODO configurable policy
                    warn_msg = 'shape mismatch:'
                    warn_msg += '{} stored:{} new:{}'.format(param.name, stored_shape, param_shape)
                    warn_msg += ' skipping'
                    log.warn(warn_msg)
                else:
                    param.set_value(weights[param.name])
            else:
                log.warn('unable to load parameter {} from weights'.format(param.name))


    def get_weights(self):
        params = self.get_all_params()
        param_dict = OrderedDict()
        for param in params:
            param_dict[param.name] = param.get_value(borrow=False)
        return param_dict
