#
# @author  Daniel Maturana
# @year    2015
#
# @attention Copyright (c) 2015
# @attention Carnegie Mellon University
# @attention All rights reserved.
#
# @=


import theano
import theano.tensor as T

#import lasagne

def class_weighted_loss(x, t, weights):
    """ assumes t in range [0, K) and len(weights) = K
    """
    loss = T.nnet.categorical_crossentropy(x, t)*weights[t]
    return T.cast(T.mean(loss) , 'float32')
