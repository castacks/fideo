""" Helpers to set up some common training problems.
"""

from collections import OrderedDict

import numpy as np

import theano
import theano.tensor as T
import lasagne
from lasagne.regularization import regularize_network_params

import fideo

log = fideo.get_logger(__name__)


def struct2d_training_functions(cfg, model, y_dtype='int32', debug=False):
    log = pyml_util.get_logger('make_training_functions')
    l_out = model['l_out']
    # layers = lasagne.layers.get_all_layers(l_out)
    batch_index = T.iscalar('batch_index')
    # bc01
    X = T.TensorType('float32', [False]*4)('X')
    y = T.TensorType(y_dtype, [False]*4)('y')
    out_shape = lasagne.layers.get_output_shape(l_out)
    log.info('output_shape = {}'.format(out_shape))

    batch_slice = slice(batch_index*cfg.batch_size, (batch_index+1)*cfg.batch_size)

    out = lasagne.layers.get_output(l_out, X)
    dout = lasagne.layers.get_output(l_out, X, deterministic=True)
    loss = model['loss'](out, y)
    # det_loss = model['loss'](dout, y)

    params = lasagne.layers.get_all_params(l_out)
    l2_norm = regularize_network_params(l_out, lasagne.regularization.l2)
    learning_rate = theano.shared(np.float32(cfg.learning_rate))
    reg_loss = loss + cfg.reg*l2_norm
    updates = lasagne.updates.momentum(reg_loss, params, learning_rate, cfg.momentum)
    ## TODO use tagging
    ## TODO make this optional
    for param, update in updates.iteritems():
        if ((param.name is not None) and
            (param.name.startswith('conv') or
             param.name.startswith('deconv') or
             param.name.startswith('nin')) and
             param.name.endswith('.W')):
            # TODO make this a param. hinton says 3-4. but that's 2d
            c_update = lasagne.updates.norm_constraint(update, 3.)
            updates[param] = c_update
    X_shared = lasagne.utils.shared_empty(4, dtype='float32')
    y_shared = lasagne.utils.shared_empty(4, dtype='float32')

    update_iter = theano.function([batch_index], reg_loss,
            updates=updates, givens={
            X: X_shared[batch_slice],
            y: T.cast(y_shared[batch_slice], y_dtype),
        })
    dout_fn = theano.function([X], dout, allow_input_downcast=True)

    funcs, tvars = OrderedDict(), OrderedDict()

    tvars['X'] = X
    tvars['y'] = y
    tvars['X_shared'] = X_shared
    tvars['y_shared'] = y_shared
    tvars['batch_slice'] = batch_slice
    tvars['batch_index'] = batch_index
    tvars['learning_rate'] = learning_rate

    funcs['update_iter'] = update_iter
    funcs['dout'] = dout_fn

    # TODO configurable err_rate, pred functions
    if 'train_metrics' in model:
        funcs['train_metrics'] = OrderedDict()
        for mname, mexpr in model['train_metrics'].items():
            # det_err_rate = fideo.expr2d.masked_error_rate_2d( dout, y )
            mexpr_bound = mexpr(dout, y)
            mfn = theano.function([batch_index], mexpr_bound, givens={
                    X: X_shared[batch_slice],
                    y: T.cast(y_shared[batch_slice], y_dtype),
                })
            funcs['train_metrics'][mname] = mfn

    if 'pred' in model:
        det_pred = fideo.expr2d.predict_2d(dout)
        pred_fn = theano.function([X], det_pred, allow_input_downcast=True)
        funcs['pred'] = pred_fn

    if debug:
        layers = lasagne.layers.get_all_layers(l_out)
        #dbg_outs = [lasagne.layers.get_output(X) for l in layers]
        dbg_outs = [lasagne.layers.get_output(l, X, deterministic=True) for l in layers]
        funcs['dump'] = theano.function([X], dbg_outs, allow_input_downcast=True)

    return funcs, tvars


def struct2d_testing_functions(cfg, model, y_dtype='int32', debug=False):

    log = pyml_util.get_logger('make_testing_functions')
    l_out = model['l_out']
    X = T.TensorType('float32', [False]*4)('X')
    y = T.TensorType(y_dtype, [False]*4)('y')
    out_shape = lasagne.layers.get_output_shape(l_out)
    log.info('output_shape = {}'.format(out_shape))

    dout = lasagne.layers.get_output(l_out, X, deterministic=True)
    # X_shared = lasagne.utils.shared_empty(4, dtype='float32')
    # y_shared = lasagne.utils.shared_empty(4, dtype='float32')

    dout_fn = theano.function([X], dout, allow_input_downcast=True)
    funcs, tvars = OrderedDict(), OrderedDict()

    tvars['X'] = X
    tvars['y'] = y
    # tvars['X_shared'] = X_shared
    # tvars['y_shared'] = y_shared
    funcs['dout'] = dout_fn

    if debug:
        layers = lasagne.layers.get_all_layers(l_out)
        #dbg_outs = [lasagne.layers.get_output(X) for l in layers]
        dbg_outs = [lasagne.layers.get_output(l, X, deterministic=True) for l in layers]
        funcs['dump'] = theano.function([X], dbg_outs, allow_input_downcast=True)

    return funcs, tvars



def scalar_training_functions(cfg, model, y_dtype='int32', debug=False):
    """
    TODO unify with structured2d
    """
    log = pyml_util.get_logger('make_training_functions')

    l_out = model['l_out']
    #layers = lasagne.layers.get_all_layers(l_out)

    batch_index = T.iscalar('batch_index')

    # bc01
    X = T.TensorType('float32', [False]*4)('X')
    y = T.TensorType(cfg.y_dtype, [False]*2)('y')

    out_shape = lasagne.layers.get_output_shape(l_out)
    log.info('output_shape = {}'.format(out_shape))

    batch_slice = slice(batch_index*cfg.batch_size, (batch_index+1)*cfg.batch_size)

    out = lasagne.layers.get_output(l_out, X)
    dout = lasagne.layers.get_output(l_out, X, deterministic=True)
    loss = model['loss'](out, y)
    det_loss = model['loss'](dout, y)

    if debug:
        layers = lasagne.layers.get_all_layers(l_out)
        #dbg_outs = [lasagne.layers.get_output(X) for l in layers]
        dbg_outs = [lasagne.layers.get_output(l, X, deterministic=True) for l in layers]

    params = lasagne.layers.get_all_params(l_out)
    l2_norm = lasagne.regularization.regularize_network_params(l_out,
            lasagne.regularization.l2)

    learning_rate = theano.shared(np.float32(cfg.learning_rate))

    reg_loss = loss + cfg.reg*l2_norm
    updates = lasagne.updates.momentum(reg_loss, params, learning_rate, cfg.momentum)
    ## TODO use tagging
    for param, update in updates.iteritems():
        if ((param.name is not None) and
             param.name.startswith('conv') and
             param.name.endswith('.W')):
            # TODO make this a param. hinton says 3-4. but that's 2d
            c_update = lasagne.updates.norm_constraint(update, 3.)
            updates[param] = c_update

    X_shared = lasagne.utils.shared_empty(4, dtype='float32')
    y_shared = lasagne.utils.shared_empty(2, dtype='float32')

    update_iter = theano.function([batch_index],
            reg_loss,
            updates=updates, givens={
            X: X_shared[batch_slice],
            # TODO can theano detect noop?
            y: T.cast(y_shared[batch_slice], cfg.y_dtype),
        })

    dout_fn = theano.function([X], dout, allow_input_downcast=True)
    funcs = {'update_iter':update_iter,
             'dout' : dout_fn,
            }
    if debug:
        funcs['dump'] = theano.function([X], dbg_outs, allow_input_downcast=True)
    tvars = {'X': X,
             'y': y,
             'X_shared': X_shared,
             'y_shared': y_shared,
             'batch_slice': batch_slice,
             'batch_index': batch_index,
             'learning_rate': learning_rate,}
    return funcs, tvars


def get_all_layer_output_shapes(layer):
    layers = lasagne.layers.get_all_layers(layer)
    return [l.get_output_shape() for l in layers]
