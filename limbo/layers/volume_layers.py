#
# @author  Daniel Maturana
# @year    2015
#
# @attention Copyright (c) 2015
# @attention Carnegie Mellon University
# @attention All rights reserved.
#
# @=

"""
Layers for spatially 3d/ volumetric data
"""

import numpy as np

import lasagne
from lasagne.layers import Layer

from ..expr3d import max_pool_3d
#from ..activations import relu, softplus

import theano
import theano.tensor as T
from theano.tensor.nnet import conv3d2d
from theano.sandbox.cuda.basic_ops import gpu_contiguous
from theano.sandbox.cuda.blas import GpuCorr3dMM

floatX = theano.config.floatX

__all__ = [
        'Conv3dLayer',
        'Conv3dMMLayer',
        'Norm3dLayer',
        'Pool3dLayer',
        'RotPool3dLayer',
        'RotPoolLayer',
        'SplitLayer',
        ]

"""
class OccupancyLayer(Layer):
    def __init__(self, input_layer, in_time, in_height, in_width):
        self.layer_name = "OccupancyLayer"
        self.batch_size = batch_size
        # assuming 2 channels, hit and pass
        self.in_dims = (2, in_time, in_height, in_width)

    def get_output_for(self, input, *args, **kwargs):
        # 3D convolution; dimshuffle: last 3 dimensions must be (in, h, w)
        n_fr, h, w = self.video_shape
        n_fr_k, h_k, w_k = self.kernel_shape
        out = conv3d(
                signals=input.dimshuffle([0,2,1,3,4]),
                filters=self.W,
                signals_shape=(self.batch_size, n_fr, self.n_in_maps, h, w),
                filters_shape=(self.num_filters, n_fr_k, self.n_in_maps, h_k, w_k),
                border_mode='valid').dimshuffle([0,2,1,3,4])
        out += self.b.dimshuffle('x',0,'x','x','x')
        self.output = self.activation(out)
        return self.output
"""

class Conv3dLayer(Layer):
    """
    conv3d

    TODO incorporate newer lasagne conventions for padding etc
    TODO why t01?
    """
    def __init__(self, input_layer, num_filters, filter_size,
            border_mode=None,
            W=lasagne.init.Normal(std=0.01),
            b=lasagne.init.Constant(0.),
            nonlinearity=lasagne.nonlinearities.rectify,
            pad=None,
            **kwargs):
        """
        input_shape: (frames, height, width)

        W_shape: (out, in, kern_frames, kern_height, kern_width)
        """
        super(Conv3dLayer, self).__init__(input_layer, **kwargs)

        # TODO note that lasagne allows 'untied' biases, the same shape
        # as the input filters.

        self.num_filters = num_filters
        self.filter_size = filter_size
        self.strides = (1,1,1)
        self.flip_filters = False
        if nonlinearity is None:
            self.nonlinearity = lasagne.nonlinearities.identity
        else:
            self.nonlinearity = nonlinearity

        # TODO lasagne calc size

        if border_mode is not None and pad is not None:
            raise RuntimeError("You cannot specify both 'border_mode' and 'pad'. To avoid ambiguity, please specify only one of them.")
        elif border_mode is None and pad is None:
            # no option specified, default to valid mode
            self.pad = (0, 0, 0)
        elif border_mode is not None:
            if border_mode == 'valid':
                self.pad = (0, 0, 0)
            elif border_mode == 'full':
                self.pad = (self.filter_size[0] - 1, self.filter_size[1] -1, self.filter_size[2] - 1)
            elif border_mode == 'same':
                # only works for odd filter size, but the even filter size case is probably not worth supporting.
                self.pad = ((self.filter_size[0] - 1) // 2,
                            (self.filter_size[1] - 1) // 2,
                            (self.filter_size[2] - 1) // 2)
            else:
                raise RuntimeError("Unsupported border_mode for Conv3dLayer: %s" % border_mode)
        else:
            self.pad = pad

        self.W = self.add_param(W, self.get_W_shape(), name='W')
        if b is None:
            self.b = None
        else:
            self.b = self.add_param(b, (num_filters,), name='b', regularizable=False)

        """
        # TODO check what pylearn does
        # fan in: filter time x filter height x filter width x input maps
        fan_in = np.prod(kernel_shape)*self.n_in_maps
        #norm_scale = 2.*sqrt( 1. / fan_in )
        # from soumtihg torch7 nn
        a = 1./np.sqrt(fan_in)
        W_shape = (self.num_filters, self.n_in_maps)+tuple(kernel_shape)
        W_val = theano._asarray(rng.uniform(-a, a, size=W_shape),dtype=floatX)
        """

    def get_W_shape(self):
        num_input_channels = self.input_layer.get_output_shape()[1]
        return (self.num_filters, num_input_channels, self.filter_size[0], self.filter_size[1], self.filter_size[2])

    def get_output_shape_for(self, input_shape):
        """ input is bct01
        """
        batch_size = input_shape[0]
        volume_shape = np.asarray(input_shape[-3:]).astype(np.float32)
        filter_size = np.asarray(self.filter_size).astype(np.float32)
        pad = np.asarray(self.pad).astype(np.float32)
        # TODO check this is right. also it depends on border_mode
        # this assumes strides = 1
        #out_dim = (video_shape-(2*np.floor(kernel_shape/2.))).astype(np.int32)
        #out_dim = ( (volume_shape-filter_size) + 1).astype(np.int32)
        out_dim = ( (volume_shape + 2*pad - filter_size) + 1 ).astype(np.int32)
        return (batch_size, self.num_filters, out_dim[0], out_dim[1], out_dim[2])

    def get_output_for(self, input, *args, **kwargs):
        # TODO filter flip
        # input is bct01
        # theano doc: signals is [Ns, Ts, C, Hs, Ws],
        # filters is  [Nf, Tf, C, Hf, Wf]
        input_shape = self.input_layer.get_output_shape()
        t, h, w = input_shape[2], input_shape[3], input_shape[4]
        input_c = input_shape[1]
        batch_size = input_shape[0]
        filter_t, filter_h, filter_w = self.filter_size
        # TODO I think filters_shape is wrong?
        out = conv3d2d.conv3d(
                signals=input.dimshuffle([0,2,1,3,4]), # bct01 -> btc01
                filters=self.W,
                signals_shape=(batch_size, t, input_c, h, w),
                filters_shape=(self.num_filters, filter_t, input_c, filter_h, filter_w),
                border_mode='valid').dimshuffle([0,2,1,3,4]) # btc01 -> bct01
        if self.b is not None:
            out = out + self.b.dimshuffle('x',0,'x','x','x')
        return self.nonlinearity( out )

class Conv3dMMLayer(Layer):
    def __init__(self, input_layer, num_filters, filter_size,
            strides=(1,1,1),
            border_mode=None,
            W=lasagne.init.Normal(std=0.001), # usually 0.01
            b=lasagne.init.Constant(0.),
            nonlinearity=lasagne.nonlinearities.rectify,
            pad=None,
            flip_filters=True,
            **kwargs):
        """
        input_shape: (frames, height, width)

        W_shape: (out, in, kern_frames, kern_height, kern_width)
        """
        super(Conv3dMMLayer, self).__init__(input_layer, **kwargs)

        # TODO note that lasagne allows 'untied' biases, the same shape
        # as the input filters.

        self.num_filters = num_filters
        self.filter_size = filter_size
        if strides is None:
            self.strides = (1,1,1)
        else:
            self.strides = tuple(strides)
        self.flip_filters = flip_filters
        if nonlinearity is None:
            self.nonlinearity = lasagne.nonlinearities.identity
        else:
            self.nonlinearity = nonlinearity

        if border_mode is not None and pad is not None:
            raise RuntimeError("You cannot specify both 'border_mode' and 'pad'. To avoid ambiguity, please specify only one of them.")
        elif border_mode is None and pad is None:
            # no option specified, default to valid mode
            self.pad = (0, 0, 0)
        elif border_mode is not None:
            if border_mode == 'valid':
                self.pad = (0, 0, 0)
            elif border_mode == 'full':
                self.pad = (self.filter_size[0] - 1, self.filter_size[1] -1, self.filter_size[2] - 1)
            elif border_mode == 'same':
                # only works for odd filter size, but the even filter size case is probably not worth supporting.
                self.pad = ((self.filter_size[0] - 1) // 2,
                            (self.filter_size[1] - 1) // 2,
                            (self.filter_size[2] - 1) // 2)
            else:
                raise RuntimeError("Unsupported border_mode for Conv3dLayer: %s" % border_mode)
        else:
            self.pad = tuple(pad)

        self.W = self.add_param(W, self.get_W_shape(), name='W')
        if b is None:
            self.b = None
        else:
            self.b = self.add_param(b, (num_filters,), name='b', regularizable=False)

        """
        # TODO check what pylearn does
        # fan in: filter time x filter height x filter width x input maps
        fan_in = np.prod(kernel_shape)*self.n_in_maps
        #norm_scale = 2.*sqrt( 1. / fan_in )
        # from soumtihg torch7 nn
        a = 1./np.sqrt(fan_in)
        W_shape = (self.num_filters, self.n_in_maps)+tuple(kernel_shape)
        W_val = theano._asarray(rng.uniform(-a, a, size=W_shape),dtype=floatX)
        """

        """
        # from docs in theano.sandbox.cuda.blas
        :param border_mode: currently supports "valid" only; "full" can be
            simulated by setting `pad="full"` (at the cost of performance), or
            by using `GpuCorrMM_gradInputs`
        :param subsample: the subsample operation applied to each output image.
            Should be a tuple with 3 elements.
            `(sv, sh, sl)` is equivalent to `GpuCorrMM(...)(...)[:,:,::sv, ::sh, ::sl]`,
            but faster.
            Set to `(1, 1, 1)` to disable subsampling.
        :param pad: the width of a border of implicit zeros to pad the input
            image with. Should be a tuple with 3 elements giving the numbers of
            rows and columns to pad on each side, or "half" to set the padding
            to `(kernel_rows // 2, kernel_columns // 2, kernel_depth // 2)`, or "full" to set the
            padding to `(kernel_rows - 1, kernel_columns - 1, kernel_depth - 1)` at runtime.
            Set to `(0, 0, 0)` to disable padding.

        :note: Currently, the Op requires the inputs, filters and outputs to be
            C-contiguous. Use :func:`gpu_contiguous
            <theano.sandbox.cuda.basic_ops.gpu_contiguous>` on these arguments
            if needed.
        """

        self.corr_mm_op = GpuCorr3dMM(subsample=self.strides, pad=self.pad)

    def get_W_shape(self):
        # out in t01
        #num_input_channels = self.input_layer.get_output_shape()[1]
        num_input_channels = self.input_shape[1]
        return (self.num_filters, num_input_channels, self.filter_size[0], self.filter_size[1], self.filter_size[2])

    def get_output_shape_for(self, input_shape):
        """ input is bct01
        """
        batch_size = input_shape[0]
        volume_shape = np.asarray(input_shape[-3:]).astype(np.float32)
        filter_size = np.asarray(self.filter_size).astype(np.float32)
        pad = np.asarray(self.pad).astype(np.float32)
        strides = np.asarray(self.strides).astype(np.float32)
        # TODO check this is right. also it depends on border_mode
        # this assumes strides = 1
        #out_dim = (video_shape-(2*np.floor(kernel_shape/2.))).astype(np.int32)
        #out_dim = ( (volume_shape-filter_size) + 1).astype(np.int32)
        out_dim = ( (volume_shape + 2*pad - filter_size) // strides + 1 ).astype(np.int32)
        return (batch_size, self.num_filters, out_dim[0], out_dim[1], out_dim[2])

    def get_output_for(self, input, *args, **kwargs):
        # TODO figure out shapes
        # TODO filter flip
        t, h, w = input.shape[2], input.shape[3], input.shape[4]
        filter_t, filter_h, filter_w = self.filter_size
        #input_c = input.shape[1]

        filters = self.W
        if self.flip_filters:
            filters = filters[:,:,::-1,::-1,::-1]
        contiguous_filters = gpu_contiguous(filters)
        contiguous_input = gpu_contiguous(input)

        conved = self.corr_mm_op(contiguous_input, contiguous_filters)

        if self.b is None:
            activation = conved
        else:
            activation = conved + self.b.dimshuffle('x', 0, 'x', 'x', 'x')

        return self.nonlinearity(activation)

class Norm3dLayer(object):
    """ Normalization layer
    """
    def __init__(self, input, method="lcn", **kwargs):
        """
        method: "lcn", "gcn", "mean"

        LCN: local contrast normalization
            kwargs:
                kernel_size=9, threshold=1e-4, use_divisor=True

        GCN: global contrast normalization
            kwargs:
                scale=1., subtract_mean=True, use_std=False, sqrt_bias=0.,
                min_divisor=1e-8

        MEAN: local mean subtraction
            kwargs:
                kernel_size=5
        """

        input_shape = input.shape

        # make 4D tensor out of 5D tensor -> (n_images, 1, height, width)
        input_shape_4D = (input_shape[0]*input_shape[1]*input_shape[2], 1,
                          input_shape[3], input_shape[4])
        input_4D = input.reshape(input_shape_4D, ndim=4)
        if method=="lcn":
            out = self.lecun_lcn(input_4D, **kwargs)
        elif method=="gcn":
            out = self.global_contrast_normalize(input_4D,**kwargs)
        elif method=="mean":
            out = self.local_mean_subtraction(input_4D, **kwargs)
        else:
            raise NotImplementedError()

        self.output = out.reshape(input_shape)

    def lecun_lcn(self, X, kernel_size=7, threshold = 1e-4, use_divisor=False):
        """
        Yann LeCun's local contrast normalization
        Orginal code in Theano by: Guillaume Desjardins
        """

        filter_shape = (1, 1, kernel_size, kernel_size)
        filters = gaussian_filter(kernel_size).reshape(filter_shape)
        filters = theano.shared(theano._asarray(filters, dtype=floatX), borrow=True)

        convout = T.nnet.conv2d(X, filters=filters, filter_shape=filter_shape,
                            border_mode='full')

        # For each pixel, remove mean of kernel_sizexkernel_size neighborhood
        mid = int(np.floor(kernel_size/2.))
        new_X = X - convout[:,:,mid:-mid,mid:-mid]

        if use_divisor:
            # Scale down norm of kernel_sizexkernel_size patch
            sum_sqr_XX = T.nnet.conv2d(T.sqr(T.abs_(X)), filters=filters,
                                filter_shape=filter_shape, border_mode='full')

            denom = T.sqrt(sum_sqr_XX[:,:,mid:-mid,mid:-mid])
            per_img_mean = denom.mean(axis=[2,3])
            divisor = T.largest(per_img_mean.dimshuffle(0,1,'x','x'), denom)
            divisor = T.maximum(divisor, threshold)

            new_X /= divisor

        return new_X#T.cast(new_X, floatX)

    def local_mean_subtraction(self, X, kernel_size=5):

        filter_shape = (1, 1, kernel_size, kernel_size)
        filters = mean_filter(kernel_size).reshape(filter_shape)
        filters = theano.shared(theano._asarray(filters, dtype=floatX), borrow=True)

        mean = T.nnet.conv2d(X, filters=filters, filter_shape=filter_shape,
                        border_mode='full')
        mid = int(np.floor(kernel_size/2.))

        return X - mean[:,:,mid:-mid,mid:-mid]

    def global_contrast_normalize(self, X, scale=1., subtract_mean=True,
        use_std=False, sqrt_bias=0., min_divisor=1e-8):

        ndim = X.ndim
        if not ndim in [3,4]: raise NotImplementedError("X.dim>4 or X.ndim<3")

        scale = float(scale)
        mean = X.mean(axis=ndim-1)
        new_X = X.copy()

        if subtract_mean:
            if ndim==3:
                new_X = X - mean[:,:,None]
            else: new_X = X - mean[:,:,:,None]

        if use_std:
            normalizers = T.sqrt(sqrt_bias + X.var(axis=ndim-1)) / scale
        else:
            normalizers = T.sqrt(sqrt_bias + (new_X ** 2).sum(axis=ndim-1)) / scale

        # Don't normalize by anything too small.
        T.set_subtensor(normalizers[(normalizers < min_divisor).nonzero()], 1.)

        if ndim==3: new_X /= normalizers[:,:,None]
        else: new_X /= normalizers[:,:,:,None]

        return new_X

class Pool3dLayer(Layer):
    """ Subsampling and pooling layer
    """
    def __init__(self, input_layer, pool_shape, **kwargs):
        super(Pool3dLayer, self).__init__(input_layer, **kwargs)
        self.pool_shape = pool_shape

    def calc_out_dim(self, input_shape, pool_shape):

        output_length = (input_shape-1)//pool_shape

    def get_output_shape_for(self, input_shape):
        # input_shape[0] is batch size?
        # bct01, only last three change
        # NOTE maxpool3d and the ignore_border.
        # if ignore_border=False (default), out is ceil; else it's floor
        return (input_shape[0],
                input_shape[1],
                int(np.ceil(float(input_shape[2])/(self.pool_shape[0]))),
                int(np.ceil(float(input_shape[3])/(self.pool_shape[1]))),
                int(np.ceil(float(input_shape[4])/(self.pool_shape[2]))))

    def get_output_for(self, input, *args, **kwargs):
        # TODO what if input is in flat form?
        out = max_pool_3d(input, self.pool_shape)
        self.output = out
        return self.output

def gaussian_filter(kernel_shape):
    x = np.zeros((kernel_shape, kernel_shape), dtype='float32')
    def gauss(x, y, sigma=2.0):
        Z = 2 * np.pi * sigma**2
        return  1./Z * np.exp(-(x**2 + y**2) / (2. * sigma**2))
    mid = np.floor(kernel_shape/ 2.)
    for i in xrange(0,kernel_shape):
        for j in xrange(0,kernel_shape):
            x[i,j] = gauss(i-mid, j-mid)
    return x / sum(x)

def mean_filter(kernel_size):
    s = kernel_size**2
    x = np.repeat(1./s, s).reshape((kernel_size, kernel_size))
    return x

class RotPoolLayer(Layer):
    """
    Pool over num_degs rotations.
    """
    def __init__(self, input_layer, num_degs, method='max', **kwargs):
        """
        """
        super(RotPoolLayer, self).__init__(input_layer, **kwargs)
        self.method = method
        self.num_degs = num_degs

    # rely on defaults for get_params
    def get_output_shape_for(self, input_shape):
        assert( len(input_shape)==2 )
        if any(s is None for s in input_shape):
            return input_shape
        batch_size = input_shape[0]
        # TODO if is none?
        return ( input_shape[0]//self.num_degs, input_shape[1] )

    def get_output_for(self, input, *args, **kwargs):

        input_shape = self.input_shape
        if any(s is None for s in input_shape):
            input_shape = input.shape
            print 'input shape none'
        print input_shape

        batch_size2 = input_shape[0]//self.num_degs
        num_features = input_shape[1]

        out_pool_shape = (batch_size2, self.num_degs, num_features)
        if self.method=='max':
            out_pooled = T.max(input.reshape(out_pool_shape), 1)
        elif self.method=='avg':
            out_pooled = T.mean(input.reshape(out_pool_shape), 1)
        else:
            raise Exception('unknown pooling type')

        return out_pooled

class RotPool3dLayer(Layer):
    def __init__(self, input_layer, num_degs, method='max', **kwargs):
        super(RotPool3dLayer, self).__init__(input_layer, **kwargs)
        self.method = method
        self.num_degs = num_degs

    def get_output_shape_for(self, input_shape):
        assert( len(input_shape)==2 )
        if any(s is None for s in input_shape):
            return input_shape
        batch_size = input_shape[0]
        # TODO if is none?
        return ( input_shape[0]//self.num_degs, ) + tuple(input_shape[1:])

    def get_output_for(self, input, *args, **kwargs):

        input_shape = self.input_shape
        if any(s is None for s in input_shape):
            input_shape = input.shape

        batch_size2 = input_shape[0]//self.num_degs

        out_pool_shape = (batch_size2, self.num_degs,) + input_shape[1:]
        if self.method=='max':
            out_pooled = T.max(input.reshape(out_pool_shape), 1)
        elif self.method=='avg':
            out_pooled = T.mean(input.reshape(out_pool_shape), 1)
        else:
            raise Exception('unknown pooling type')

        return out_pooled

class SplitLayer(Layer):
    """
    select k features, starting at ix, out of all features.
    useful to split features into subsets.
    TODO: this is not purely 3d, move elsewhere
    """
    def __init__(self, input_layer, ix, k=1, **kwargs):
        super(SplitLayer, self).__init__(input_layer, **kwargs)
        self.ix = ix
        self.k = k

    def get_output_shape_for(self, input_shape):
        #if any(s is None for s in input_shape):
        #    return input_shape
        return (input_shape[0], self.k,) + input_shape[2:]

    def get_output_for(self, input, *args, **kwargs):
        #input_shape = self.input_shape
        #if any(s is None for s in input_shape):
            #input_shape = input.shape
        return input[:,self.ix:(self.ix+self.k)]
