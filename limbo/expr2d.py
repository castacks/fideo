"""

Some Theano expressions for spatially 2D data.

Equivalent ones may be already in theano/lasagne, as they're
continually evolving; check regularly.

"""

import numpy as np
import theano
import theano.tensor as T


def binary_logloss(pre_sigmoid_out, y_batch):
    """
    instead of binary crossentropy we do equivalent expression
    that is more stable because of theano quirks
    """
    #ce = T.nnet.binary_crossentropy(T.nnet.sigmoid(pre_sigmoid_out), y_batch).mean()
    eltwise_ce = -y_batch*pre_sigmoid_out + T.nnet.softplus(pre_sigmoid_out)
    ce = eltwise_ce.mean()
    return ce


def masked_binary_logloss(pre_sigmoid_out, y_target):
    """
    input and output are (b,c,h,w): b*w*h pixels and c channels.
    each channel is a per-class confidence.

    note that classes are not necessarily exclusive; i.e. per-pixel
    class predictions don't need to sum to 1.

    values for the target must be between 0 and 1. values
    for the input are passed through a sigmoid.

    NOTE. mask for y is indicated with negative values (not 0, as usual).
    XXX. there is something off with the mask. avoid for now.
    """
    mask = T.gt(y_target, 0.)
    ce = T.nnet.binary_crossentropy(T.nnet.sigmoid(pre_sigmoid_out[mask]), y_target[mask]).mean()
    return T.cast(ce, 'float32')


def binary_crossentropy_2d_zero_is_none(output, y_batch):
    """
    note: 0 pixel means no class!
    """
    B, K, H, W = output.shape
    yhat_reshaped = T.reshape(output.dimshuffle((0, 2, 3, 1)), (B*H*W, K))
    yflat = T.flatten(y_batch)
    # note how we discard first column of one hot
    yflat_onehot =  T.extra_ops.to_one_hot(T.cast(yflat, 'int32'), K+1)[:,1:]
    cost = T.nnet.binary_crossentropy(T.nnet.sigmoid(yhat_reshaped), yflat_onehot).mean()
    return cost



# TODO many these are basically a sort of im2col; can we
# do it more efficiently?

def logloss_2d(pre_softmax_output, y_batch):
    """
    input is (b,c,h,w). b*w*h samples, and a K unnormalized pred for each
    assuming y_batch (ground truth) is a label image with indexes in each pixel
    """
    # note that we need pre_softmax_output,
    # because the theano magic for numerical stability
    # is not working after this reshape.
    BHW = pre_softmax_output.shape[0]*pre_softmax_output.shape[2]*pre_softmax_output.shape[3]
    K = pre_softmax_output.shape[1]
    # have to dimshuffle before flattening
    out_reshaped = T.reshape(pre_softmax_output.dimshuffle(0, 2, 3, 1), (BHW, K), ndim=2)
    softmax_out = T.nnet.softmax( out_reshaped )
    # b*w*h samples with a single index k for each
    y_batch_reshaped = y_batch.flatten()
    loss = T.cast(T.mean(T.nnet.categorical_crossentropy(softmax_out, y_batch_reshaped)), 'float32')
    return loss


def stable_logloss_2d(pre_softmax_output):
    """
    input is (b,c,h,w). b*w*h samples, and a K unnormalized pred for each
    assuming y_batch (ground truth) is a label image with indexes in each pixel

    this is for numerical robustness; https://github.com/Lasagne/Lasagne/issues/332#issuecomment-122328992

    """
    # note that we need pre_softmax_output,
    # because the theano magic for numerical stability
    # is not working after this reshape.
    BHW = pre_softmax_output.shape[0]*pre_softmax_output.shape[2]*pre_softmax_output.shape[3]
    K = pre_softmax_output.shape[1]
    # have to dimshuffle before flattening
    out_reshaped = T.reshape(pre_softmax_output.dimshuffle(0, 2, 3, 1), (BHW, K), ndim=2)

    # b*w*h samples with a single index k for each
    y_batch_reshaped = y_batch.flatten()

    # log softmax
    ydev = out_reshaped - out_reshaped.max(1, keepdims=True)
    ydev2 = ydev - T.log(T.sum(T.exp(ydev), axis=1, keepdims=True))

    loss = -T.cast(T.mean(ydev2[y_batch_reshaped]), 'float32')

    return loss


def masked_logloss_2d(pre_softmax_output, y_batch):
    """
    by convention 0 means 'no label'.
    TODO case when mask != 0
    """
    # note that we need pre_softmax_output,
    # because the theano magic for numerical stability
    # is not working after this reshape.

    y_batch_reshaped = y_batch.flatten()
    nz_ix = y_batch_reshaped.nonzero()
    y_batch_reshaped = y_batch_reshaped[nz_ix] - 1

    # input is (b,c,h,w). b*w*h samples, and a K unnormalized for each
    # b*w*h samples with a single index k for each
    # labels are in 0-K range inclusive but 0 is invalid/missing.
    BHW = pre_softmax_output.shape[0]*pre_softmax_output.shape[2]*pre_softmax_output.shape[3]
    K = pre_softmax_output.shape[1]
    out_reshaped = T.reshape(pre_softmax_output.dimshuffle(0, 2, 3, 1), (BHW, K), ndim=2)
    out_reshaped = out_reshaped[nz_ix]
    softmax_out = T.nnet.softmax( out_reshaped )
    loss = T.cast(T.mean(T.nnet.categorical_crossentropy(softmax_out, y_batch_reshaped)), 'float32')
    return loss


def stable_masked_logloss_2d(pre_softmax_output, y_batch):
    """
    by convention 0 means 'no label'.
    TODO case when mask != 0
    """
    # note that we need pre_softmax_output,
    # because the theano magic for numerical stability
    # is not working after this reshape.
    y_batch_reshaped = y_batch.flatten()
    nz_ix = y_batch_reshaped.nonzero()
    y_batch_reshaped = y_batch_reshaped[nz_ix] - 1

    # input is (b,c,h,w). pre_softmax_output is b*w*h samples, k-vector unnormalized for each.
    # y_batch labels is b*w*h samples with a single index for each.
    # labels are in 0-K range inclusive but 0 is invalid/missing.
    BHW = pre_softmax_output.shape[0]*pre_softmax_output.shape[2]*pre_softmax_output.shape[3]
    K = pre_softmax_output.shape[1]
    out_reshaped = T.reshape(pre_softmax_output.dimshuffle(0, 2, 3, 1), (BHW, K), ndim=2)
    out_reshaped = out_reshaped[nz_ix]

    # log softmax
    #ydev = out_reshaped - out_reshaped.max(1, keepdims=True)
    #ydev2 = ydev - T.log(T.sum(T.exp(ydev), axis=1, keepdims=True))
    # q_denom = T.log(T.sum(T.exp(out_reshaped), axis=1, keepdims=True))
    # e_x = T.exp(out_reshaped - out_reshaped.max(1, keepdims=True))
    # softmax = e_x/e_x.sum(axis=1, keepdims=True)

    #out_reshaped = out_reshaped - out_reshaped.max(1, keepdims=True)
    #q_denom = T.log(T.sum(T.exp(out_reshaped), axis=1, keepdims=True))
    #loss = T.mean(-out_reshaped[y_batch_reshaped] + q_denom)

    # this one works like old crossentropy
    #e_x = T.exp(out_reshaped - out_reshaped.max(1, keepdims=True))
    #softmax = e_x/e_x.sum(axis=1, keepdims=True)
    #loss = T.mean(-T.log(softmax[y_batch_reshaped]))

    out_reshaped2 = out_reshaped - out_reshaped.max(1, keepdims=True)
    q_denom = T.log(T.sum(T.exp(out_reshaped2), axis=1, keepdims=True))
    loss = T.mean(-out_reshaped2[y_batch_reshaped] + q_denom)

    # -log softmax =
    # -log(e_x/sum_e_x)
    # -log(e_x) + log(sum_e_x)
    # -x + log(sum_e_x)
    # -(x - x_max) + log sum exp (x - x_max)

    return loss


def stable_masked_unnorm_logloss_2d(pre_softmax_output, y_batch):
    """
    by convention 0 means 'no label'.
    TODO case when mask != 0
    """
    # note that we need pre_softmax_output,
    # because the theano magic for numerical stability
    # is not working after this reshape.
    y_batch_reshaped = y_batch.flatten()
    nz_ix = y_batch_reshaped.nonzero()
    y_batch_reshaped = y_batch_reshaped[nz_ix] - 1

    # input is (b,c,h,w). pre_softmax_output is b*w*h samples, k-vector unnormalized for each.
    # y_batch labels is b*w*h samples with a single index for each.
    # labels are in 0-K range inclusive but 0 is invalid/missing.
    BHW = pre_softmax_output.shape[0]*pre_softmax_output.shape[2]*pre_softmax_output.shape[3]
    K = pre_softmax_output.shape[1]
    out_reshaped = T.reshape(pre_softmax_output.dimshuffle(0, 2, 3, 1), (BHW, K), ndim=2)
    out_reshaped = out_reshaped[nz_ix]

    out_reshaped2 = out_reshaped - out_reshaped.max(1, keepdims=True)
    q_denom = T.log(T.sum(T.exp(out_reshaped2), axis=1, keepdims=True))
    loss = T.sum(-out_reshaped2[y_batch_reshaped] + q_denom)

    # adjust for batch size
    return loss/pre_softmax_output.shape[0]


def masked_error_rate_2d(output, y_batch):
    """
    by convention 0 means 'no label'.
    TODO case when mask != 0
    """
    # because the theano magic for numerical stability
    # is not working after this reshape.

    y_batch_reshaped = y_batch.flatten()
    nz_ix = y_batch_reshaped.nonzero()
    y_batch_reshaped = y_batch_reshaped[nz_ix] - 1

    # input is (b,c,h,w). b*w*h samples, and a K unnormalized for each
    # b*w*h samples with a single index k for each
    # labels are in 0-K range inclusive but 0 is invalid/missing.
    BHW = output.shape[0]*output.shape[2]*output.shape[3]
    K = output.shape[1]
    out_reshaped = T.reshape(output.dimshuffle(0, 2, 3, 1), (BHW, K), ndim=2)
    out_reshaped = out_reshaped[nz_ix]

    #pred = T.argmax( out_reshaped, axis=1, keepdims=True )
    pred = T.argmax( out_reshaped, axis=1 )
    err = T.cast( T.mean( T.neq(pred, y_batch_reshaped) ), 'float32' )

    return err


def predict_2d(output):
    return T.argmax(output, axis=1, keepdims=True)


def error_rate_2d(output, y_batch):
    # assuming output is a softmax image with K channels, one for each class
    # assuming y_batch (ground truth) is a label image with
    # indexes in each pixel
    # input is (b,c,h,w). b*w*h samples, and a K-softmax for each
    # this would be a (b,1,h,w) label image
    pred = predict_2d(output)
    err = T.cast( T.mean( T.neq(pred, y_batch) ), 'float32' )
    return err


def unitize_normals_2d(tensor_var, epsilon=1e-7):
    """
    input is (bchw)
    """
    assert(tensor_var.ndim == 4)
    dtype = np.dtype(theano.config.floatX).type
    norms = T.sqrt(T.sum(T.sqr(tensor_var), axis=1, keepdims=True))
    constrained_output = (tensor_var * (1./(dtype(epsilon)+norms)))
    return constrained_output


def normal_loss_2d(output, y_batch):
    """
    input is (b,c,h,w). b*w*h samples, and a K unnormalized pred for each
    y_batch (ground truth) should be HxWx3 image with normals

    from DNS paper:
        - normalize vector w l2 norm, backprop through this norm
        - elementwise loss formula
        L(N, N*) = -1/n sum_i N_i . N_i^* = -1/n N . N^*
        where . is dot product, i is the ith pixel (of n)
        so at the end of the day we can vectorize the whole image map
        and do dot prod
    """
    nnz = output.shape[0]*output.shape[2]*output.shape[3]
    norm_output = unitize_normals_2d(output)
    loss = -T.sum(norm_output * y_batch)/nnz
    return loss


def masked_normal_loss_2d(output, y_batch):
    """
    by convention 0,0,0 normal means 'no label'.
    that naturally leads to 0 loss.
    however, the mean has to be adjusted to account
    for different number of pixels.
    """
    # number of nonzero pixels
    nnz = T.all(T.neq(y_batch, 0), axis=1).sum()

    norm_output = unitize_normals_2d(output)
    loss = -T.sum(norm_output * y_batch)/nnz
    return loss



def masked_smooth_depth_loss_2d(output, y_batch):
    """
    """

    nz_mask = T.neq(y_batch, 0.)
    B = output.shape[0]
    HW = output.shape[2]*output.shape[3]

    y_batch_reshaped = y_batch.reshape((B, HW))
    out_reshaped = output.reshape((B, HW))
    nz_mask_reshaped = nz_mask.reshape((B, HW))

    p = out_reshaped * nz_mask_reshaped
    t = y_batch_reshaped * nz_mask_reshaped
    d = (p - t)
    nnz = T.sum(nz_mask_reshaped, axis=1)
    num = T.sum(nnz*T.sum(d**2, axis=1))-0.5*T.sum(T.sum(d, axis=1)**2)
    den = T.maximum(T.sum(nnz**2), 1)
    depth_loss =  num / den

    h = 1
    p_di = (output[:,0,h:,:] - output[:,0,:-h,:]) * (1./np.float32(h))
    p_dj = (output[:,0,:,h:] - output[:,0,:,:-h]) * (1./np.float32(h))
    t_di = (y_batch[:,0,h:,:] - y_batch[:,0,:-h,:]) * (1./np.float32(h))
    t_dj = (y_batch[:,0,:,h:] - y_batch[:,0,:,:-h]) * (1./np.float32(h))
    m_di = nz_mask[:,0,h:,:] & nz_mask[:,0,:-h,:]
    m_dj = nz_mask[:,0,:,h:] & nz_mask[:,0,:,:-h]

    grad_loss_i = T.sum(m_di * (p_di - t_di)**2) / T.sum(m_di)
    grad_loss_j = T.sum(m_dj * (p_dj - t_dj)**2) / T.sum(m_dj)

    loss = depth_loss + grad_loss_i + grad_loss_j
    return loss/B


def masked_depth_loss_2d(output, y_batch):
    """
    """

    nz_mask = T.neq(y_batch, 0.)
    B = output.shape[0]
    HW = output.shape[2]*output.shape[3]

    y_batch_reshaped = y_batch.reshape((B, HW))
    out_reshaped = output.reshape((B, HW))
    nz_mask_reshaped = nz_mask.reshape((B, HW))

    p = out_reshaped * nz_mask_reshaped
    t = y_batch_reshaped * nz_mask_reshaped
    d = (p - t)
    nnz = T.sum(nz_mask_reshaped, axis=1)
    num = T.sum(nnz*T.sum(d**2, axis=1))-0.5*T.sum(T.sum(d, axis=1)**2)
    den = T.maximum(T.sum(nnz**2), 1)
    depth_loss =  num / den
    return depth_loss/B


def masked_mse_loss_2d(output, y_batch):
    """
    """

    nz_mask = T.neq(y_batch, 0.)
    B = output.shape[0]
    HW = output.shape[2]*output.shape[3]

    y_batch_reshaped = y_batch.reshape((B, HW))
    out_reshaped = output.reshape((B, HW))
    nz_mask_reshaped = nz_mask.reshape((B, HW))

    p = out_reshaped * nz_mask_reshaped
    t = y_batch_reshaped * nz_mask_reshaped
    d = (p - t)**2.
    nnz = T.sum(nz_mask_reshaped, axis=1)
    loss = T.mean(T.sum(d, axis=1)/(T.maximum(nnz, 1)))
    return loss


def masked_smooth_mse_loss_2d(output, y_batch):
    """
    """

    nz_mask = T.neq(y_batch, 0.)
    B = output.shape[0]
    HW = output.shape[2]*output.shape[3]

    y_batch_reshaped = y_batch.reshape((B, HW))
    out_reshaped = output.reshape((B, HW))
    nz_mask_reshaped = nz_mask.reshape((B, HW))

    p = out_reshaped * nz_mask_reshaped
    t = y_batch_reshaped * nz_mask_reshaped
    d = (p - t)**2.
    nnz = T.sum(nz_mask_reshaped, axis=1)
    depth_loss = T.sum(T.sum(d, axis=1)/(T.maximum(nnz, 1)))

    h = 1
    p_di = (output[:,0,h:,:] - output[:,0,:-h,:]) * (1./np.float32(h))
    p_dj = (output[:,0,:,h:] - output[:,0,:,:-h]) * (1./np.float32(h))
    t_di = (y_batch[:,0,h:,:] - y_batch[:,0,:-h,:]) * (1./np.float32(h))
    t_dj = (y_batch[:,0,:,h:] - y_batch[:,0,:,:-h]) * (1./np.float32(h))
    m_di = nz_mask[:,0,h:,:] & nz_mask[:,0,:-h,:]
    m_dj = nz_mask[:,0,:,h:] & nz_mask[:,0,:,:-h]

    grad_loss_i = T.sum(m_di * (p_di - t_di)**2) / T.sum(m_di)
    grad_loss_j = T.sum(m_dj * (p_dj - t_dj)**2) / T.sum(m_dj)

    loss = depth_loss + grad_loss_i + grad_loss_j

    return loss/B


def masked_exp_mse_loss_2d(output, y_batch):
    """
    """
    output = T.exp(output)
    y_batch = T.exp(y_batch)

    nz_mask = T.neq(y_batch, 0.)
    B = output.shape[0]
    HW = output.shape[2]*output.shape[3]

    y_batch_reshaped = y_batch.reshape((B, HW))
    out_reshaped = output.reshape((B, HW))
    nz_mask_reshaped = nz_mask.reshape((B, HW))

    p = out_reshaped * nz_mask_reshaped
    t = y_batch_reshaped * nz_mask_reshaped
    d = (p - t)**2.
    nnz = T.sum(nz_mask_reshaped, axis=1)
    loss = T.mean(T.sum(d, axis=1)/(T.maximum(nnz, 1)))
    return loss
