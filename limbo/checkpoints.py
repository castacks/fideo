"""
Save and load network state with npz.
"""

import sys
import json
import time

import numpy as np

import pyml_util

from spath import Path


log = fideo.get_logger(__name__)


def save_checkpoint(out_dir, net, state, overwrite=False):
    """ Checkpoint net to out_dir.

    State records itr_ctr, and may be used for other variables
    such as learning rate.

    Checkpoints are stored in out_dir, indexed by itr_ctr (in state).
    """

    out_dir = Path(out_dir)

    if not out_dir.exists():
        log.info('Making directory {}'.format(out_dir))
        out_dir.makedirs_p()

    itr_ctr = int(state.get('itr_ctr', 0))

    weights_fname = out_dir/('weights_{:06d}.npz'.format(itr_ctr))

    if not overwrite and weights_fname.exists():
        raise IOError('{} exists'.format(weights_fname))

    log.info('checkpointing to {}'.format(weights_fname))

    # weights is an ordered dict
    weights = net.get_weights()

    # npz doesn't preserve ordering of dictionary items.
    # most of the time it doesn't matter (since names
    # are unique) but sometimes it is slightly inconvenient.
    # we store param names in a list.

    ts = time.time()
    weights['_itr_ctr'] = int(itr_ctr)
    weights['_timestamp'] = float(ts)
    weights['_param_names'] = weight.keys()

    np.savez_compressed(weights_fname, **weights)

    state['timestamp'] = float(ts)
    state['itr_ctr'] = int(state['itr_ctr'])
    state_fname = weights_fname.stripext()+'.json'
    state_json = json.dumps(state, indent=2)
    state_fname.write_bytes(state_json)


def restore_checkpoint(net, out_dir):
    """ Restore weights to net from a directory
    containing potentially many checkpoints.
    """
    weights_fname = find_last_checkpoint(out_dir)
    return restore_checkpoint_from_file(net, weights_fname)


def find_last_checkpoint(out_dir, use_mtime=False):
    """ Find most recent checkpoint file.
    Sorts by itr_ctr in filename; if use_mtime is True, uses modified time.
    """

    log.info('searching for latest weights filename in {}'.format(out_dir))
    weight_fnames = out_dir.files('weights_*.npz')

    if len(weight_fnames)==0:
        log.error('No weight files found in {}; exiting'.format(out_dir))
        sys.exit(1)

    if use_mtime:
        key = lambda x: -x.mtime
    else:
        key = lambda x: -int(x.basename().stripext().split('_')[1])

    weights_fname = sorted(weight_fnames, key=key)[0]
    return weights_fname


def load_weights_from_file(weights_fname):
    """ Wrapper around np.load that restores parameter order.
    """
    npz = np.load(weights_fname)
    out = OrderedDict()

    # ensure parameter ordering is restored
    if '_param_names' in npz:
        param_names = map(str, npz['_param_names'])
        for param_name in param_names:
            out[param_name] = npz[param_name]
    else:
        for k, v in npz.iteritems():
            if k.startswith('_'):
                continue
            out[k] = v

    return out


def restore_checkpoint_from_file(net, weights_fname):
    """ Given weights filename, will restore weights to net.
    """

    log.info('loading weights from {}'.format(weights_fname))
    weights = load_weights_from_file(weights_fname)

    net.set_weights(weights)

    state_fname = weights_fname.stripext()+'.json'
    if state_fname.exists():
        state = json.loads(state_fname.bytes())
    else:
        log.warn('No state json file found.')
        state = {}
    if 'itr_ctr' in state:
        log.info('state[itr_ctr] is {}'.format(state[itr_ctr]))
    return state
