
import copy
from collections import OrderedDict
import traitlets as tr

import numpy as np
import treedict
import lasagne
import theano
import theano.tensor as T

import fideo

import fideo
from fideo import checkpoints
from fideo import monitoring

log = fideo.get_logger(__name__)


class Trainer(tr.HasTraits):
    """
    Takes in:
        - Model, which has net and loss function and theano vars
        - PipelineRunner, which runs pipeline (api: fill_chunk)
        - out_dir that checkpoints

        this initializes theano stuff, including training functions
        it also controls flow, including checkpointing.
        it calls callbacks()

        essential params: batch size, learnign rate, moementum, callbacks etc
        model, obv
        essential api: train_chunk()


    """

    model = tr.Any()
    reg = tr.Float(0.0001)
    save_every_nth = tr.Int(200)
    valid_every_nth = tr.Int(10000000)
    train_metric_every_nth = tr.Int(8)
    disp_every_nth = tr.Int(16)
    batch_size = tr.Int(16)
    batches_per_chunk = tr.Int(32)
    momentum = tr.Float(0.9)
    norm_constraint = tr.Float(3.)
    out_dir = tr.Unicode()
    init_weights_fname = tr.Unicode()

    # TODO make this accept functions
    updater = tr.Unicode('momentum')

    # called only when --debug is set
    debug_callbacks = tr.List()
    # called on every iteration
    iter_callbacks = tr.List()
    # called on validation
    valid_callbacks = tr.List()
    display_callbacks = tr.List()


    def __init__(self, model, pipeline_runner, out_dir):
        self.model = model
        self.pipeline_runner = pipeline_runner
        self.out_dir = out_dir
        self.args = treedict.getTree('args')


    @property
    def chunk_size(self):
        return self.batch_size*self.batches_per_chunk


    def theano_init(self):
        if self.args.debug:
            # override this so memory doesn't run out
            self.batches_per_chunk = 1

        self.itr_ctr_restored = 0
        if self.args.resume:
            log.info('resuming from checkpoint')
            self.state = checkpoints.restore_checkpoint(self.model, self.out_dir)
            self.itr_ctr_restored = copy.copy(int(self.state['itr_ctr']))
        elif self.args.scratch:
            log.info('initializing from scratch')
            self.state = {'itr_ctr': 0}
        else:
            #self.init_weights_fname: # TODO settable by cmdline
            log.info('initializing from pretrained weights ({})'.format(self.init_weights_fname))
            self.state = checkpoints.restore_checkpoint_from_file(self.model, self.init_weights_fname)
            self.state['itr_ctr'] = 0

        self.x_shared_tmp = np.zeros((self.chunk_size,)+self.model.x_dims[1:], dtype='f4')
        self.y_shared_tmp = np.zeros((self.chunk_size,)+self.model.y_dims[1:], dtype='f4')

        x_shared = lasagne.utils.shared_empty(len(self.model.x_dims), dtype='f4')
        y_shared = lasagne.utils.shared_empty(len(self.model.y_dims), dtype='f4')

        x = self.model.tvar['x']
        y = self.model.tvar['y']
        yhat = self.model.tvar['yhat']

        out = self.model.texpr['out']
        dout = self.model.texpr['dout']

        if self.args.debug:
            dump = self.model.texpr['dump']

        loss = self.model.texpr['loss']
        loss_val = self.model.texpr['loss_val']
        if self.reg > 0.:
            l2_norm = self.model.params_l2_norm()
            reg_loss = loss_val + self.reg*l2_norm
        else:
            reg_loss = loss_val

        batch_index = T.iscalar('batch_index')
        batch_size = self.batch_size
        batch_slice = slice(batch_index*batch_size, (batch_index+1)*batch_size)

        params = self.model.get_all_params(trainable=True)

        lr_val = self.learning_rate(self.state['itr_ctr'])

        if self.updater=='momentum':
            updates = lasagne.updates.momentum(reg_loss, params, lr_val, self.momentum)
        elif self.updater=='adam':
            updates = lasagne.updates.adam(reg_loss, params, lr_val)
        elif self.updater=='adadelta':
            updates = lasagne.updates.adadelta(reg_loss, params, lr_val)
        else:
            raise ValueError('unsupported updater. TODO fix this hackish code.')

        # TODO use tagging or better way to detect conv layers
        # TODO make this optional
        if self.norm_constraint > 0.:
            for param, update in updates.iteritems():
                if ((param.name is not None) and
                    (param.name.startswith('conv') or
                     param.name.startswith('deconv') or
                     param.name.startswith('nin')) and
                     param.name.endswith('.W')):
                    # TODO make this a param. hinton says 3-4. but that's 2d
                    updates[param] = lasagne.updates.norm_constraint(update,
                            self.norm_constraint)

        update_iter = theano.function(inputs=[batch_index],
                                      outputs=reg_loss,
                                      updates=updates,
                                      givens={x: x_shared[batch_slice],
                                              y: T.cast(y_shared[batch_slice], self.model.y_dtype),
                                             })

        self.tfunc, self.tvar = {}, {}
        self.tfunc['update_iter'] = update_iter
        self.tfunc['out'] = theano.function([x], out, allow_input_downcast=True)
        self.tfunc['dout'] = theano.function([x], dout, allow_input_downcast=True)
        if self.args.debug:
            self.tfunc['dump'] = theano.function([x], dump, allow_input_downcast=True)
        self.tfunc['loss'] = theano.function([yhat, y], loss, allow_input_downcast=True)
        self.tvar['x_shared'] = x_shared
        self.tvar['y_shared'] = y_shared
        self.tvar['learning_rate'] = theano.shared(np.float32(lr_val))
        self.tvar['batch_index'] = batch_index
        self.tvar['batch_slice'] = batch_slice

        # TODO configurable err_rate, pred functions

        self.tfunc['train_metrics'] = {}
        for mname, mexpr in self.model.texpr['train_metrics'].iteritems():
            mfn = theano.function([batch_index], mexpr, givens={
                    x: x_shared[batch_slice],
                    y: T.cast(y_shared[batch_slice], self.model.y_dtype),
                }, allow_input_downcast=True)
            self.tfunc['train_metrics'][mname] = mfn

        log.info(self.model.repr_net_shapes())



    def train_chunk(self):

        #weights = self.model.get_weights()
        #wisnan = [k for k,v in weights.items() if np.any(np.isnan(v))]
        #import ipdb; ipdb.set_trace()

        itr_ctr = int(self.state['itr_ctr'])

        log.info('loading chunk')
        self.pipeline_runner.fill_chunk(self.x_shared_tmp, self.y_shared_tmp)

        num_batches = self.x_shared_tmp.shape[0]//self.batch_size
        log.info('num_batches = {}'.format(num_batches))

        lr = np.float32(self.tvar['learning_rate'].get_value())
        new_lr = self.learning_rate(itr_ctr)
        if np.abs(new_lr-lr)>1e-6:
            log.warn('decreasing learning rate form {} to {}'.format(lr, new_lr))
            self.tvar['learning_rate'].set_value(np.float32(new_lr))

        # load chunk into GPU
        self.tvar['x_shared'].set_value(self.x_shared_tmp, borrow=True)
        self.tvar['y_shared'].set_value(self.y_shared_tmp, borrow=True)

        if self.args.debug:
            for cb in self.debug_callbacks:
                cb(self)

        #weights = self.model.get_weights()
        #wisnan = [k for k,v in weights.items() if np.any(np.isnan(v))]
        #import ipdb; ipdb.set_trace()

        for bi in xrange(num_batches):
            lv = self.tfunc['update_iter'](bi)

            msg_dict = OrderedDict()
            msg_dict['itr_ctr'] = int(itr_ctr)
            msg_dict['bi'] = bi
            msg_dict['lv'] = lv

            # if train_metric_every_nth is valid (value > 0)
            # the evaluate every n iterations.
            # funcs['train_metrics'] are functions called on the data.
            if (('train_metrics' in self.tfunc) and
                (self.train_metric_every_nth > 0) and
                (itr_ctr%self.train_metric_every_nth==0)):
                for mname, mfn in self.tfunc['train_metrics'].iteritems():
                    mval = mfn(bi)
                    msg_dict[mname] = mval

            for cb in self.iter_callbacks:
                cb(self, msg_dict)

            if (itr_ctr % self.valid_every_nth) == 0:
                for cb in self.valid_callbacks:
                    cb(self)

            if (itr_ctr % self.disp_every_nth) == 0:
                for cb in self.display_callbacks:
                    cb(self)

            # import pdb; pdb.set_trace()
            if ((itr_ctr > self.itr_ctr_restored) and
                ((itr_ctr % self.save_every_nth) == 0)):
                # TODO pluggable checkpoint
                log.info('checkpointing...')
                checkpoints.checkpoint_training(self.out_dir, self.model,
                                                self.state, self.args.overwrite)
            itr_ctr += 1
            self.state['itr_ctr'] = itr_ctr


    def train(self):
        while True:
            self.train_chunk()
