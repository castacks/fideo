
import base64
import cStringIO as StringIO

from PIL import Image
import cv2
import matplotlib as mpl
import matplotlib.cm as cm
import matplotlib.pyplot as pl
import numpy as np

# good enough for most purposes
ALEXNET_MEAN_BGR = np.array([104.00653839, 116.66886139, 122.67784119], dtype='f4')


def unpreprocess(imgpp):
    """
    given float32 bgr image shaped as (3, height, width), centered
    around alexnet mean bgr, return uint8 rgb image shaped as (height, width, 3).
    """
    if imgpp.ndim!=3:
        raise ValueError('imgpp must have shape (3, height, width)')
    if imgpp.shape[0]!=3:
        raise ValueError('imgpp must have shape (3, height, width)')
    if imgpp.dtype not in (np.float32, np.float64):
        raise ValueError('imgpp must have float type')
    imgpp2 = imgpp.copy()
    imgpp2 += ALEXNET_MEAN_BGR[:, None, None]
    imgpp2 = imgpp2.clip(0, 255).astype('u1')
    imgpp2 = imgpp2.transpose(1,2,0)
    imgpp2 = cv2.cvtColor(imgpp2, cv2.COLOR_BGR2RGB)
    return imgpp2


def viz_montage(xi, yi, md):
    # TODO use the non-pyimg version
    import pyimg

    class_id_to_rgb = md['class_id_to_rgb']
    #class_id_to_rgb = expt.md['class_id_to_rgb']
    for k,v in class_id_to_rgb.iteritems():
        class_id_to_rgb[k] = np.asarray(v)
    label_to_rgb = pyimg.cyimg.RgbRemapper(class_id_to_rgb)

    x_i = Image.fromarray(x_i).resize(new_size)

    y_i = label_to_rgb(np.ascontiguousarray(yi))
    y_i = Image.fromarray(y_i)
    y_i = y_i.resize(x_i.size, Image.NEAREST)

    yh_i = label_to_rgb(np.ascontiguousarray(limg_hat))
    yh_i = Image.fromarray(yh_i)
    yh_i = yh_i.resize(x_i.size, Image.NEAREST)

    blend_i = Image.blend(x_i, yh_i, .5)
    blend_i_elt = pyhtml.encoders.image_to_html(blend_i, fmt='jpeg')


def normal_to_color_image(normal_img):
    assert(normal_img.ndim==3)
    assert(normal_img.shape[0]==3 or normal_img.shape[2]==3)
    if normal_img.shape[0]==3:
        normal_img = normal_img.transpose((1,2,0))
    invalid = np.all(normal_img==0., axis=2, keepdims=True)
    # normalize
    norms = np.sqrt((normal_img**2).sum(2, keepdims=True))+1e-6
    normal_img = normal_img/norms
    # scale from -1, 1 to 0, 255
    normal_img = (((normal_img/2.) + 0.5)*255).clip(0, 255).astype(np.uint8)
    return normal_img


#def show_normals(i):
#    yhati = yhat[i]
#    yhati_norm = np.sqrt((yhati**2).sum(0))
#    yhati = yhati/yhati_norm
#    yhimg = ((yhati/2.+0.5)*255).transpose((1,2,0)).astype(np.uint8)
#    yimg = ((y_shared_tmp[i]/2.+0.5)*255).transpose((1,2,0)).astype(np.uint8)
#    disp.image(yimg, title='y', win='y')
#    disp.image(yhimg, title='yhat', win='yhat')


def log_depth_to_color_image(depth_img, min_range=1., max_range=200.):
    """ NOTE LOG10
    """
    log_min = np.log10(min_range)
    log_max = np.log10(max_range)

    depth_img = depth_img.squeeze()
    norm = mpl.colors.Normalize(vmin=log_min, vmax=log_max, clip=True)
    cmap = mpl.cm.jet
    smap = pl.cm.ScalarMappable(norm=norm, cmap=cmap)
    # import IPython; IPython.embed()
    dimgs = (smap.to_rgba(depth_img)[:,:,:3]*255).astype(np.uint8)
    # Image.blend(Image.fromarray(img), Image.fromarray(dimgs), .5)
    return dimgs


def scalar_to_color_figure(depth_img, colorbar=True, **kwargs):
    fig = pl.figure()
    gca = pl.gca()
    gca.axes.get_xaxis().set_visible(False)
    gca.axes.get_yaxis().set_visible(False)
    # pl.hot()
    #if depth_img.ndim==4:
    #    depth_img = depth_img[0]
    pl.imshow(depth_img, interpolation='nearest', **kwargs)
    if colorbar:
        pl.colorbar()
    return fig
