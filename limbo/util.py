
import numpy as np


def one_hot(vec, m=None):
    if m is None:
        m = int(np.max(vec)) + 1
    return np.eye(m)[vec]


def one_hot_image(label_image,
                  classes=None,
                  ignore_zeros=True,
                  order='c01',
                  dtype='float32'):
    if classes is None:
        classes = np.unique(label_image)
    one_hot_vecs = np.eye(np.max(classes)+1, dtype=dtype)
    if order=='01c':
        if ignore_zeros:
            return one_hot_vecs[label_image][...,1:]
        return one_hot_vecs[label_image]
    elif order=='c01':
        if ignore_zeros:
            return np.rollaxis(one_hot_vecs[label_image][...,1:], 2)
        return np.rollaxis(one_hot_vecs[label_image], 2)
    else:
        raise ValueError('order must be c01 or 01c')


def un_one_hot_image(label_image,
                     classes=None,
                     ignore_zeros=True,
                     dtype='float32'):
    if classes is None:
        classes = np.unique(label_image)
    one_hot_vecs = np.eye(np.max(classes)+1, dtype=dtype)
    if ignore_zeros:
        return one_hot_vecs[label_image][...,1:]
    return one_hot_vecs[label_image]


def log_losses(y, t, eps=1e-15):
    if t.ndim == 1:
        t = one_hot(t)

    y = np.clip(y, eps, 1 - eps)
    losses = -np.sum(t * np.log(y), axis=1)
    return losses


def log_loss(y, t, eps=1e-15):
    """
    cross entropy loss, summed over classes, mean over batches
    """
    losses = log_losses(y, t, eps)
    return np.mean(losses)


def accuracy(y, t):
    if t.ndim == 2:
        t = np.argmax(t, axis=1)
    predictions = np.argmax(y, axis=1)
    return np.mean(predictions == t)


def softmax(x):
    m = np.max(x, axis=1, keepdims=True)
    e = np.exp(x - m)
    return e / np.sum(e, axis=1, keepdims=True)


def entropy(x):
    h = -x * np.log(x)
    h[np.invert(np.isfinite(h))] = 0
    return h.sum(1)


def conf_matrix(p, t, num_classes):
    if p.ndim == 1:
        p = one_hot(p, num_classes)
    if t.ndim == 1:
        t = one_hot(t, num_classes)
    return np.dot(p.T, t)


def accuracy_topn(y, t, n=5):
    if t.ndim == 2:
        t = np.argmax(t, axis=1)

    predictions = np.argsort(y, axis=1)[:, -n:]

    accs = np.any(predictions == t[:, None], axis=1)

    return np.mean(accs)


def log_loss_std(y, t, eps=1e-15):
    """
    cross entropy loss, summed over classes, mean over batches
    """
    losses = log_losses(y, t, eps)
    return np.std(losses)
