""" Convenience functions for logging.
"""

import sys
import logging

__all__ = ['logging_config',
           'get_logger',
           ]


def logging_config(log_fname=None, level=logging.INFO):
    """ Simple default config for logging.
    By default, log to console. If log_fname is
    given, statements are logged to log_fname as well.

    :parameters:
        - log_fname: string
            filename to store log

    :todo:
        - support for file handles
    """

    #logging.basicConfig(level=logging.DEBUG)
    root_logger = logging.getLogger()
    root_logger.setLevel(level)
    human_log_fmt = logging.Formatter('%(asctime)s|%(name)s|%(levelname)s: %(message)s')
    ch = logging.StreamHandler(sys.stdout)
    ch.setFormatter(human_log_fmt)
    root_logger.addHandler(ch)

    computer_log_fmt = logging.Formatter('%(created)s|%(name)s|%(pathname)s|%(lineno)d|%(levelname)s|%(message)s')
    if not log_fname is None:
        fh = logging.FileHandler(log_fname)
        fh.setFormatter(computer_log_fmt)
        root_logger.addHandler(fh)


def get_logger(name):
    """ convenience function to save an import stmt.
    """
    return logging.getLogger(name)
