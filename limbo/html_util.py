
from jinja2 import Template, DictLoader, Environment

_page_template = """\
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <title>{% block title %}{% endblock %}</title>
        {# <link rel="stylesheet" href="style.css"> #}
        {# <script src="script.js"></script> #}
        <style>
        {% block css %}
        {% endblock %}
        </style>
    </head>
    <body>
    {% block body %}
    {% endblock %}
    </body>
</html>
"""

_table_template = """\
{% extends "page.html" %}
{% block body %}
<table>
    <tr>
        {% for hdr in header %}
        <th>{{hdr}}</th>
        {% endfor %}
    </tr>
    {% for row in rows %}
    <tr>
        {% for elt in row %}
        <td>{{elt}}</td>
        {% endfor %}
    </tr>
    {% endfor %}
</table>
{% endblock %}
"""


loader = DictLoader({'page.html': _page_template,
                     'table.html': _table_template})
env = Environment(loader=loader)
page_template = env.get_template('page.html')
table_template = env.get_template('table.html')


def table_page(header, rows):
    return table_template.render(header=header, rows=rows)
