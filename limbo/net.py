"""
Container for networks.
Meant as a step above using an OrderedDict.
"""

import warnings
from collections import OrderedDict

import numpy as np
import lasagne

from lasagne import nonlinearities
from lasagne.regularization import regularize_network_params
from lasagne.layers import BatchNormLayer
from lasagne.layers import NonlinearityLayer
from lasagne.layers import DropoutLayer
from lasagne.layers import GaussianNoiseLayer

import fideo

log = fideo.get_logger(__name__)


class Net(object):

    def __init__(self):
        self.input_layer = None
        self.output_layer = None
        self.layers = OrderedDict()

        self.tvars = OrderedDict()
        self.texpr = OrderedDict()


    @property
    def input_layer(self):
        if self.input_layer is not None:
            return self.input_layer
        if len(self.layers)==0:
            return None
        return self.layers.values()[0]


    @input_layer.setter
    def input_layer(self, value):
        assert(value in self.layers.values())
        self.input_layer = value


    @property
    def output_layer(self):
        if self.output_layer is not None:
            return self.output_layer
        if len(self.layers)==0:
            return None
        return self.layers.values()[-1]


    @output_layer.setter
    def output_layer(self, value):
        assert(value in self.layers.values())
        self.output_layer = value


    def get_all_params(self, layer=None, **tags):
        if layer is None:
            layer = self.output_layer
        if layer is None:
            return []
        return lasagne.layers.get_all_params(layer, **tags)


    def get_all_layers(self, layer=None):
        if layer is None:
            layer = self.output_layer
        if layer is None:
            return []
        return lasagne.layers.get_all_layers(layer)


    def params_l2_norm(self, layer=None):
        if layer is None:
            layer = self.output_layer
        if layer is None:
            return 0.
        return regularize_network_params(layer, lasagne.regularization.l2)


    def params_l1_norm(self, layer=None):
        if layer is None:
            layer = self.output_layer
        if layer is None:
            return 0.
        return regularize_network_params(layer, lasagne.regularization.l1)


    def repr_net_shapes(self):
        layer_shape = '\n'.join(
                ['{} : {}'.format(layer.name, layer.output_shape)
                    for layer in self.get_all_layers()])
        return layer_shape


    def make_net_diagram(self, fname):
        from . import draw_net
        if len(self)==0:
            raise ValueError('no layers in net')
        draw_net.draw_to_file(lasagne.layers.get_all_layers(self['l_out']),
                              fname,
                              verbose=True)


    def _sanity_check(self):
        params = self.get_all_params()
        names = [par.name for par in params]
        if len(names)!=len(set(names)):
            raise ValueError('net needs unique param names')
        for name, layer in self.layers.items():
            # l_in and l_out are special; aliasing is allowed.
            if name != layer.name:
                raise ValueError('net layer names and keys should be the same')


    def set_weights(self, weights):
        """ weights has a dictionary interface.

        TODO: policy for excess/too few parameters.
        """

        self._sanity_check()

        params = self.get_all_params()
        if len(params)==0:
            log.warn('set_weights called, but no parameters in model')
            return

        # note we ignore any other stuff that may be in weights
        for param in params:
            if param.name in weights:
                stored_shape = np.asarray(weights[param.name].shape)
                param_shape = np.asarray(param.get_value().shape)
                if not np.all(stored_shape == param_shape):
                    # TODO configurable policy
                    warn_msg = 'shape mismatch:'
                    warn_msg += '{} stored:{} new:{}'.format(param.name, stored_shape, param_shape)
                    warn_msg += ' skipping'
                    log.warn(warn_msg)
                else:
                    param.set_value(weights[param.name])
            else:
                log.warn('unable to load parameter {} from weights'.format(param.name))


    def get_weights(self):
        params = self.get_all_params()
        param_dict = OrderedDict()
        for param in params:
            param_dict[param.name] = param.get_value(borrow=False)
        return param_dict
