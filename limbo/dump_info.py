import sys

from spath import Path

from . import draw_net


log = fideo.get_logger(__name__)


def dump_layer_info(cfg, layers):
    layer_shape = '\n'.join(
            ['{} : {}'.format(layer.name, layer.output_shape)
                for layer in layers])
    log.info('layer_shape: {}'.format(layer_shape))
    draw_net.draw_to_file(layers,
                          Path(cfg.output_dir)/('network_diagram.png'),
                          verbose=True)
