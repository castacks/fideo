"""
TODO!
needs cleanup and configurability
"""

import warnings
from collections import OrderedDict
from multiprocessing import Process
from datetime import datetime

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as pl
from matplotlib import rcParams

import numpy as np
import pandas as pd
import cv2

try:
    import pydisp as disp
except ImportError, ex:
    warnings.warn('pydisplay is not installed')
    disp = None

from spath import Path

import pyml_util
from pyml_util import training_log

from pycolor import distinct_colors
from pycolor import colour
from pyimg2 import remapping
import fideo.util

log = pyml_util.get_logger(__file__)


def set_matplotlib_params():
    rcParams['axes.labelsize'] = 12
    rcParams['xtick.labelsize'] = 12
    rcParams['ytick.labelsize'] = 12
    rcParams['legend.fontsize'] = 12
    #rcParams['font.family'] = 'serif'
    #rcParams['font.serif'] = ['Computer Modern Roman']
    #rcParams['text.usetex'] = True
    #rcParams['figure.figsize'] = 7.3, 4.2
    golden_ratio = (np.sqrt(5)-1.0)/2.0
    fig_width_in = 8.
    fig_height_in = fig_width_in * golden_ratio
    #fig_dims = [fig_width_in, fig_height_in]
    rcParams['figure.figsize'] = [fig_width_in, fig_height_in]


def display_dump(layers, dump):
    """ show layer content with pydisplay
    dump: list of arrays with outputs
    """
    if disp is None:
        log.error('pydisplay is not available and required for display_dump')
        return
    for ix, layer in enumerate(layers):
        if dump[ix].ndim==4:
            disp.image(dump[ix][0][0],
                    win='layer%d'%ix, title='layer %d %s'%(ix, layer.name))
        elif dump[ix].ndim==2:
            pdata = np.c_[np.arange(dump[ix].size), dump[ix].flatten()]
            disp.dyplot(pdata, win='layer%d'%ix, title='layer %d %s'%(ix, layer.name))


def display_metrics(fname, keys, last_start=False, use_mpl=False):
    """ show metrics from training log in fname with pydisplay
    """
    if disp is None:
        log.error('pydisplay is not available and required for display_metrics')
        return
    recs = list(training_log.read_records(fname))
    log.info('found {} recs'.format(len(recs)))
    to_stamp = lambda r: np.datetime64(datetime.fromtimestamp(r['_stamp']))
    stamps = map(to_stamp, recs)
    df = pd.DataFrame(recs, index=stamps)
    del df['_stamp']
    for k in keys:
        df[k] = df[k].astype(np.float)
    df = df.sort_index()
    # TODO this loses resolution on loss

    if last_start:
        ix0 = df[(df['itr_ctr'].diff() < 0)].index[-1]
        df = df.loc[ix0:]

    #loss = df['loss'].sort_index()
    #loss.index = df.sort_index()['itr_ctr']

    df = df.dropna(axis=0)
    # df = df[df['loss'] < 10.]
    if use_mpl:
        for k in keys:
            smoothing_window = 32
            set_matplotlib_params()
            fig = pl.figure()
            val = df[k].astype(np.float)
            val.plot(label='raw')
            pd.rolling_mean(val, smoothing_window).plot(label='smoothed')
            pl.xlabel('Iter')
            pl.ylabel(k)
            pl.legend()
            fig.tight_layout(pad=0.1)
            disp.pylab(fig)

    else:
        for k in keys:
            data = df[['itr_ctr', k]].dropna(axis=0).astype(np.float32)
            log.info('plotting {}, {} values'.format(k, data.shape))
            disp.dyplot(data.values, labels=['itr', k], win=k, title=k)


class PastalogMonitoring(object):
    """ callback to post on pastalog
    """

    def __init__(self, name, metrics=['lv'], port=8120):
        import pastalog
        self.metrics = metrics
        self.port = port
        self.pastalog_logger = pastalog.Log('http://localhost:{}'.format(port), name)

    def __call__(self, trainer, msg_dict):
        itr_ctr = int(msg_dict['itr_ctr'])
        try:
            # self.pastalog_logger.post('lv', value=float(msg_dict['lv']), step=itr_ctr)
            for k, v in msg_dict.items():
                if k in self.metrics:
                    self.pastalog_logger.post(k, value=float(v), step=itr_ctr)

            # self.pastalog_logger.post(mname, value=float(mval), step=itr_ctr)
        except IOError, ex:
            log.error('error with pastalog: {}'.format(ex))


class TrainingLogger(object):
    """ callback to log to training logger
    """

    def __init__(self, out_fname):
        import pyml_util
        self.training_logger = pyml_util.training_log.TrainingLogger(out_fname)

    def __call__(self, trainer, msg_dict):
        rlog = pyml_util.get_logger('report')
        msg = 'train, '
        for k, v in msg_dict.iteritems():
            msg += '{} : {}, '.format(k, v)
        rlog.info(msg)
        #log.info("train, itr_ctr: {}, bi: {:2d}, loss: {:.4f}".format(itr_ctr, bi, float(lv)))
        #log.info("train, itr_ctr: {}, bi: {:2d}, loss: {:.4f}, acc: {:.4f}".format(itr_ctr, bi, float(lv), float(acc)))
        self.training_logger.log(msg_dict)


class DisplayBinaryLabels(object):

    def __init__(self):
        pass

    def __call__(self, trainer):
        import fideo.viz_util
        x_shared_tmp = trainer.x_shared_tmp
        y_shared_tmp = trainer.y_shared_tmp
        r_ix = np.random.randint(len(x_shared_tmp))
        yhat = np.asarray(trainer.tfunc['dout'](x_shared_tmp[r_ix][np.newaxis,...]))
        yhat = 1./(1.+np.exp(-yhat))

        xviz = fideo.viz_util.unpreprocess(x_shared_tmp[r_ix])
        yviz = pl.cm.jet(y_shared_tmp[r_ix,0], bytes=True)[...,:3]
        if yviz.shape != xviz.shape:
            yviz = cv2.resize(yviz, (xviz.shape[1], xviz.shape[0]), None, 0, 0, cv2.INTER_NEAREST)

        yhat_viz = pl.cm.jet(yhat[0,0], bytes=True)[...,:3]
        if yhat_viz.shape != xviz.shape:
            yhat_viz = cv2.resize(yhat_viz, (xviz.shape[1], xviz.shape[0]), None, 0, 0, cv2.INTER_NEAREST)

        yviz_blend = cv2.addWeighted(yviz, 0.5, xviz, 0.5, 0.)
        yhat_viz_blend = cv2.addWeighted(yhat_viz, 0.5, xviz, 0.5, 0.)
        # zero_img = np.zeros_like(xviz)

        xyviz0 = np.concatenate((xviz, yviz, yhat_viz), 1)
        xyviz1 = np.concatenate((xviz, yviz_blend, yhat_viz_blend), 1)
        xyviz = np.concatenate((xyviz0, xyviz1), 0)

        disp.image(xyviz, win='xyviz', title='xyviz')


class DisplayLabels(object):

    def __init__(self, num_classes=None, palette=None):
        if palette is None:
            if num_classes is None:
                # reasonable upper bound?
                num_classes = 256
            palette = OrderedDict()
            # class 0 is invalid/black; we add 1
            for ix in xrange(num_classes+1):
                hex_color = distinct_colors[ix]
                palette[ix] = colour.hex2rgb(hex_color, to_float=False)
        self.palette = palette

    def __call__(self, trainer):
        import fideo.viz_util
        x_shared_tmp = trainer.x_shared_tmp
        y_shared_tmp = trainer.y_shared_tmp
        # sample an image
        r_ix = np.random.randint(len(x_shared_tmp))
        y = y_shared_tmp[r_ix, 0].astype('int32')
        yhat = np.asarray(trainer.tfunc['dout'](x_shared_tmp[r_ix][None,...]))
        # yhat = fideo.util.softmax(yhat)
        yhat_argmax = np.argmax(yhat, 1)[0]+1 # network has no notion of 0

        xviz = fideo.viz_util.unpreprocess(x_shared_tmp[r_ix])
        # colorize images
        # import pdb; pdb.set_trace()
        yviz = remapping.label_image_to_rgb_image(y, self.palette)
        yhat_viz = remapping.label_image_to_rgb_image(yhat_argmax, self.palette)

        # resize if necessary
        if yviz.shape != xviz.shape:
            yviz = cv2.resize(yviz, (xviz.shape[1], xviz.shape[0]), None, 0, 0, cv2.INTER_NEAREST)
        if yhat_viz.shape != xviz.shape:
            yhat_viz = cv2.resize(yhat_viz, (xviz.shape[1], xviz.shape[0]), None, 0, 0, cv2.INTER_NEAREST)

        # blendy blends
        yviz_blend = cv2.addWeighted(yviz, 0.5, xviz, 0.5, 0.)
        yhat_viz_blend = cv2.addWeighted(yhat_viz, 0.5, xviz, 0.5, 0.)

        # witout blend row
        xyviz0 = np.concatenate((xviz, yviz, yhat_viz), 1)
        # with blend row
        xyviz1 = np.concatenate((xviz, yviz_blend, yhat_viz_blend), 1)
        # cat
        xyviz = np.concatenate((xyviz0, xyviz1), 0)

        disp.image(xyviz, win='xyviz', title='xyviz')



class DisplayImage(object):

    def __init__(self):
        pass

    def __call__(self, trainer):
        import fideo.viz_util
        x_shared_tmp = trainer.x_shared_tmp
        r_ix = np.random.randint(len(x_shared_tmp))
        xviz = fideo.viz_util.unpreprocess(x_shared_tmp[r_ix])
        disp.image(xviz, win='xviz', title='xviz')


class DisplayImageAndLabelVec(object):

    def __init__(self):
        pass

    def __call__(self, trainer):
        import fideo.viz_util
        x_shared_tmp = trainer.x_shared_tmp
        r_ix = np.random.randint(len(x_shared_tmp))
        xviz = fideo.viz_util.unpreprocess(x_shared_tmp[r_ix])
        y = trainer.y_shared_tmp[r_ix]
        yhat = trainer.tfunc['dout'](trainer.x_shared_tmp[r_ix][np.newaxis,...])
        # ytxt = str(trainer.y_shared_tmp[r_ix])
        ytxt = 'y = ' + str(y) + '<br> yhat = ' + str(yhat)
        disp.image(xviz, win='xviz', title='xviz')
        disp.text(ytxt, win='yhat_vec', title='yhat_vec')
