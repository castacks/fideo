

# TODO some sort of currying. could this work?
class Namer(object):
    def __init__(self, Layer, *args, **kwargs):
        self.Layer = Layer
        self.args = args
        self.kwargs = kwargs

    def __call__(self, net, name):
        self.kwargs['name'] = name
        layer = self.Layer(*self.args, **self.kwargs)
        net.layers[layer.name] = layer
