import numpy as np

import lasagne
from lasagne.utils import floatX


class Prelu(lasagne.init.Initializer):
    """ He's "prelu" initialization.
    Note: HeInit is now in Lasagne.
    """

    def sample(self, shape):
        # eg k^2 for conv2d
        receptive_field_size = np.prod(shape[2:])
        c = shape[1] # input channels
        nl = c*receptive_field_size
        std = np.sqrt(2.0/(nl))
        return floatX(np.random.normal(0, std, size=shape))
