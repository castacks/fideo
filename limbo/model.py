
import warnings
from collections import OrderedDict

import numpy as np
import traitlets as tr
import theano.tensor as T
import lasagne
from lasagne.regularization import regularize_network_params

import pyml_util

from .net import Net

log = fideo.get_logger(__name__)


class Model(objects):

    net = tr.Instance(klass=Net)
    y_dtype = tr.Unicode('int32')

    # this is spatial dimensions, if applicable
    # for normals, y_channels is 3.
    # for softmax classification, y_channels is 1.

    # for 2D structured i/o is typically BC01
    # None is for dynamic batch sizes

    # typical rgb is (None, 3, 227, 227)
    x_dims = tr.Tuple((None, 3, 227, 227))

    # typical segmantic segmentation: (None, 1, 227, 227)
    # typical classif: (None, 1)
    y_dims = tr.Tuple((None, None, 227, 227))
    # typical segmantic segmentation: (None, num_classes, 227, 227)
    # typical classif: (None, num_classes)
    yhat_dims = tr.Tuple((None, None, 227, 227))

    batch_size = tr.Int(16)

    tvar = tr.Dict()
    texpr = tr.Dict()
    tfunc = tr.Dict()

    def __init__(self, net, y_dtype, x_dims, y_dims, yhat_dims,
                 loss, train_metrics):

        self.net = net
        self.y_dtype = y_dtype
        self.x_dims = x_dims
        self.y_dims = y_dims
        self.yhat_dims = yhat_dims
        self.loss = loss
        self.train_metrics = train_metrics
        # self._create_expr()


    def init(self):
        """ create theano variables.
        """
        ndim_x = len(self.x_dims)
        ndim_y = len(self.y_dims)
        ndim_yhat = len(self.yhat_dims)

        self.tvar['x'] = T.TensorType('float32', [False]*ndim_x)('x')
        self.tvar['y'] = T.TensorType(self.y_dtype, [False]*ndim_y)('y')
        self.tvar['yhat'] = T.TensorType('float32', [False]*ndim_yhat)('yhat')

        l_out = self.net['l_out']
        self.texpr['out'] = lasagne.layers.get_output(l_out, self.tvar['x'])
        self.texpr['dout'] = lasagne.layers.get_output(l_out, self.tvar['x'], deterministic=True)

        self.texpr['dump'] = [lasagne.layers.get_output(l, self.tvar['x'], deterministic=True) for l in self.net.values()]

        # function of x, y
        self.texpr['loss_val'] = self.loss(self.texpr['out'], self.tvar['y'])

        # function of any yhat, y
        self.texpr['loss'] = self.loss(self.tvar['yhat'], self.tvar['y'])

        self.texpr['train_metrics'] = {}
        for tm_name, tm_expr in self.train_metrics.items():
            self.texpr['train_metrics'][tm_name] = tm_expr(self.texpr['dout'], self.tvar['y'])
