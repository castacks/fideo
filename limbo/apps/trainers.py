import sys

import argparse

from spath import Path

import fideo

from parafina.pipeline.serial_runner import SerialRunner

log = fideo.get_logger(__name__)


class SupervisedTrainerApp(object):

    def __init__(self, exp_id):
        self.exp_id = exp_id


    def update_config(self, args):
        self.data_base_dir = Path(args.data_dir)
        self.output_base_dir = Path(args.out_dir)

        self.output_dir = self.output_base_dir/exp_id


    def build_trainer(self):
        pass


    def dump_info(self):
        log.info('theano version: {}'.format(theano.__version__))
        log.info('lasagne version: {}'.format(lasagne.__version__))
        log.info('cmd: {}'.format(' '.join(sys.argv)))
        log.info('output dir is {}'.format(self.output_dir))
        #log.info(model.repr_net_shapes())
        #log.info('layer_shape: {}'.format(layer_shape))
        #model.make_net_diagram(fname)


    def train(self, args):
        print('train', args)

        self.update_config(args)

        self.dump_info()

        print('making output directory: {}'.format(self.output_dir))
        out_dir = self.output_dir.makedirs_p()

        fideo.logging_config(self.output_dir/('train.log'))

        model = self.build_model()

        data_src = self.build_data_src('train')
        data_pipeline = self.build_data_pipeline('train')

        pipeline_runner = SerialRunner(data_src,
                                       data_pipeline,
                                       self.x_key,
                                       self.y_key)
        # TODO zmq option
        trainer = self.build_trainer()
        trainer.train()



    def cli(self):
        parser = argparse.ArgumentParser()
        subparsers = parser.add_subparsers()

        train_parser = subparsers.add_parser('train')
        train_parser.add_argument('--weights',
                                  type=str,
                                  help='weights for initialization')
        train_parser.add_argument('-r', '--resume',
                                  action='store_true',
                                  help='resume from checkpoint')
        train_parser.add_argument('-d', '--debug',
                                  action='store_true',
                                  help='activate debugging')
        train_parser.add_argument('--data-dir',
                                  type=str,
                                  help='data base directory')
        train_parser.add_argument('--out-dir',
                                  type=str,
                                  help='output base directory')
        train_parser.add_argument('-j', '--workers',
                                  type=int,
                                  default=1,
                                  help='number of workers')
        train_parser.set_defaults(func=self.train)

        args = parser.parse_args()
        args.func(args)
