"""
adapted from lasagne/recipes
"""

from collections import OrderedDict

from lasagne.layers import InputLayer
from lasagne.layers import DenseLayer
from lasagne.layers import NonlinearityLayer
from lasagne.layers import DropoutLayer
from lasagne.layers import Pool2DLayer as PoolLayer
from lasagne.layers.dnn import Conv2DDNNLayer as ConvLayer
from lasagne.nonlinearities import softmax

from fideo.net import Net

def build_net(num_classes=1000, trainable=True):
    net = Net()
    net.add_layer('input', InputLayer((None, 3, 224, 224)))
    net.add_layer('conv1_1', ConvLayer(net.last_layer, 64, 3, pad=1, flip_filters=False))
    net.add_layer('conv1_2', ConvLayer(net.last_layer, 64, 3, pad=1, flip_filters=False))
    net.add_layer('pool1', PoolLayer(net.last_layer, 2))
    net.add_layer('conv2_1', ConvLayer(net.last_layer, 128, 3, pad=1, flip_filters=False))
    net.add_layer('conv2_2', ConvLayer(net.last_layer, 128, 3, pad=1, flip_filters=False))
    net.add_layer('pool2', PoolLayer(net.last_layer, 2))
    net.add_layer('conv3_1', ConvLayer(net.last_layer, 256, 3, pad=1, flip_filters=False))
    net.add_layer('conv3_2', ConvLayer(net.last_layer, 256, 3, pad=1, flip_filters=False))
    net.add_layer('conv3_3', ConvLayer(net.last_layer, 256, 3, pad=1, flip_filters=False))
    net.add_layer('pool3', PoolLayer(net.last_layer, 2))
    net.add_layer('conv4_1', ConvLayer(net.last_layer, 512, 3, pad=1, flip_filters=False))
    net.add_layer('conv4_2', ConvLayer(net.last_layer, 512, 3, pad=1, flip_filters=False))
    net.add_layer('conv4_3', ConvLayer(net.last_layer, 512, 3, pad=1, flip_filters=False))
    net.add_layer('pool4', PoolLayer(net.last_layer, 2))
    net.add_layer('conv5_1', ConvLayer(net.last_layer, 512, 3, pad=1, flip_filters=False))
    net.add_layer('conv5_2', ConvLayer(net.last_layer, 512, 3, pad=1, flip_filters=False))
    net.add_layer('conv5_3', ConvLayer(net.last_layer, 512, 3, pad=1, flip_filters=False))
    net.add_layer('pool5', PoolLayer(net.last_layer, 2))
    net.add_layer('fc6', DenseLayer(net.last_layer, num_units=4096))
    net.add_layer('fc6_dropout', DropoutLayer(net.last_layer, p=0.5))
    net.add_layer('fc7', DenseLayer(net.last_layer, num_units=4096))
    if num_classes is not None:
        net.add_layer('fc7_dropout', DropoutLayer(net.last_layer, p=0.5))
        net.add_layer('fc8', DenseLayer(net.last_layer, num_units=num_classes, nonlinearity=None))
        if add_softmax:
            net.add_layer('prob', NonlinearityLayer(net['fc8'], softmax))

    if not trainable:
        """ TODO I think most of the time, if we don't want to train the param
        we don't want to regularize it either. We will assume it's the case.
        """
        for layer in net.values():
            for param in layer.get_params():
                layer.params[param] = layer.params[param].difference(['trainable', 'regularizable'])

    return net
