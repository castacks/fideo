=====
fideo
=====


.. image:: https://img.shields.io/pypi/v/fideo.svg
        :target: https://pypi.python.org/pypi/fideo

.. image:: https://readthedocs.org/projects/fideo/badge/?version=latest
        :target: https://fideo.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status


Utilities for theano+lasagne

* Free software: BSD license
* Documentation: https://fideo.readthedocs.io.
