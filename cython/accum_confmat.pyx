
#from libc.stdint cimport uint32_t, uint64_t

def _accum_confmat(int[:, :] gt, int[:, :] pred, double[:, :] cm):
    """ gt, pred, cm
    gt, pred shapes are HxW (no channels)
    By definition a confusion matrix :math:`C` is such that :math:`C_{i, j}`
    is equal to the number of observations known to be in group :math:`i` but
    predicted to be in group :math:`j`.
    """
    cdef unsigned int i = 0
    cdef unsigned int j = 0
    cdef unsigned int gt_lbl = 0
    cdef unsigned int pred_lbl = 0
    cdef unsigned int I = gt.shape[0]
    cdef unsigned int J = gt.shape[1]
    assert(cm.shape[0]==cm.shape[1])
    assert(gt.shape[0]==pred.shape[0])
    assert(gt.shape[1]==pred.shape[1])
    for i in range(I):
        for j in range(J):
            gt_lbl = gt[i, j]
            pred_lbl = pred[i, j]
            cm[gt_lbl, pred_lbl] += 1



def _accum_confmat_batch(int[:, :, :] gt, int[:, :, :] pred, double[:, :] cm):
    """ gt, pred, cm
    gt, pred shapes are BxHxW (no channels)
    By definition a confusion matrix :math:`C` is such that :math:`C_{i, j}`
    is equal to the number of observations known to be in group :math:`i` but
    predicted to be in group :math:`j`.
    """
    cdef unsigned int i = 0
    cdef unsigned int j = 0
    cdef unsigned int gt_lbl = 0
    cdef unsigned int pred_lbl = 0
    cdef unsigned int I = gt.shape[1]
    cdef unsigned int J = gt.shape[2]
    cdef unsigned int B = gt.shape[0]
    assert(cm.shape[0] == cm.shape[1])
    assert(gt.shape[0] == pred.shape[0])
    assert(gt.shape[1] == pred.shape[1])
    assert(gt.shape[2] == pred.shape[2])

    for b in range(B):
        for i in range(I):
            for j in range(J):
                gt_lbl = gt[b, i, j]
                pred_lbl = pred[b, i, j]
                cm[gt_lbl, pred_lbl] += 1
