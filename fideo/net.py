import warnings
import logging
from collections import OrderedDict

import numpy as np
from funcy import first, last
import lasagne.layers


log = logging.getLogger(__name__)


class Net(object):
    """ A simple container for lasagne layers.
    It contains `layers` an OrderedDict of (name -> layer),
    and adds various helper methods.
    """

    def __init__(self):
        self._input_layer = None
        self._output_layer = None
        self.layers = OrderedDict()

    def add_layer(self, name, layer):
        """ Add unnamed layer, update layer to reflect given name.
        be careful when adding a named layer; existing name will
        be overwritten. Useful for [] syntactic sugar.
        """
        if name in self.layers:
            raise ValueError('trying to overwrite layer {}'.format(name))
        layer.name = name
        for param in layer.get_params():
            param.name = "%s.%s" % (layer.name, param.name)
        self.layers[layer.name] = layer

    def add_named_layer(self, layer):
        """ Assumes layer has name, and uses name as key """
        self.layers[layer.name] = layer

    def __getitem__(self, name):
        """ Get layer by name """
        return self.layers[name]

    def iteritems(self):
        """ Iterate over name, layer tuples """
        return self.layers.iteritems()

    def __setitem__(self, name, maybe_layer):
        """ Add layer with dict-style assignment.
        if maybe_layer is Layer, shorthand for add_layer(name, layer),
        if maybe_layer is a callable, it is called with 'self' and 'name'
        as parameters; the callable is free to do whatever, but in practice
        this is used for 'composite' layers such as batch normalized layers.
        """
        if callable(maybe_layer):
            maybe_layer(self, name)
        else:
            self.add_layer(name, maybe_layer)

    @property
    def last(self):
        """ Last layer added """
        return last(self.layers.itervalues())

    @property
    def input_layer(self):
        """ Get input layer, assuming it was first added,
        or explicitly set as `input_layer`.
        """
        if self._input_layer is not None:
            return self._input_layer
        if len(self.layers) == 0:
            return None
        return first(self.layers.itervalues())

    @input_layer.setter
    def input_layer(self, value):
        """ Set input_layer explicitly.
        """
        assert(value in self.layers.values())
        self._input_layer = value

    @property
    def output_layer(self):
        """ Get output layer, assuming it was last added,
        or explicitly set as `output_layer`.
        """
        if self._output_layer is not None:
            return self._output_layer
        if len(self.layers) == 0:
            return None
        return self.last

    @output_layer.setter
    def output_layer(self, value):
        """ Set output_layer explicitly.
        """
        assert(value in self.layers.values())
        self._output_layer = value

    def get_all_params(self, layer=None, **tags):
        """ Get parameters of all layers, filter by tags.
        """
        if layer is None:
            layer = self.output_layer
        if layer is None:
            return []
        return lasagne.layers.get_all_params(layer, **tags)

    def get_all_layers(self, layer=None):
        """ Get all layers.
        """
        if layer is None:
            layer = self.output_layer
        if layer is None:
            return []
        return lasagne.layers.get_all_layers(layer)

    def repr_net_shapes(self):
        """ Get all layer shapes as a printable string.
        """
        layer_shape = '\n'.join(
                ['{} : {}'.format(layer.name, layer.output_shape)
                    for layer in self.get_all_layers()])
        return layer_shape

    def repr_net_table(self, tablefmt='simple'):
        """ Get layer shape table as a printable string.
        """
        from . import dump_info
        return dump_info.dump_net_table(self, tablefmt=tablefmt)

    def save_net_diagram(self, fname):
        """ Use pydot to save network as diagram.
        """
        from . import draw_net
        if len(self) == 0:
            raise ValueError('no layers in net')
        all_layers = self.get_all_layers()
        draw_net.draw_to_file(all_layers,
                              fname,
                              verbose=True)

    def count_params(self, layer=None):
        """ Count network parameters.
        """
        if layer is None:
            layer = self.output_layer
        if layer is None:
            return 0
        return lasagne.layers.count_params(layer)

    def _sanity_check(self):
        params = self.get_all_params()
        names = [par.name for par in params]
        if len(names) != len(set(names)):
            raise ValueError('net needs unique param names')
        for name, layer in self.layers.items():
            # l_in and l_out are special; aliasing is allowed.
            if name != layer.name:
                raise ValueError('net layer names and keys should be the same')

    def set_weights(self, weights):
        """ weights has a dictionary interface.

        TODO: policy for excess/too few parameters.
        """

        self._sanity_check()

        params = self.get_all_params()
        if len(params) == 0:
            log.warn('set_weights called, but no parameters in model')
            return

        # note we ignore any other stuff that may be in weights
        for param in params:
            if param.name in weights:
                stored_shape = np.asarray(weights[param.name].shape)
                param_shape = np.asarray(param.get_value().shape)
                if not np.all(stored_shape == param_shape):
                    warn_msg = 'shape mismatch:'
                    warn_msg += '{} stored:{} new:{}'.format(param.name,
                                                             stored_shape,
                                                             param_shape)
                    warn_msg += ' skipping'
                    log.warn(warn_msg)
                    warnings.warn(warn_msg)
                else:
                    log.info('loading parameter {} from weights'.format(param.name))
                    param.set_value(weights[param.name])
            else:
                warn_msg = 'unable to load parameter {} from weights'.format(param.name)
                warnings.warn(warn_msg)
                log.warn(warn_msg)

    def get_weights(self):
        """ Get all network weights as dictionary.
        """
        params = self.get_all_params()
        param_dict = OrderedDict()
        for param in params:
            param_dict[param.name] = param.get_value(borrow=False)
        return param_dict
