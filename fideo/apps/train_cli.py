from __future__ import print_function

import argparse
import os
import logging

from spath import Path

from .. import dump_info
from .train import SupervisedTrainerApp

log = logging.getLogger(__name__)


class SupervisedTrainerAppCli(SupervisedTrainerApp):
    """ cli driver for SupervisedTrainerApp """

    def __init__(self, expt_fname):
        super(SupervisedTrainerAppCli, self).__init__(expt_fname)

    def update_cfg(self, args):
        """ configure runtime options, considering env variables and cli
        parameters.
        """
        # first set up from env
        self.env['data_base_dir'] = os.environ.get('DATA_BASE_DIR')
        self.env['results_base_dir'] = os.environ.get('RESULTS_BASE_DIR')
        self.env['models_base_dir'] = os.environ.get('MODELS_BASE_DIR')

        # cli may override env
        for cli_k, cli_v in args.iteritems():
            if cli_k == 'func':
                continue
            if cli_v is None and self.env.get(cli_k, None) is not None:
                # let the env value as-is
                pass
            else:
                # override
                self.env[cli_k] = cli_v

        # default is cwd for dirs, if not set yet
        for k in 'data_base_dir', 'results_base_dir', 'models_base_dir':
            if self.env.get(k, None) is None:
                self.env[k] = '.'
            # ensure type path
            self.env[k] = Path(self.env[k])

        self.env['results_dir'] = Path(self.env['results_base_dir'])/self.expt.exp_id

        # override expt settings
        if self.env['extra_options']:
            argstring = self.env['extra_options'].split(',')
            for arg in argstring:
                key, val = arg.split('=')
                key = key.strip()
                val = eval(val.strip())
                print('setting {} to {}'.format(key, val))
                setattr(self.expt, key, val)

    def cli_train(self, args):
        self.update_cfg(args)
        self.train()

    def cli_valid(self, args):
        self.update_cfg(args)
        self.validate()

    def cli_info(self, args):
        self.info()

    def cli_time(self, args):
        self.time(args['iters'], args['continuous'])

    def cli_clean(self, args):
        self.clean_results_dir()

    def cli(self):
        parser = argparse.ArgumentParser()
        subparsers = parser.add_subparsers()

        # train parser
        train_parser = subparsers.add_parser('train')

        train_parser.add_argument('--weights-fname',
                                  type=str,
                                  help='weights for initialization')

        train_parser.add_argument('--scratch-clean',
                                  action='store_true',
                                  help='train from scratch after deleting previous checkpoints')

        train_parser.add_argument('-r', '--resume',
                                  action='store_true',
                                  help='resume from checkpoint')

        train_parser.add_argument('--pretrain-from',
                                  type=str,
                                  help='resume from different model checkpt')

        train_parser.add_argument('-d', '--debug',
                                  action='store_true',
                                  help='activate debugging')

        train_parser.add_argument('--data-dir',
                                  default='/data',
                                  dest='data_base_dir',
                                  help='data base directory')

        train_parser.add_argument('--results-dir',
                                  default='/results',
                                  dest='results_base_dir',
                                  help='output base directory')

        train_parser.add_argument('--models-dir',
                                  default='/models',
                                  dest='models_base_dir',
                                  help='model base directory')

        train_parser.add_argument('-j', '--workers',
                                  type=int,
                                  default=1,
                                  help='number of workers')

        train_parser.add_argument('-q', '--qsize',
                                  type=int,
                                  default=128,
                                  help='queue size')

        train_parser.add_argument('--extra-options',
                                  type=str,
                                  default=None,
                                  help='override options in expt. dangerous!')

        train_parser.set_defaults(func=self.cli_train)

        # validate parser
        valid_parser = subparsers.add_parser('validate')
        valid_parser.add_argument('--weights-fname',
                                  type=str,
                                  help='weights for initialization')

        valid_parser.add_argument('-r', '--resume',
                                  action='store_true',
                                  help='resume from checkpoint')

        valid_parser.add_argument('--pretrain-from',
                                  type=str,
                                  help='resume from different model checkpt')

        valid_parser.add_argument('-d', '--debug',
                                  action='store_true',
                                  help='activate debugging')

        valid_parser.add_argument('--data-dir',
                                  default='/data',
                                  dest='data_base_dir',
                                  help='data base directory')

        valid_parser.add_argument('--results-dir',
                                  default='/results',
                                  dest='results_base_dir',
                                  help='output base directory')

        valid_parser.add_argument('--models-dir',
                                  default='/models',
                                  dest='models_base_dir',
                                  help='model base directory')

        valid_parser.add_argument('-j', '--workers',
                                  type=int,
                                  default=1,
                                  help='number of workers')

        valid_parser.add_argument('-q', '--qsize',
                                  type=int,
                                  default=128,
                                  help='queue size')

        valid_parser.add_argument('--extra-options',
                                  type=str,
                                  default=None,
                                  help='override options in expt. dangerous!')

        valid_parser.add_argument('--splits',
                                  type=str,
                                  default=None,
                                  help='json with splits')
        valid_parser.set_defaults(func=self.cli_valid)


        # info parser

        info_parser = subparsers.add_parser('info')
        info_parser.set_defaults(func=self.cli_info)

        # time parser

        time_parser = subparsers.add_parser('time')
        time_parser.add_argument('--iters', default=10, type=int)
        time_parser.add_argument('--continuous', action='store_true')
        time_parser.set_defaults(func=self.cli_time)

        args = parser.parse_args()
        args.func(vars(args))
