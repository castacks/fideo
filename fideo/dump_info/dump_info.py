from __future__ import print_function


__all___ = ['dump_expt_info',
            'dump_net_table']

import sys
import logging

import pickleshare
import tabulate
from spath import Path

log = logging.getLogger(__name__)


def dump_expt_info(app, console_only=False):
    """ Dump some info regarding env and config.
    Call after logger is initialized, and after theano_flags are set.
    Uses the pickleshare library.
    """

    import theano
    import lasagne

    if console_only:
        def cb(key, val, console):
            if not console:
                return
            print('{} = {}'.format(key, val))
            print()
    else:
        output_dir = Path(app.env['results_dir'])
        infodb_dirname = output_dir/'info.pkls'
        infodb = pickleshare.PickleShareDB(infodb_dirname)
        infodb.clear()  # TODO should I?

        def cb(key, val, console):
            if console:
                log.info('{} = {}'.format(key, val))
            infodb[key] = val

    cb('cmd', ' '.join(sys.argv), console=True)

    for k, v in app.env.iteritems():
        cb('env.{}'.format(k), v, console=True)

    cb('theano_version', theano.__version__, console=True)
    cb('lasagne_version', lasagne.__version__, console=True)

    app.model.dump_info(cb)

    if not console_only:
        # TODO whole source tree?
        app.expt_fname.copy(app.env['results_dir'])
    # pickle.dump(self.net, f, protocol=pickle.HIGHEST_PROTOCOL)


def dump_net_table(net, tablefmt='plain', add_inputs=True):
    """ Represent network in tabular form.
    Uses tabulate to return nicely string-formatted table.
    If tablefmt=None, return list-of-list representation,
    with first row being headers.
    """
    import lasagne
    layers = lasagne.layers.get_all_layers(net.output_layer)

    headers = ['name', 'type', 'units', 'sz', 'st', 'inshape', 'outshape']
    if add_inputs:
        headers.append('inputs')

    rows = []
    for layer in layers:
        name = layer.name
        typename = layer.__class__.__name__
        row = [name, typename]

        if hasattr(layer, 'num_filters'):
            row.append(layer.num_filters)
        elif hasattr(layer, 'num_units'):
            row.append(layer.num_units)
        else:
            row.append('')

        if hasattr(layer, 'filter_size'):
            row.append(layer.filter_size[0])
        elif hasattr(layer, 'pool_size'):
            row.append(layer.pool_size[0])
        else:
            row.append('')

        if hasattr(layer, 'stride'):
            row.append(layer.stride[0])
        elif hasattr(layer, 'strides'):
            row.append(layer.strudes[0])
        else:
            row.append('')

        if hasattr(layer, 'input_shape'):
            row.append('x'.join(map(str, layer.input_shape[1:])))
        else:
            row.append('')

        row.append('x'.join(map(str, layer.output_shape[1:])))

        if add_inputs:
            if hasattr(layer, 'input_layer'):
                row.append(layer.input_layer.name)
            elif hasattr(layer, 'input_layers'):
                txt = ', '.join([l.name for l in layer.input_layers])
                row.append(txt)
            else:
                row.append('')

        rows.append(row)

    if tablefmt is None:
        return headers + rows

    out = tabulate.tabulate(rows, headers=headers, tablefmt=tablefmt)
    return out
