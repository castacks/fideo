"""Top-level package for fideo."""

__author__ = """Daniel Maturana"""
__email__ = 'dimatura@cmu.edu'
__version__ = '0.1.0'

import activations
import apps
import checkpoints
import dump_info
import expr
import expr2d
import expr3d
import init
import layer_mod
import layers
import models
import monitoring
import net
import net_zoo
import trainers
import updaters
import util
import validators
