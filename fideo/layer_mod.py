"""
Functions that modify layers, e.g. a ConvLayer is internally Conv + Nonlin.
The BatchNorm() wrapper replaces it with three stages: Conv + BN + Nonlin.
"""

__all__ = ['BatchNorm',
           'BatchNormPN',
           'Dropout',
           'DropoutChannels',
           'GaussianNoise',
           ]


from lasagne import nonlinearities
from lasagne.layers import BatchNormLayer
from lasagne.layers import NonlinearityLayer
from lasagne.layers import GaussianNoiseLayer
from lasagne.layers import DropoutLayer

from .layers import IdentityLayer


class BatchNorm(object):
    """
    let layer = (dot -> nonlin)
    we transform
    input -> dot -> nonlin -> output
    to
    input -> dot -> batchnorm -> nonlin -> output

    Names will be changed. We add postfixes +bn, +dot, +nl.

    kwargs go to BatchNormLayer.
    """

    def __init__(self, layer, **kwargs):
        self.layer = layer
        self.kwargs = kwargs

    def __call__(self, net, name):

        nonlinearity = getattr(self.layer, 'nonlinearity', None)
        # remove nonlinearity
        if nonlinearity is not None:
            self.layer.nonlinearity = nonlinearities.identity

        # get rid of bias - it's in batch norm now
        if hasattr(self.layer, 'b') and self.layer.b is not None:
            del self.layer.params[self.layer.b]
            self.layer.b = None

        # original (renamed) layer, 'dot' is dense or conv
        net.add_layer(name+'+dot', self.layer)

        # batch norm layer
        net.add_named_layer(BatchNormLayer(net.last, name=name+'+bn',
                            **self.kwargs))

        # nonlin layer
        if nonlinearity is not None:
            net.add_named_layer(NonlinearityLayer(net.last, nonlinearity,
                                name=name+'+nl'))

        # dummy layer that preserves original name for interpretability.
        net.add_layer(name, IdentityLayer(net.last))


class BatchNormPN(object):
    """
    some people say it's better to add the batch norm
    *after* the nonlinearity.

    let layer = (dot -> nonlin)
    we transform
    input -> dot -> nonlin -> output
    to
    input -> dot -> nonlin -> batchnorm -> output

    Names will be changed. We add postfixes +bn, +dot, +nl.
    """

    def __init__(self, layer, **kwargs):
        self.layer = layer
        self.kwargs = kwargs

    def __call__(self, net, name):
        # original (renamed) layer, 'dot' is dense or conv
        net.add_layer(name+'+dotnl', self.layer)
        # batch norm layer
        net.add_named_layer(BatchNormLayer(net.last, name=name+'+bn',
                            **self.kwargs))
        # dummy layer that preserves original name for interpretability.
        net.add_layer(name, IdentityLayer(net.last))


class Dropout(object):
    """ Adds layer followed by dropout with postfix +drop
    """
    def __init__(self, layer, **kwargs):
        self.layer = layer
        self.kwargs = kwargs

    def __call__(self, net, name):
        net.add_layer(name+'+predrop', self.layer)
        net.add_layer(name+'+drop', DropoutLayer(net.last, **self.kwargs))
        net.add_layer(name, IdentityLayer(net.last))


class DropoutChannels(object):
    """ Adds layer followed by spatial/channelwise dropout with postfix +dropc
    """

    def __init__(self, layer, **kwargs):
        self.layer = layer
        self.kwargs = kwargs

    def __call__(self, net, name):
        net.add_layer(name+'+predrop', self.layer)
        ndim = len(getattr(self.layer, 'output_shape', self.layer))
        self.kwargs['shared_axes'] = tuple(range(2, ndim))
        net.add_layer(name+'+drop', DropoutLayer(net.last, **self.kwargs))
        net.add_layer(name, IdentityLayer(net.last))


class GaussianNoise(object):
    """ Adds layer followed by gaussian noise with postfix +gn
    """

    def __init__(self, layer, **kwargs):
        self.layer = layer
        self.kwargs = kwargs

    def __call__(self, net, name):
        net.add_layer(name+'+pregn', self.layer)
        net.add_layer(name+'+gn', GaussianNoiseLayer(self.layer,
                                                     **self.kwargs))
        net.add_layer(name, IdentityLayer(self.layer))
