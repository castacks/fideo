#
# @author  Daniel Maturana
# @year    2015
#
# @attention Copyright (c) 2015
# @attention Carnegie Mellon University
# @attention All rights reserved.
#
# @=

"""
Layers for spatially 3d/ volumetric data - deprecated.
"""

__all__ = [
    'RotPool3dLayer',
    'RotPoolLayer',
]


import theano.tensor as T
from lasagne.layers import Layer


class RotPoolLayer(Layer):
    """
    Pool over num_degs rotations.
    """
    def __init__(self, input_layer, num_degs, method='max', **kwargs):
        """
        """
        super(RotPoolLayer, self).__init__(input_layer, **kwargs)
        self.method = method
        self.num_degs = num_degs

    # rely on defaults for get_params
    def get_output_shape_for(self, input_shape):
        assert(len(input_shape) == 2)
        if any(s is None for s in input_shape):
            return input_shape
        batch_size = input_shape[0]
        # TODO if is none?
        return (input_shape[0]//self.num_degs, input_shape[1])

    def get_output_for(self, input, *args, **kwargs):

        input_shape = self.input_shape
        if any(s is None for s in input_shape):
            input_shape = input.shape
            print('input shape none')
        print(input_shape)

        batch_size2 = input_shape[0]//self.num_degs
        num_features = input_shape[1]

        out_pool_shape = (batch_size2, self.num_degs, num_features)
        if self.method == 'max':
            out_pooled = T.max(input.reshape(out_pool_shape), 1)
        elif self.method == 'avg':
            out_pooled = T.mean(input.reshape(out_pool_shape), 1)
        else:
            raise Exception('unknown pooling type')

        return out_pooled


class RotPool3dLayer(Layer):
    def __init__(self, input_layer, num_degs, method='max', **kwargs):
        super(RotPool3dLayer, self).__init__(input_layer, **kwargs)
        self.method = method
        self.num_degs = num_degs

    def get_output_shape_for(self, input_shape):
        assert(len(input_shape) == 2)
        if any(s is None for s in input_shape):
            return input_shape
        batch_size = input_shape[0]
        # TODO if is none?
        return (input_shape[0]//self.num_degs,) + tuple(input_shape[1:])

    def get_output_for(self, input, *args, **kwargs):

        input_shape = self.input_shape
        if any(s is None for s in input_shape):
            input_shape = input.shape

        batch_size2 = input_shape[0]//self.num_degs

        out_pool_shape = (batch_size2, self.num_degs,) + input_shape[1:]
        if self.method == 'max':
            out_pooled = T.max(input.reshape(out_pool_shape), 1)
        elif self.method == 'avg':
            out_pooled = T.mean(input.reshape(out_pool_shape), 1)
        else:
            raise Exception('unknown pooling type')

        return out_pooled
