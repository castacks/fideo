#
# @author  Daniel Maturana
# @year    2015
#
# @attention Copyright (c) 2015
# @attention Carnegie Mellon University
# @attention All rights reserved.
#
# @=


__all__ = [
    'ReshapeLayer',
    'IdentityLayer',
    'SplitLayer',
    'IdentityLayer',
]


import theano.tensor as T
import lasagne


class ReshapeLayer(lasagne.layers.Layer):
    """ lasagne has too.
    """
    def __init__(self, input_layer, output_shape, **kwargs):
        super(ReshapeLayer, self).__init__(input_layer, **kwargs)
        # not including batch
        self.output_shape = output_shape
        self.ndim = len(output_shape)

    def get_output_shape_for(self, input_shape):
        return (input_shape[0],)+self.output_shape

    def get_output_for(self, input, *args, **kwargs):
        shape_with_batch = (input.shape[0],)+self.output_shape
        return T.reshape(input, shape_with_batch, ndim=(self.ndim+1))


class IdentityLayer(lasagne.layers.Layer):
    """ A no-op layer that simply passes along the input.
    """
    def __init__(self, incoming, **kwargs):
        super(IdentityLayer, self).__init__(incoming, **kwargs)

    def get_output_for(self, input, **kwargs):
        return input


class SplitLayer(lasagne.layers.Layer):
    """ select k features, starting at ix, out of all features.
    useful to split features into subsets.
    """
    def __init__(self, input_layer, ix, k=1, **kwargs):
        super(SplitLayer, self).__init__(input_layer, **kwargs)
        self.ix = ix
        self.k = k

    def get_output_shape_for(self, input_shape):
        return (input_shape[0], self.k,) + input_shape[2:]

    def get_output_for(self, input, *args, **kwargs):
        return input[:, self.ix:(self.ix+self.k)]
