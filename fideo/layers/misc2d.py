""" Various lasagne layers for 2D feature maps.
"""

__all__ = [
        'RepeatUpsample2dLayer',
        'SparseUpsample2dLayer',
        'Softmax2dLayer',
        'Crop2dLayer',
        'TiedDropoutLayer',
        'NormalizeScale2dLayer',
        ]


import lasagne
import theano
import theano.tensor as T

floatX = theano.config.floatX

from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams


class NormalizeScale2dLayer(lasagne.layers.Layer):
    """ Normalize and scale, inspired by parsenet.
    """
    def __init__(self, incoming, scales=lasagne.init.Constant(1.), **kwargs):
        super(NormalizeScale2dLayer, self).__init__(incoming, **kwargs)

        self.shared_axes = (0, 2, 3)
        shape = [self.input_shape[1]]
        self.scales = self.add_param(scales, shape, 'scales', regularizable=False)

    def get_output_for(self, input, *args, **kwargs):
        input_sqr = input**2.
        input_norm_sqr = input_sqr.sum(1, keepdims=True)
        input_norm = T.sqrt(input_norm_sqr + 1e-12)
        input_unit = input/input_norm

        axes = iter(range(self.scales.ndim))
        # pattern = ['x' if input_axis in self.shared_axes
        #           else next(axes) for input_axis in range(input.ndim)]

        return input_unit * self.scales.dimshuffle('x', 0, 'x', 'x')


class RepeatUpsample2dLayer(lasagne.layers.Layer):
    """ This layer performs unpooling over the last two dimensions
    of a 4D tensor. Uses theano repeat.
    """

    def __init__(self, incoming, factor=2, **kwargs):
        super(RepeatUpsample2dLayer, self).__init__(incoming, **kwargs)
        self.factor = factor

    def get_output_shape_for(self, input_shape):
        output_shape = list(input_shape)
        output_shape[2] = input_shape[2] * self.factor
        output_shape[3] = input_shape[3] * self.factor
        return tuple(output_shape)

    def get_output_for(self, input, **kwargs):
        return input.repeat(self.factor, axis=2).repeat(self.factor, axis=3)


class SparseUpsample2dLayer(lasagne.layers.Layer):
    """ Only fills in upper left pixels. Rest of them are zeros.
    """

    def __init__(self, input_layer, factor=2, pad=0, **kwargs):
        """ times specifies powers of two, ie factor = 2**times.
        factor is assumed to be int.
        """
        super(SparseUpsample2dLayer, self).__init__(input_layer, **kwargs)
        self.factor = factor
        self.pad = pad

    def get_output_shape_for(self, input_shape):
        return (input_shape[:2] +
                tuple(d*(self.factor)+self.pad for d in input_shape[2:]))

    def get_output_for(self, input, *args, **kwargs):
        factor = self.factor
        out = T.zeros((input.shape[0], input.shape[1],
                      factor*input.shape[2]+self.pad,
                      factor*input.shape[3]+self.pad),
                      dtype=floatX)
        out = T.set_subtensor(out[:, :, 0::factor, 0::factor], input)
        return out


class Softmax2dLayer(lasagne.layers.Layer):
    """ Applies softmax transform along the channel axis for each pixel in an
    image.
    """

    def __init__(self, input_layer):
        super(Softmax2dLayer, self).__init__(input_layer)

    def get_output_shape_for(self, input_shape):
        # b c remain same. note c is the number of classes.
        return input_shape[0:2] + tuple(d for d in input_shape[2:])

    def get_output_for(self, input, *args, **kwargs):
        pixelwise_max = input.max(axis=1, keepdims=True)
        pixelwise_exp = theano.tensor.exp(input-pixelwise_max)
        pixelwise_sum = pixelwise_exp.sum(axis=1, keepdims=True)
        out = pixelwise_exp/(pixelwise_sum)
        return out


class Crop2dLayer(lasagne.layers.Layer):
    """ Crop spatial borders in 2D.
    """

    def __init__(self, input_layer, output_shape, **kwargs):
        super(Crop2dLayer, self).__init__(input_layer, **kwargs)
        # not including batch
        self._output_shape = output_shape

    def get_output_shape_for(self, input_shape):
        for i in (0, 1):
            if input_shape[2+i] < self._output_shape[i]:
                raise ValueError('input shape {} is smaller ({}) than output shape ({})'
                        .format(i, input_shape[i], self._output_shape[i]))
        return input_shape[0:2]+self._output_shape

    def get_output_for(self, input, *args, **kwargs):
        row0 = (input.shape[2]-self._output_shape[0])//2
        col0 = (input.shape[3]-self._output_shape[1])//2
        row1 = row0+self._output_shape[0]
        col1 = col0+self._output_shape[1]
        return input[:, :, row0:row1, col0:col1]


class TiedDropoutLayer(lasagne.layers.Layer):
    """
    from kaggle-ndsb
    Dropout layer that broadcasts the mask across all axes beyond the first
    two.
    NOTE: lasagne now supports this by tying together axes in dropout.
    """
    def __init__(self, input_layer, p=0.5, rescale=True, **kwargs):
        super(TiedDropoutLayer, self).__init__(input_layer, **kwargs)
        self.p = p
        self.rescale = rescale
        self._srng = RandomStreams()

    def get_output_for(self, input, deterministic=False, *args, **kwargs):
        if deterministic or self.p == 0:
            return input
        else:
            retain_prob = 1 - self.p
            if self.rescale:
                input /= retain_prob
            mask = self._srng.binomial(input.shape[:2], p=retain_prob,
                                       dtype=floatX)
            axes = [0, 1] + (['x'] * (input.ndim - 2))
            mask = mask.dimshuffle(*axes)
            return input * mask
