"""
Weight initialization functions, following Lasagne API.
"""

import numpy as np

import lasagne
from lasagne.utils import floatX


class Prelu(lasagne.init.Initializer):
    """ He's "prelu" initialization.
    Note: HeInit is now in Lasagne.
    """

    def sample(self, shape):
        # eg k^2 for conv2d
        receptive_field_size = np.prod(shape[2:])
        c = shape[1]  # input channels
        nl = c*receptive_field_size
        std = np.sqrt(2.0/(nl))
        return floatX(np.random.normal(0, std, size=shape))


class Bilinear(lasagne.init.Initializer):
    """ Initialize with weights for bilinear sampling.
    Useful for transposed convolutions.

    :usage:
        >>> init = Bilinear()
        >>> init.sample((1, 1, 3, 3))[0, 0]
        array([[ 0.25,  0.5 ,  0.25],
               [ 0.5 ,  1.  ,  0.5 ],
               [ 0.25,  0.5 ,  0.25]], dtype=float32)
    """

    def __init__(self, normalize=False):
        self.normalize = normalize

    def sample(self, shape):
        """ below adapted from FCN caffe code """
        # shape: output, input, height, width
        # inverse of eqn
        # kernel size =  2*factor - factor%2
        k = shape[2]
        factor = float((k + 1) // 2)
        if shape[2] % 2 == 1:
            center = factor - 1.0
        else:
            center = factor - 0.5
        og = np.ogrid[:shape[2], :shape[3]]
        kernel2d = (1 - abs(og[0] - center) / factor) * \
                   (1 - abs(og[1] - center) / factor)
        kernel2d = kernel2d.astype(np.float32)
        if self.normalize:
            # TODO why 4? (something/2)**2 ?
            kernel2d /= (kernel2d.sum()/4.)
        kernel4d = np.zeros(shape, dtype=np.float32)
        # TODO what if input > output?
        for n in xrange(shape[0]):
            if n < shape[1]:
                kernel4d[n, n] = kernel2d
        return kernel4d


class ConvIdentity(lasagne.init.Initializer):
    """ Initialize conv with identity function.

    :usage:
        >>> init = ConvIdentity()
        >>> init.sample((3, 3))
        array([[ 1.,  0.,  0.],
               [ 0.,  1.,  0.],
               [ 0.,  0.,  1.]], dtype=float32)
    """
    def sample(self, shape):
        kernel4d = np.zeros(shape, 'f4')
        center = tuple([i//2 for i in kernel4d.shape[2:]])
        for n in xrange(shape[0]):
            # TODO what to do if output != input
            if n < shape[1]:
                kernel4d[(n, n) + center] = 1.
        return kernel4d


class NINIdentity(lasagne.init.Initializer):
    """ Initialize NIN with identity function.
    TODO should be good for Dense too?
    """
    def sample(self, shape):
        w = np.zeros(shape, 'f4')
        for n in xrange(shape[1]):
            # TODO what to do if output != input
            if n < shape[0]:
                w[n, n] = 1.
        return w


if __name__ == "__main__":
    kernel = Bilinear()
    print(kernel.sample((1, 1, 3, 3)))
    # print(kernel.sample((1, 1, 4, 4)))
    # print(kernel.sample((1, 1, 5, 5)))

    # kerneln = Bilinear(normalize=True)
    # print(kerneln.sample((1, 1, 3, 3)))
    # print(kerneln.sample((1, 1, 4, 4)))
    # print(kerneln.sample((1, 1, 5, 5)))

    # kernel = ConvIdentity()
    # print(kernel.sample((4, 3, 3, 3)))
    # print(kernel.sample((4, 4, 4, 4)))

    #print(NINIdentity().sample((4, 3)))
    # print(ConvIdentity().sample((3, 3)))

    import doctest
    doctest.testmod(verbose=True)
