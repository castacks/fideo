
__all__ = ['MonitorCb']


class MonitorCb(object):

    def __init__(self):
        pass

    def init(self, app):
        pass

    def start(self, app, state):
        pass

    def chunk(self, app, state, chunk, out):
        pass

    def end(self, app, state):
        pass
