from __future__ import print_function

__all__ = ['SemSegMetricsCb']


import cPickle as pickle

import numpy as np
import tabulate

from spath import Path

from ..util.confmat import compute_confmat, compute_confmat2
from ..util.confmat import metrics_from_confmat
from .cb import MonitorCb


class SemSegMetricsCb(MonitorCb):

    def __init__(self,
                 fname,
                 x_key,
                 y_key,
                 yhat_key,
                 num_classes,
                 masked=False):
        self.fname = fname
        self.x_key = x_key
        self.y_key = y_key
        self.yhat_key = yhat_key
        self.num_classes = num_classes
        # for binary problems we sometimes use num_classes=1
        self.binary = (num_classes == 1)
        self.masked = masked

    def start(self, app, state):
        if self.masked:
            self.confmat = np.zeros((self.num_classes+1,
                                     self.num_classes+1), 'f8')
        elif self.binary:
            self.confmat = np.zeros((2, 2), 'f8')
        else:
            self.confmat = np.zeros((self.num_classes,
                                     self.num_classes), 'f8')

    def chunk(self, app, state, chunk, out):
        # x = chunk[self.x_key]

        y = chunk[self.y_key].astype('i4').squeeze(1)
        yhat = out[self.yhat_key]
        print(yhat.shape, yhat.dtype)
        print(y.shape, y.dtype)

        # yhat_softmax = yhat/np.sum(np.exp(yhat), 1)

        if self.binary:
            yhat_argmax = (yhat > 0.5).astype('i4').squeeze(1)
        else:
            yhat_argmax = np.argmax(yhat, 1).astype('i4')
            if self.masked:
                yhat_argmax += 1

        if self.binary:
            if self.masked:
                raise NotImplementedError()
            else:
                self.confmat += compute_confmat(y, yhat_argmax, 2)
        else:
            if self.masked:
                self.confmat += compute_confmat(y, yhat_argmax, self.num_classes+1)
            else:
                cc1 = compute_confmat(y, yhat_argmax, self.num_classes)
                cc2 = compute_confmat2(y, yhat_argmax, self.num_classes)
                assert(np.all(cc1 == cc2))
                self.confmat += cc1

    def end(self, app, state):
        entries = metrics_from_confmat(self.confmat, self.masked)
        print(tabulate.tabulate(entries, headers='keys'))

        #import pandas as pd
        #if len(entries) > 2:
        #    df = pd.DataFrame(entries[1])
        #    miou = df['iou'].mean()
        #    totalsupp = df['support'].sum()
        #    fwiou = ((df['iou']*df['support'])/float(totalsupp)).sum()

        latest_fname = Path(app.env['results_dir'])/(Path(self.fname).stripext()+'_latest.txt')
        with open(latest_fname, 'w') as f:
            f.write('itr_ctr = %d\n'%app.state['itr_ctr'])
            f.write('split: %s\n'%app.expt.SPLITS)
            f.write('\n\n')
            f.write(tabulate.tabulate(entries, headers='keys'))
            f.write('\n\n')
            f.write(tabulate.tabulate(entries, headers='keys', tablefmt='latex_booktabs'))

        # latest_fname_bin = app.env['results_dir']/(self.fname.stripext()+'_latest.pkl')
        # db = pickleshare.PickleShareDB(app.env['results_dir']/self.fname)
        record = {}
        record['metrics'] = entries
        record['cm'] = self.confmat.copy()

        out_fname = app.env['results_dir']/(self.fname)
        out_fname.write_pickle(record, mode='ab', protocol=pickle.HIGHEST_PROTOCOL)
