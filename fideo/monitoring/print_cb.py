from __future__ import print_function

import logging
import numpy as np

from .cb import MonitorCb

__all__ = ['PrintCb']


class PrintCb(MonitorCb):

    def __init__(self, name):
        self.name = name
        self.log = logging.getLogger(__name__ + '.' + name)

    def chunk(self, app, state, chunk, out):
        means = {}
        for name in out.dtype.names:
            means[name] = np.mean(out[name])
        s = ', '.join(['%s: %.4f' % (k, m) for k, m in means.iteritems()])
        self.log.info('itr %d| %s', state['itr_ctr'], s)
