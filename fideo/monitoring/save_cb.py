from __future__ import print_function


__all__ = ['SaveImageLabelCb',
           'SaveBinaryImageLabelCb',
           'build_save_image_cb']


import logging
from collections import OrderedDict

import numpy as np
from PIL import Image

from spath import Path

from ..util import remapping
from ..util.distinct_colors import distinct_colors_int
from ..util import viz
from .cb import MonitorCb


log = logging.getLogger(__name__)


def build_save_image_cb(name,
                        x_key,
                        y_key,
                        num_classes,
                        masked,
                        palette,
                        every_nth,
                        max_files,
                        ext):
    if num_classes==1:
        return SaveBinaryImageLabelCb(name,
                                      x_key,
                                      y_key,
                                      every_nth,
                                      max_files,
                                      ext)
    return SaveImageLabelCb(name,
                            x_key,
                            y_key,
                            num_classes,
                            masked,
                            palette,
                            every_nth,
                            max_files,
                            ext)



def _cleanup(out_dir, max_files):
    files = sorted(out_dir.files('*.png') + out_dir.files('*.jpg'), key=lambda x: x.mtime)
    if len(files) > max_files:
        # delete oldest
        to_delete = files[:len(files)-max_files]
        for fname in to_delete:
            fname.remove()


class SaveImageLabelCb(MonitorCb):

    def __init__(self,
                 name,
                 x_key=None,
                 y_key=None,
                 num_classes=None,
                 masked=True,
                 palette=None,
                 every_nth=1,
                 max_files=40,
                 ext='jpg'):

        self.name = name
        self.x_key = x_key
        self.y_key = y_key
        # reasonable upper bound?
        self.num_classes = (num_classes or 256)
        self.masked = masked
        self.palette = palette
        self.every_nth = every_nth
        self.max_files = max_files
        self.ext = ext
        assert(self.ext in ('jpg', 'png'))

    def init(self, app):
        # initialize palette

        if app.mode == 'valid':
            self.ext = 'png'

        if self.palette is None:
            self.palette = OrderedDict()
            # add 1 for in case 0 is invalid class.
            for ix in xrange(self.num_classes+1):
                self.palette[ix] = distinct_colors_int[ix]
        elif (isinstance(self.palette, str) or
              isinstance(self.palette, Path)):
            log.info('loading palette: %s', self.palette)
            data_dir = Path(app.env['data_base_dir'])
            self.palette = (data_dir/self.palette).read_json()
            self.palette = {int(k): v for k, v in self.palette.iteritems()}

        # initialize directory
        log.info('initializing save image directory %s', self.name)
        self.out_dir = Path(app.env['results_dir'])/self.name
        self.out_dir.makedirs_p()
        if app.mode == 'train':
            _cleanup(self.out_dir, self.max_files)

    def start(self, app, state):
        self.ctr = 0

    def chunk(self, app, state, chunk, out):

        if (self.ctr % self.every_nth) != 0:
            self.ctr += 1
            return

        x_data = chunk[self.x_key]
        y_data = chunk[self.y_key]

        # sample an image
        r_ix = np.random.randint(len(x_data))
        y = y_data[r_ix, 0].astype('int32')
        x = x_data[r_ix:r_ix+1]
        yhat = app.model.compiled_dout(x)[0]

        # yhat = fideo.util.softmax(yhat)
        yhat_argmax = np.argmax(yhat, 0)
        if self.masked:
            yhat_argmax += 1  # network has no notion of 0

        xviz = viz.unpreprocess(x_data[r_ix])

        # colorize images
        yviz = remapping.label_image_to_rgb_image(y, self.palette)
        yhat_viz = remapping.label_image_to_rgb_image(yhat_argmax, self.palette)

        xyviz = viz.make_image_label_montage(xviz, yviz, yhat_viz)

        # pydisp.image(xyviz, win=self.name, title=self.name)
        out_fname = self.out_dir/('out_%09d_%05d.%s' %
                                  (state['itr_ctr'],
                                   self.ctr,
                                   self.ext))
        xyviz.save(str(out_fname))
        if app.mode == 'train':
            _cleanup(self.out_dir, self.max_files)
        self.ctr += 1


class SaveBinaryImageLabelCb(MonitorCb):

    def __init__(self,
                 name,
                 x_key=None,
                 y_key=None,
                 every_nth=1,
                 max_files=40,
                 ext='jpg'):

        self.name = name
        self.x_key = x_key
        self.y_key = y_key
        self.every_nth = every_nth
        self.max_files = max_files
        self.ext = ext
        assert(self.ext in ('jpg', 'png'))

    def init(self, app):
        log.info('initializing save image directory %s', self.name)
        self.out_dir = Path(app.env['results_dir'])/self.name
        self.out_dir.makedirs_p()
        if app.mode == 'train':
            _cleanup(self.out_dir, self.max_files)

    def start(self, app, state):
        self.ctr = 0

    def chunk(self, app, state, chunk, out):
        if (self.ctr % self.every_nth) != 0:
            self.ctr += 1
            return

        x_data = chunk[self.x_key]
        y_data = chunk[self.y_key]

        channels = y_data.shape[1]

        # sample an image from batch
        r_ix = np.random.randint(len(x_data))
        x = x_data[r_ix:r_ix+1]

        # classify and squeeze out batch dim
        all_yhat = app.model.compiled_dout(x).squeeze(0)

        # only need this during train
        if app.mode == 'train':
            # TODO more stable sigmoid
            all_yhat = 1./(1 + np.exp(-all_yhat))

        # sometimes we have multiple independent channels
        for channel in range(channels):
            y = y_data[r_ix, channel].astype('int32')
            yhat = all_yhat[channel]

            xviz = viz.unpreprocess(x_data[r_ix])
            # TODO other colormaps
            yviz = (y*255).clip(0, 255).astype('u1')
            yhat_viz = (yhat*255).clip(0, 255).astype('u1')

            # three-channel
            yviz = np.dstack((yviz,)*3)
            yhat_viz = np.dstack((yhat_viz,)*3)

            xyviz = viz.make_image_label_montage(xviz, yviz, yhat_viz)

            out_fname = self.out_dir/('out_%09d_%05d_%02d.%s' %
                                      (state['itr_ctr'],
                                       self.ctr,
                                       channel,
                                       self.ext))
            xyviz.save(str(out_fname))
        if app.mode == 'train':
            _cleanup(self.out_dir, self.max_files)

        self.ctr += 1
