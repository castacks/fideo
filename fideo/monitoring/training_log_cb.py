
__all__ = ['TrainingLogCb']

from collections import OrderedDict

import numpy as np

from .cb import MonitorCb
from ..util.training_log import TrainingLogger


class TrainingLogCb(MonitorCb):

    def __init__(self, fname, reinitialize=False):
        self.tlogger = TrainingLogger(fname, reinitialize)

    def chunk(self, app, state, chunk, out):
        record = OrderedDict()
        record['itr_ctr'] = state['itr_ctr']
        for name in out.dtype.names:
            record[name] = np.mean(out[name])
        self.tlogger.log(record)
