"""
Some Theano expressions for spatially 3D / volumetric data.
"""

import theano.tensor as T


def logloss_3d(pre_softmax_output, y_batch):
    """
    input is (b,c,h,w). b*w*h samples, and a K unnormalized pred for each
    assuming y_batch (ground truth) is a label image with indexes in each pixel
    """
    B, K, T_, H, W = pre_softmax_output.shape
    out_reshaped = T.reshape(pre_softmax_output.dimshuffle(0, 2, 3, 4, 1),
                             (B * T_ * H * W, K),
                             ndim=2)
    softmax_out = T.nnet.softmax(out_reshaped)
    y_batch_reshaped = y_batch.flatten()
    loss = T.cast(T.mean(T.nnet.categorical_crossentropy(
        softmax_out, y_batch_reshaped)), 'float32')
    return loss


def masked_logloss_3d(pre_softmax_output, y_batch):
    """
    input is (b,c,h,w). b*w*h samples, and a K unnormalized pred for each
    assuming y_batch (ground truth) is a label image with indexes in each pixel
    """
    y_batch_reshaped = y_batch.flatten()
    nz_ix = y_batch_reshaped.nonzero()
    y_batch_reshaped = y_batch_reshaped[nz_ix] - 1

    B, K, T_, H, W = pre_softmax_output.shape
    out_reshaped = T.reshape(pre_softmax_output.dimshuffle(0, 2, 3, 4, 1),
                             (B * T_ * H * W, K),
                             ndim=2)
    out_reshaped = out_reshaped[nz_ix]
    softmax_out = T.nnet.softmax(out_reshaped)
    loss = T.cast(T.mean(T.nnet.categorical_crossentropy(
        softmax_out, y_batch_reshaped)), 'float32')
    return loss


def masked_binary_cross_entropy_3d(pre_sigmoid_output, y_batch):
    """
    input is (b,c,h,w). b*w*h samples, and a K unnormalized pred for each
    assuming y_batch (ground truth) is a label image with indexes in each pixel
    """
    B, K, T_, H, W = pre_sigmoid_output.shape
    y_batch_reshaped = T.reshape(y_batch.dimshuffle(0, 2, 3, 4, 1),
                                 (B * T_ * H * W, K),
                                 ndim=2)

    # TODO how to calc mask
    nz_ix = y_batch_reshaped.sum(1).nonzero()
    y_batch_reshaped = y_batch_reshaped[nz_ix]

    out_reshaped = T.reshape(pre_sigmoid_output.dimshuffle(0, 2, 3, 4, 1),
                             (B * T_ * H * W, K),
                             ndim=2)
    out_reshaped = out_reshaped[nz_ix]

    sigmoid_out = T.nnet.sigmoid(out_reshaped)
    loss = T.cast(T.mean(
        T.nnet.binary_cross_entropy(sigmoid_out, y_batch_reshaped)), 'float32')
    return loss


def masked_binary_cross_entropy_3d_fast(pre_sigmoid_output, y_batch):
    """
    uses one hot encoding.
    differs by constant of factor of K (num labels) with other theano
    """
    nz = T.cast((y_batch.sum(1) > 0), 'float32')
    nnz = nz.sum()  # *y_batch.shape[1]
    sigmoid_out = T.nnet.sigmoid(pre_sigmoid_output)
    # masked = nz.dimshuffle([0,'x',1,2,3])*sigmoid_out
    loss = T.cast(
        T.sum(nz.dimshuffle([0, 'x', 1, 2, 3]) * T.nnet.binary_crossentropy(
            sigmoid_out, y_batch)) / nnz, 'float32')
    return loss


def masked_error_rate_3d(output, y_batch):
    """
    TODO case when mask != 0
    """
    # because the theano magic for numerical stability
    # is not working after this reshape.
    y_batch_reshaped = y_batch.flatten()
    nz_ix = y_batch_reshaped.nonzero()
    y_batch_reshaped = y_batch_reshaped[nz_ix] - 1
    # input is (b,c,t,h,w). b*w*t*h samples, and a K unnormalized for each
    # labels are in 0-K range inclusive but 0 is invalid/missing.
    B, K, T_, H, W = output.shape
    out_reshaped = T.reshape(output.dimshuffle(0, 2, 3, 4, 1),
                             (B * T_ * H * W, K),
                             ndim=2)
    out_reshaped = out_reshaped[nz_ix]
    # pred = T.argmax( out_reshaped, axis=1, keepdims=True )
    pred = T.argmax(out_reshaped, axis=1)
    err = T.cast(T.mean(T.neq(pred, y_batch_reshaped)), 'float32')
    return err


def masked_multilabel_error_rate_3d(pre_sigmoid_output, y_batch):
    nz = T.cast((y_batch.sum(1) > 0), 'float32')
    nnz = nz.sum() * y_batch.shape[1]
    # note pre-sigmoid the threshold is 0
    errors = T.cast(T.neq((pre_sigmoid_output > 0.), y_batch), 'float32')
    loss = T.cast(T.sum(
        (nz.dimshuffle([0, 'x', 1, 2, 3]) * errors)) / nnz, 'float32')
    return loss
