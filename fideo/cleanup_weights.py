"""
Script to clean up checkpoint directories.
Use with `python -m fideo.cleanup_weights`.
"""

#!/usr/bin/env python

import sys
import imp

import argparse
import numpy as np
import humanize

from spath import Path


def get_itr(fname):
    basename = fname.basename().replace('.pkl.gz', '').replace('.npz', '')
    return int(basename.split('_')[1])


def batch(args):
    """ delete some history files """

    if args.ext[0]=='.':
        args.ext = args.ext[1:]

    pattern = 'weights_*.' + args.ext

    base_dir = Path(args.base_dir)
    if not base_dir.exists():
        print("{} doesn't exist".format(base_dir))
        sys.exit(1)

    for d in base_dir.walkdirs():
        if len(d.files(pattern)) > 0:
            print('dir: {}'.format(d))
            _cleanup(d,
                     args.move_to,
                     args.first,
                     args.last,
                     args.nth,
                     args.yes,
                     args.ext)


def cleanup(args):
    weights_dir = Path(args.weights_dir)
    move_to = args.move_to
    first = args.first
    last = args.last
    nth = args.nth
    yes = args.yes
    ext = args.ext
    _cleanup(weights_dir,
             move_to,
             first,
             last,
             nth,
             yes,
             ext)


def _cleanup(weights_dir,
             move_to,
             first,
             last,
             nth,
             yes,
             ext):
    """ delete some history files """

    if ext[0] == '.':
        ext = ext[1:]
    pattern = 'weights_*.' + ext

    weights_dir = Path(weights_dir)
    if not weights_dir.exists():
        print("{} doesn't exist".format(weights_dir))
        sys.exit()

    weight_fnames = sorted([(get_itr(fn),fn) for fn in weights_dir.files(pattern)])
    #print weight_fnames
    print('input weight_fnames: {}'.format(len(weight_fnames)))

    first_keep = set(weight_fnames[:first])
    last_keep = set(weight_fnames[-last:])
    mid_keep = set(weight_fnames[first:-last:nth])
    print('first: {}, last: {}, mid: {}'.format( len(first_keep), len(last_keep), len(mid_keep) ))
    keep_weight_fnames = first_keep.union(last_keep).union(mid_keep)
    print('total to keep: {}'.format(len(keep_weight_fnames)))

    #del_weight_fnames = weight_fnames[first:-last]
    del_weight_fnames = sorted(list(set(weight_fnames).difference(keep_weight_fnames)))

    total_size = sum([tup[1].size for tup in del_weight_fnames])

    print('to move/delete: {}, size: {}'.format(len(del_weight_fnames), humanize.naturalsize(total_size)))
    #print del_weight_fnames
    yn = raw_input('proceed (y/n)? ')
    if yn.lower() != 'y':
        print('skipping')
        return

    if move_to is not None:
        move_to = Path(move_to)
        move_to.makedirs_p()

    for itr_ctr,weight_fname in del_weight_fnames:
        if move_to is not None:
            newfname = move_to/(weight_fname.basename())
            print('{} -> {}'.format(weight_fname, newfname))
            weight_fname.move(newfname)
        else:
            print('rm {}'.format(weight_fname))
            weight_fname.remove()
    print('done')


if __name__=='__main__':
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers()
    batch_parser = subparsers.add_parser('batch')
    batch_parser.add_argument('base_dir', type=str)
    batch_parser.add_argument('--move-to', type=str, help='dir to move to instead of deleting')
    batch_parser.add_argument('--first', type=int, default=4, help='first to keep')
    batch_parser.add_argument('--last', type=int, default=4, help='last to keep')
    batch_parser.add_argument('--nth', type=int, default=4, help='every nth to keep')
    batch_parser.add_argument('--yes', action='store_true', help='no confirmation')
    batch_parser.add_argument('--ext', default='.pkl.gz')
    batch_parser.set_defaults(func=batch)

    cleanup_parser = subparsers.add_parser('cleanup')
    cleanup_parser.add_argument('weights_dir', type=str)
    cleanup_parser.add_argument('--move-to', type=str, help='dir to move to instead of deleting')
    cleanup_parser.add_argument('--first', type=int, default=4, help='first to keep')
    cleanup_parser.add_argument('--last', type=int, default=4, help='last to keep')
    cleanup_parser.add_argument('--nth', type=int, default=4, help='every nth to keep')
    cleanup_parser.add_argument('--yes', action='store_true', help='no confirmation')
    cleanup_parser.add_argument('--ext', default='.pkl.gz')
    cleanup_parser.set_defaults(func=cleanup)

    args = parser.parse_args()
    args.func(args)
