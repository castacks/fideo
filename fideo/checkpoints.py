""" Weight checkpoints.

Our checkpoint format for weights is a gzipped pickled ordered dictionary of
numpy arrays.  Why not hdf5? It introduces an additional dependency and isn't
significantly faster or more space-efficient.  Why not numpy npz? Because it's
not good at preserving non-array objects.
"""

import time
import logging
import cPickle as pickle
import gzip

from spath import Path

log = logging.getLogger(__name__)


class CheckpointDir(object):
    """ A directory populated with various checkpoint files (gzipped pickle).
    """

    def __init__(self, base_dir, mkdir=True, max_checkpoints=40):
        """
        :parameters:
            - base_dir : Path or string
                Directory where checkpoints are stored.
            - mkdir: bool
                Creates directory if it does not exist.
        """

        self.base_dir = Path(base_dir)/'checkpoints'

        self.max_checkpoints = max_checkpoints
        if mkdir and not self.base_dir.exists():
            log.info('Making checkpoint directory {}'.format(self.base_dir))
            self.base_dir.makedirs_p()
        self.cleanup()

    def cleanup(self):
        # TODO policy on mtime
        checkpts = self.find_checkpoint_fnames(sort_by_mtime=True)
        if len(checkpts) > self.max_checkpoints:
            to_delete = checkpts[:len(checkpts)-self.max_checkpoints]
            for checkpt in to_delete:
                log.info('removing old checkpoint %s', checkpt)
                checkpt.remove()

    def save(self, weights, state={}, overwrite=True):
        """
        :parameters:
            - weights : dict or OrderedDict
                Dictionary of name -> tensors.
            - state : dict
                Dictionary containing other training state.
            - overwrite : bool
                Whether to overwrite existing checkpoints.
        """

        itr_ctr = int(state.get('itr_ctr', 0))
        weights_fname = self.base_dir/('weights_{:07d}.pkl.gz'.format(itr_ctr))

        if not overwrite and weights_fname.exists():
            raise IOError('{} exists, not overwriting'.format(weights_fname))

        log.info('checkpointing to {}'.format(weights_fname))

        save_dict = {}
        save_dict.update(weights)
        save_dict['state'] = state
        save_dict['_timestamp'] = float(time.time())

        with gzip.open(weights_fname, 'wb') as f:
            pickle.dump(save_dict, f, protocol=pickle.HIGHEST_PROTOCOL)

    def find_checkpoint_fnames(self, sort_by_mtime=False):
        """ get all checkpoints in directory.
        :parameters:
            - sort_by_mtime: bool
                Sort by modification time. Otherwise, sorts by name.
        """
        weights_fnames = self.base_dir.files('weights_*.pkl.gz')
        if sort_by_mtime:
            key = lambda x: x.mtime
        else:
            key = lambda x:\
                int(x.basename().replace('.pkl.gz', '').replace('weights_', ''))
        return sorted(weights_fnames, key=key)

    def last_checkpoint_fname(self, sort_by_mtime=False):
        """ Find (but not load) most recent checkpoint file.
        Sorts by itr_ctr in filename; if use_mtime is True, uses modified time.
        """
        log.info('searching for latest weights filename in {}'
                 .format(self.base_dir))
        weight_fnames = self.find_checkpoint_fnames(
                sort_by_mtime=sort_by_mtime)

        if len(weight_fnames) == 0:
            raise IOError('No weight files found in {}'.format(self.base_dir))
        return weight_fnames[-1]

    def load_latest(self, sort_by_mtime=False):
        """ Load the most recent checkpoint. """
        weights_fname = self.last_checkpoint_fname(sort_by_mtime=sort_by_mtime)
        log.info('loading weights from {}'.format(weights_fname))
        with gzip.open(weights_fname, 'rb') as f:
            out_dict = pickle.load(f)
        return out_dict

    @staticmethod
    def load_fname(weights_fname):
        with gzip.open(weights_fname, 'rb') as f:
            out_dict = pickle.load(f)
        # for naked files
        if 'weights' not in out_dict:
            return {'weights': out_dict, 'state': {'itr_ctr': 0}}
        return out_dict
