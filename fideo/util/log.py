""" Convenience functions for logging.
"""

__all__ = ['logging_config',
           'get_logger']


import sys
import logging


def logging_config(log_fname=None, level=logging.INFO):
    """ Simple default config for logging.
    By default, log to console. If log_fname is
    given, statements are logged to log_fname as well.

    :parameters:
        - log_fname: string
            filename to store log

    :todo:
        - support for file handles
    """

    # logging.basicConfig(level=logging.DEBUG)
    logger = logging.getLogger()
    logger.propagate = False
    logger.setLevel(level)

    for handler in logger.handlers:
        logger.removeHandler(handler)

    human_log_fmt = logging.Formatter('%(asctime)s|%(name)s|%(levelname)s: %(message)s')
    ch = logging.StreamHandler(sys.stdout)
    ch.setFormatter(human_log_fmt)
    logger.addHandler(ch)

    computer_log_fmt = logging.Formatter('%(created)s|%(asctime)s|%(name)s|%(pathname)s|%(lineno)d|%(levelname)s|%(message)s')
    if log_fname is not None:
        fh = logging.FileHandler(log_fname)
        fh.setFormatter(computer_log_fmt)
        logger.addHandler(fh)


def get_logger(name):
    """ convenience function to save an import stmt.
    """
    return logging.getLogger(name)
