from __future__ import print_function

__all__ = ['OneShotTimer',
           'AggregatedTimer']


import time
from collections import defaultdict
from collections import OrderedDict

import numpy as np
import tabulate


class OneShotTimer(object):
    """
    >>> with OneShotTimer() as timer:
    ...     time.sleep(0.2)
    >>> abs(timer.elapsed - 0.2) < 1e-2
    True
    """
    def __init__(self, name='', printfn=None, callback=None):
        self.name = name
        self.printfn = printfn
        self.callback = callback

    def tic(self):
        self._tic = time.time()
        return self

    def toc(self):
        self._toc = time.time()
        self.elapsed = (self._toc - self._tic)
        if self.printfn is not None:
            self.printfn('%s (s): %f' % (self.name, self.elapsed))
        if self.callback is not None:
            self.callback(self.name, self.elapsed)

    def __enter__(self):
        return self.tic()

    def __exit__(self, exctype, excvalue, traceback):
        self.toc()


class AggregatedTimer(object):
    """
    >>> agg = AggregatedTimer()
    >>> with agg.timer('foo'):
    ...     time.sleep(0.2)
    >>> with agg.timer('foo'):
    ...     time.sleep(0.1)
    >>> with agg.timer('bar'):
    ...     time.sleep(0.4)
    >>> _ = agg.report()
    """

    def __init__(self):
        self.times = defaultdict(lambda: list())

    def _callback(self, name, elapsed):
        self.times[name].append(elapsed)

    def timer(self, name):
        return OneShotTimer(name,
                            printfn=None,
                            callback=self._callback)

    def report(self, printfn=print):
        rows = []
        for name, tlist in self.times.iteritems():
            ts = np.array(tlist)
            entry = OrderedDict()
            entry['name'] = name
            for stat in ['mean', 'median', 'std', 'min', 'max']:
                entry[stat] = getattr(np, stat)(ts)
            rows.append(entry)

        if printfn is not None:
            tab = tabulate.tabulate(rows, headers='keys')
            printfn(tab)
        return rows


if __name__ == "__main__":
    import doctest
    doctest.testmod(verbose=True)
