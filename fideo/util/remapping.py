
from PIL import Image
import numpy as np


def label_image_to_rgb_image(label_img,
                             label_mapping,
                             default=[255, 255, 255]):
    for k, v in label_mapping.iteritems():
        label_mapping[k] = np.asarray(v)
    label_tab = np.empty((np.max(label_mapping.keys())+1, 3),
                         dtype='u1')
    label_tab[:, :] = default
    for lbl_src, lbl_dst in label_mapping.items():
        label_tab[lbl_src] = lbl_dst
    return label_tab[label_img]


def remap_labels(label_img, label_mapping, default=-1):
    label_tab = np.empty((np.max(label_mapping.keys())+1), dtype='int64')
    label_tab.fill(default)
    for lbl_src, lbl_dst in label_mapping.items():
        label_tab[lbl_src] = lbl_dst
    return label_tab[label_img]


def rgb_image_to_label_image(img, mapping, default=0):
    """
    img: HxWx3 uint8 image
    mapping: dictionary of (r,g,b) : int
    default: label to use for keys not in mapping
    """
    if img.ndim != 3:
        raise ValueError('img must be HxWx3 matrix')
    if img.shape[2] != 3:
        raise ValueError('img must be HxWx3 matrix')

    img2 = img.astype('uint32')
    imgh = np.bitwise_or(np.bitwise_or((img2[:, :, 0] << 16),
                                       (img2[:, :, 1] << 8)),
                         (img2[:, :, 2]))
    # 16777216 == 255*(2**16) + 255*(2**8) + 255 + 1 == 256**3
    rgb_tab = np.zeros(16777216, dtype='uint32')
    if default != 0:
        rgb_tab.fill(default)
    for k, v in mapping.items():
        hk = k[0]*(2**16) + k[1]*(2**8) + k[2]
        rgb_tab[hk] = v
    # limg2 = rgb_tab[imgh]
    limg2 = np.take(rgb_tab, imgh)
    return limg2


def add_color_palette(img, class_id_to_rgb):
    """ add color palette to 8-bit PIL image.
    class_id_to_rgb colors must be in (0, 255) range.
    can be list or dict.
    """
    if not isinstance(img, Image.Image):
        img = Image.fromarray(img)
        if img.mode == 'RGB':
            raise ValueError('Invalid image mode (RGB)')

    if (isinstance(class_id_to_rgb, dict) or
        isinstance(class_id_to_rgb, OrderedDict)):
        class_id_to_rgb_lst = []
        keys = sorted(class_id_to_rgb.keys())
        max_key = np.max(keys)
        for k in xrange(max_key+1):
            rgb = class_id_to_rgb.get(k, (0, 0, 0))
            class_id_to_rgb_lst.append(rgb)
    else:
        class_id_to_rgb_lst = class_id_to_rgb

    class_id_to_rgb_lst_flat = []
    for rgb in class_id_to_rgb_lst:
        class_id_to_rgb_lst_flat.extend(list(rgb))

    img.putpalette(class_id_to_rgb_lst_flat)
    return img
