__all__ = ['make_shared',
           'LayerFactory']

import copy
import numpy as np
import theano
from theano.printing import Print


def make_shared(ndim, dtype, name):
    shp = tuple([1] * ndim)
    return theano.shared(np.zeros(shp, dtype=dtype), name=name)


def theano_print(var, msg):
    return Print(msg)(var)


class LayerFactory(object):
    """ Helper to make layers, keeping some parameters fixed.
    """
    def __init__(self, layercls, **kwargs):
        self.layercls = layercls
        self.kwargs = kwargs

    def __call__(self, *args, **kwargs):
        kwargs2 = copy.deepcopy(self.kwargs)
        kwargs2.update(kwargs)
        layer = self.layercls(*args, **kwargs2)
        return layer
