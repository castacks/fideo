import itertools

from PIL import Image
import numpy as np

# good enough for most purposes
ALEXNET_MEAN_BGR = np.array([104.00653839, 116.66886139, 122.67784119], dtype='f4')


def unpreprocess(imgpp):
    """ Approximate inverse of typical nnet preprocessing step.
    given float32 bgr image shaped as (3, height, width), centered
    around alexnet mean bgr, return uint8 rgb image shaped as (height, width, 3).
    """
    if imgpp.ndim != 3:
        raise ValueError('imgpp must have shape (3, height, width)')
    if imgpp.shape[0] != 3:
        raise ValueError('imgpp must have shape (3, height, width)')
    if imgpp.dtype not in (np.float32, np.float64):
        raise ValueError('imgpp must have float type')
    imgpp2 = imgpp.copy()
    imgpp2 += ALEXNET_MEAN_BGR[:, None, None]
    imgpp2 = imgpp2.clip(0, 255).astype('u1')
    imgpp2 = imgpp2.transpose(1, 2, 0)
    imgpp2 = imgpp2[:, :, ::-1]  # swap r and b
    return imgpp2


def make_image_label_montage(xviz, yviz, yhat_viz):
    xviz = Image.fromarray(xviz)
    yviz = Image.fromarray(yviz)
    yhat_viz = Image.fromarray(yhat_viz)

    # resize if necessary
    if yviz.size != xviz.size:
        yviz = yviz.resize(xviz.size, Image.NEAREST)

    if yhat_viz.size != xviz.size:
        yhat_viz = yhat_viz.resize(xviz.size, Image.NEAREST)

    # blendy blends
    yviz_blend = Image.blend(yviz, xviz, 0.5)
    yhat_viz_blend = Image.blend(yhat_viz, xviz, 0.5)

    # without blend row
    xyviz0 = pil_vstack((xviz, yviz, yhat_viz))
    # with blend row
    xyviz1 = pil_vstack((xviz, yviz_blend, yhat_viz_blend))
    # cat downwards
    xyviz = pil_hstack((xyviz0, xyviz1))
    return xyviz


def normal_to_color_image(normal_img):
    """ Image of surface normals, with R=x, G=y, B=z. """
    assert(normal_img.ndim == 3)
    assert(normal_img.shape[0] == 3 or normal_img.shape[2] == 3)
    if normal_img.shape[0] == 3:
        normal_img = normal_img.transpose((1, 2, 0))
    # invalid = np.all(normal_img == 0., axis=2, keepdims=True)
    # normalize
    norms = np.sqrt((normal_img**2).sum(2, keepdims=True))+1e-6
    normal_img = normal_img/norms
    # scale from -1, 1 to 0, 255
    normal_img = (((normal_img/2.) + 0.5)*255).clip(0, 255).astype(np.uint8)
    return normal_img


def log_depth_to_color_image(depth_img, min_range=1., max_range=200.):
    """ Colorized image of log_10(depth_img).
    """
    import matplotlib as mpl
    import matplotlib.pyplot as pl

    log_min = np.log10(min_range)
    log_max = np.log10(max_range)

    depth_img = depth_img.squeeze()
    norm = mpl.colors.Normalize(vmin=log_min, vmax=log_max, clip=True)
    cmap = mpl.cm.jet
    smap = pl.cm.ScalarMappable(norm=norm, cmap=cmap)
    dimgs = (smap.to_rgba(depth_img)[:, :, :3]*255).astype(np.uint8)
    # Image.blend(Image.fromarray(img), Image.fromarray(dimgs), .5)
    return dimgs


def scalar_to_color_figure(depth_img, colorbar=True, **kwargs):
    """ A colorized scalar image using matplotlib.
    """
    import matplotlib.pyplot as pl

    fig = pl.figure()
    gca = pl.gca()
    gca.axes.get_xaxis().set_visible(False)
    gca.axes.get_yaxis().set_visible(False)
    # if depth_img.ndim==4:
    #     depth_img = depth_img[0]
    pl.imshow(depth_img, interpolation='nearest', **kwargs)
    if colorbar:
        pl.colorbar()
    return fig


def plot_cm(cm, classes, title=None, normalize=False,
            ax=None, figsize=None, title_fontsize=u'large',
            text_fontsize=u'medium'):
    """ adapted from scikit-plot
    https://github.com/reiinakano/scikit-plot """
    import matplotlib.pyplot as pl
    if ax is None:
        fig, ax = pl.subplots(1, 1, figsize=figsize)
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        cm = np.around(cm, decimals=2)

    if title:
        ax.set_title(title, fontsize=title_fontsize)
    elif normalize:
        ax.set_title('Normalized Confusion Matrix', fontsize=title_fontsize)
    else:
        ax.set_title('Confusion Matrix', fontsize=title_fontsize)

    image = ax.imshow(cm, interpolation='nearest', cmap=pl.cm.Blues)
    pl.colorbar(mappable=image)
    tick_marks = np.arange(len(classes))
    ax.set_xticks(tick_marks)
    ax.set_xticklabels(classes, fontsize=text_fontsize)
    ax.set_yticks(tick_marks)
    ax.set_yticklabels(classes, fontsize=text_fontsize)

    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        ax.text(j, i, cm[i, j],
                horizontalalignment="center",
                verticalalignment="center",
                fontsize=text_fontsize,
                color="white" if cm[i, j] > thresh else "black")

    ax.set_ylabel('True label', fontsize=text_fontsize)
    ax.set_xlabel('Predicted label', fontsize=text_fontsize)


def pil_hstack(images):
    """ stack PIL images downwards.
    """
    widths, heights = zip(*[img.size for img in images])

    out_w = max(widths)
    out_h = sum(heights)
    out = Image.new(images[0].mode, (out_w, out_h))
    cum_h = 0
    for h, img in zip(heights, images):
        out.paste(img, (0, cum_h))
        cum_h += h
    return out


def pil_vstack(images):
    """ stack PIL images sideways.
    """
    widths, heights = zip(*[img.size for img in images])
    out_w = sum(widths)
    out_h = max(heights)
    out = Image.new(images[0].mode, (out_w, out_h))
    cum_w = 0
    for w, img in zip(widths, images):
        out.paste(img, (cum_w, 0))
        cum_w += w
    return out
