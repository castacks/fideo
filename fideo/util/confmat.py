"""
Utilities for computing confusion matrices of 2D outputs.
"""

from collections import OrderedDict

import numpy as np


def compute_confmat(gt, pred, num_classes, dtype='f8'):
    """
    gt, pred shapes are HxW or BxHxW (no channels)
    By definition a confusion matrix :math:`C` is such that :math:`C_{i, j}`
    is equal to the number of observations known to be in group :math:`i` but
    predicted to be in group :math:`j`.
    Assumption: classes are integers in [0, num_classes)
    """
    assert((gt < num_classes).all() and (pred < num_classes).all())
    assert((gt >= 0).all() and (pred >= 0).all())
    assert(gt.shape == pred.shape)
    assert(gt.ndim == 2 or gt.ndim == 3)
    ix = (num_classes*gt + pred).flatten()
    counts = np.bincount(ix, minlength=num_classes*num_classes)
    cm = counts.reshape((num_classes, num_classes)).astype(dtype)
    return cm


def compute_confmat2(gt, pred, num_classes, dtype='f8'):
    """
    gt, pred shapes are HxW or BxHxW (no channels)
    By definition a confusion matrix :math:`C` is such that :math:`C_{i, j}`
    is equal to the number of observations known to be in group :math:`i` but
    predicted to be in group :math:`j`.
    Assumption: classes are integers in [0, num_classes)
    """
    assert((gt < num_classes).all() and (pred < num_classes).all())
    assert((gt >= 0).all() and (pred >= 0).all())
    assert(gt.shape == pred.shape)
    assert(gt.ndim == 2 or gt.ndim == 3)

    gtf = gt.flatten()
    predf = pred.flatten()

    cm = np.zeros((num_classes, num_classes), dtype=dtype)
    for k in range(len(gtf)):
        i = gtf[k]
        j = predf[k]
        cm[i, j] += 1
    return cm



def metrics_from_confmat(cm, masked):
    """
    cm: confusion matrix.
    masked: whether 0 means unknown/void.

    ---

    n_cl = num of classes in GT
    n_ij = num of pixels of class i predicted to belong to class j
    t_i = total number of pixels of class i in GT

    * pixel acc.
    (sum_i n_ii) / (sum_i t_i)

    * mean acc.
    (1/n_cl) * (sum_i (n_ii/t_i))

    * mean iu
    (1/n_cl) * sum_i [ n_ii / (t_i + (sum_j n_ji) - n_ii) ]

    * freq weighted iu
    1/(sum_k t_k) * sum_i[(t_i * n_ii)/(t_i + (sum_j n_ji) - n_ii)]

    """

    if masked:
        offset = 1
    else:
        offset = 0

    rows = []
    total = cm[offset:, offset:].sum()
    for k in range(offset, cm.shape[0]):

        # 'binarize' cm
        tp = cm[k, k]

        # false positives: predicted as k but not k
        fp = cm[offset:, k].sum() - cm[k, k]

        # false negatives: actually k but not predicted as k
        fn = cm[k, offset:].sum() - cm[k, k]

        # true negatives: not k and not predicted as k
        tn = total - (tp + fp + fn)

        if tp + fp + fn == 0:
            iou = 0.
        else:
            iou = float(tp)/float(tp + fp + fn)

        if tp + fn == 0:
            recall = 0.
        else:
            recall = float(tp)/(tp + fn)

        if tp + fp == 0:
            precision = 0.
        else:
            precision = float(tp)/(tp + fp)

        if tp + tn + fp + fn == 0:
            acc = 0.
        else:
            acc = float(tp + tn)/(tp + tn + fp + fn)

        support = cm[k, offset:].sum()

        row = OrderedDict()
        row['class'] = k
        row['iou'] = iou
        row['precision'] = precision
        row['recall'] = recall
        row['acc'] = acc
        row['support'] = support
        rows.append(row)
    return rows


def metrics_from_confmat2(cm, masked):
    """
    cm: confusion matrix.
    masked: whether 0 means unknown/void.

    ---

    n_cl = num of classes in GT
    n_ij = num of pixels of class i predicted to belong to class j
    t_i = total number of pixels of class i in GT

    * pixel acc.
    (sum_i n_ii) / (sum_i t_i)

    * mean acc.
    (1/n_cl) * (sum_i (n_ii/t_i))

    * mean iu
    (1/n_cl) * sum_i [ n_ii / (t_i + (sum_j n_ji) - n_ii) ]

    * freq weighted iu
    1/(sum_k t_k) * sum_i[(t_i * n_ii)/(t_i + (sum_j n_ji) - n_ii)]

    """

    if masked:
        offset = 1
    else:
        offset = 0

    rows = []
    total = cm[offset:, offset:].sum()
    for k in range(masked, cm.shape[0]):

        # 'binarize' cm
        tp = cm[k, k]

        # false positives: predicted as k but not k
        fp = cm[offset:, k].sum() - cm[k, k]

        # false negatives: actually k but not predicted as k
        fn = cm[k, offset:].sum() - cm[k, k]

        # true negatives: not k and not predicted as k
        tn = total - (tp + fp + fn)

        if tp + fp + fn == 0:
            iou = 0.
        else:
            iou = float(tp)/float(tp + fp + fn)

        if tp + fn == 0:
            recall = 0.
        else:
            recall = float(tp)/(tp + fn)

        if tp + fp == 0:
            precision = 0.
        else:
            precision = float(tp)/(tp + fp)

        if tp + tn + fp + fn == 0:
            acc = 0.
        else:
            acc = float(tp + tn)/(tp + tn + fp + fn)

        support = cm[k, offset:].sum()

        row = OrderedDict()
        row['class'] = k
        row['iou'] = iou
        row['precision'] = precision
        row['recall'] = recall
        row['acc'] = acc
        row['support'] = support
        rows.append(row)
    return rows
