
__all__ = ['SupervisedModel']


from collections import OrderedDict

import theano
import lasagne

from funcy import cached_property

from ..validators import ChunkedValidator
from ..trainers import ChunkedTrainer


class SupervisedModel(object):
    """
    Model holds network topology and loss, including regularization.
    Holds theano variables.
    Provides output expression.
    Provides updates expression.
    """

    def __init__(self, net, mode):
        self.net = net
        self.mode = mode

    @cached_property
    def compiled_out(self):
        return theano.function([self.x_var], self.out_expr)

    @cached_property
    def compiled_dout(self):
        return theano.function([self.x_var], self.dout_expr)

    @cached_property
    def compiled_dump(self):
        dumpdict = {}
        for layer in self.net.get_all_layers():
            dumpdict[layer.name] = lasagne.layers.get_output(layer, self.x_var, deterministic=True)
        out = theano.function([self.x_var], dumpdict)

        def ordered_dump(x):
            unordered = out(x)
            ordered = OrderedDict()
            for k in self.net.layers.keys():
                ordered[k] = unordered[k]
            return ordered
        return ordered_dump

    def weights_from_checkpoint(self, checkpt):
        weights = checkpt['weights']
        self.net.set_weights(weights)

    def weights_to_checkpoint(self):
        return {'weights': self.net.get_weights()}

    def dump_info(self, info_cb):
        info_cb('net_tbl_ascii', self.net.repr_net_table(), console=True)
        info_cb('net_tbl', self.net.repr_net_table(tablefmt=None), console=False)
        info_cb('num_params', self.net.count_params(), console=True)

    def build_validator(self, x_key, y_key):
        """
        validator: needs model, data source (w/ labels)
        """
        valid_inputs = {x_key: self.x_var, y_key: self.y_var}
        valid_outputs = {'dout': self.dout_expr, 'y': self.y_var}
        validator = ChunkedValidator(valid_inputs=valid_inputs,
                                     valid_outputs=valid_outputs)
        return validator

    def build_trainer(self, updater, x_key, y_key, batch_size, batches_per_chunk):
        # needs updater
        train_inputs = {x_key: self.x_var, y_key: self.y_var}

        train_outputs = {'obj_loss': self.losses['obj_loss'],
                         'reg_loss': self.losses['reg_loss'],
                         'loss': self.losses['loss']}

        updates = updater(self.losses['loss'], self.net)

        trainer = ChunkedTrainer(
                   batch_size=batch_size,
                   batches_per_chunk=batches_per_chunk,
                   train_inputs=train_inputs,
                   train_outputs=train_outputs,
                   train_updates=updates)

        return trainer
