
__all__ = ['Classification2d']


from collections import OrderedDict

import theano.tensor as T
from lasagne import layers
from lasagne import regularization
from lasagne import objectives
from funcy import cached_property

from .supervised import SupervisedModel


class Classification2d(SupervisedModel):
    """ 1-of-K classification, input 2D
    """

    def __init__(self,
                 net,
                 weight_decay):
        super(Classification2d, self).__init__(self, net)

        self.net = net

        self.weight_decay = weight_decay
        self._init_theano()

    def _init_theano(self):
        """
        Init theano variables.
        """

        self.x_var = T.ftensor4('x')
        self.y_var = T.fmatrix('y')  # B x 1 matrix

        self.out_expr = layers.get_output(self.net.output_layer, self.x_var)
        self.dout_expr = layers.get_output(self.net.output_layer, self.x_var, deterministic=True)

    def _get_losses_dict(self):
        if self.num_classes == 1:
            out_loss = objectives.aggregate(
                    objectives.binary_crossentropy(
                        self.out_expr, T.cast(self.y_var, 'int32').flatten()))
        else:
            out_loss = objectives.aggregate(
                    objectives.categorical_crossentropy(
                        self.out_expr, T.cast(self.y_var, 'int32').flatten()))
            # out_loss = fideo.expr.log_softmax(self.out_expr, T.cast(self.y_var, 'int32'))
            # out_loss = T.nnet.categorical_crossentropy(self.out_expr, T.cast(self.y_var.squeeze(), 'int32')).mean()

        l2_norm = regularization.regularize_network_params(self.net.output_layer,
                                                           regularization.l2)
        loss_dict = OrderedDict()
        loss_dict['ce'] = (1.0, out_loss)
        loss_dict['l2'] = (self.regularization_weight, l2_norm)
        return loss_dict

    @cached_property
    def accuracy(self):
        if self.num_classes == 1:
            acc = objectives.aggregate(
                    objectives.binary_accuracy(
                        self.out_expr, T.cast(self.y_var, 'int32').flatten()))
        else:
            acc = objectives.aggregate(
                    objectives.categorical_accuracy(
                        self.out_expr, T.cast(self.y_var, 'int32').flatten()))
        return acc
