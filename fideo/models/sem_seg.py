
__all__ = ['SemanticSegmentation2d']


from collections import OrderedDict

import theano.tensor as T

import lasagne
from lasagne import layers
from lasagne import regularization
from funcy import cached_property

from ..expr2d import accuracy_2d
from ..expr2d import binary_accuracy_2d
from ..expr2d import logspace_binary_logloss
from ..expr2d import logspace_logloss_2d_stable

from ..expr2d import masked_accuracy_2d
from ..expr2d import masked_logspace_binary_logloss
from ..expr2d import masked_logspace_logloss_2d_stable

from .supervised import SupervisedModel


class SemanticSegmentation2d(SupervisedModel):
    """
    1-of-K pixelwise classification, input 2D, output 2D.
    NOTE: if you say 1 class, different loss
    function will be used. It will be binary cross entropy.
    This is theoretically same as 2-class multiclass CE
    but might be faster.

    """

    def __init__(self,
                 net,
                 mode,
                 num_classes=1,
                 masked=False,
                 weight_decay=0.):
        super(SemanticSegmentation2d, self).__init__(net, mode)

        self.num_classes = num_classes
        self.masked = masked
        self.weight_decay = weight_decay

        self._init_theano()

        if self.mode == 'train':
            self.losses = self._build_losses()

    def _init_theano(self):
        """
        Init theano variables.
        """

        self.x_var = T.ftensor4('x')
        # self.y = T.itensor4('y')
        self.y_var = T.ftensor4('y')

        self.out_expr = layers.get_output(self.net.output_layer, self.x_var)
        self.dout_expr = layers.get_output(self.net.output_layer, self.x_var, deterministic=True)

    def _build_losses(self):
        # depends on num classes, masked, weight_decay
        if self.num_classes == 1:
            if self.masked:
                out_loss = masked_logspace_binary_logloss(self.out_expr, T.cast(self.y_var, 'int32'))
            else:
                out_loss = logspace_binary_logloss(self.out_expr, T.cast(self.y_var, 'int32'))
        else:
            if self.masked:
                out_loss = masked_logspace_logloss_2d_stable(self.out_expr, T.cast(self.y_var, 'int32'))
            else:
                out_loss = logspace_logloss_2d_stable(self.out_expr, T.cast(self.y_var, 'int32'))

        l2_norm = regularization.regularize_network_params(
                self.net.output_layer, lasagne.regularization.l2)

        loss_dict = OrderedDict()
        loss_dict['obj_loss'] = out_loss
        loss_dict['reg_loss'] = self.weight_decay*l2_norm
        loss_dict['loss'] = loss_dict['obj_loss'] + loss_dict['reg_loss']
        return loss_dict

    @cached_property
    def accuracy(self):
        if self.num_classes == 1:
            if self.masked:
                raise Exception('no binary masked accuracy')
            else:
                acc = binary_accuracy_2d(self.out_expr, T.cast(self.y_var, 'int32'))
        else:
            if self.masked:
                acc = masked_accuracy_2d(self.out_expr, T.cast(self.y_var, 'int32'))
            else:
                acc = accuracy_2d(self.out_expr, T.cast(self.y_var, 'int32'))
        return acc
