"""
Some theano expressions.
"""

import theano.tensor as T


def rms(x, axis=None, epsilon=1e-12):
    """ Root Mean Square.
    """
    return T.sqrt(T.mean(T.sqr(x), axis=axis) + epsilon)


def line_abc_sqr_error(yhat, y, scale_err_c=1/227.):
    """ Squared error for a 3-vector (a, b, c),
    with scaling parameter for c.
    """
    ac = yhat[:, 0]
    bc = yhat[:, 1]
    cc = yhat[:, 2]
    return ((ac-y[:, 0])**2 + (bc-y[:, 1])**2 +
            ((cc-y[:, 2])*scale_err_c)**2).mean()


def line_norm_abc_sqr_error(yhat, y):
    """ Squared error for unitized 3-vector (a, b, c).
    Let ax + by + c = 0.
    Let abnorm = l2norm([a,b])
    then a = a/norm, b = b/norm
    TODO c = c/norm?
    """
    ac = yhat[:, 0]
    bc = yhat[:, 1]
    cc = yhat[:, 2]
    abnorm = T.sqrt(ac**2 + bc**2) + 1e-12
    ac2 = ac/abnorm
    bc2 = bc/abnorm
    return ((ac2-y[:, 0])**2 + (bc2-y[:, 1])**2 +
            ((cc-y[:, 2])/227.)**2).mean()


def log_softmax(x):
    """ softmax in logspace """
    xdev = x - x.max(1, keepdims=True)
    return xdev - T.log(T.sum(T.exp(xdev), axis=1, keepdims=True))


def categorical_crossentropy_logdomain_onehot(log_predictions, targets):
    """ categorical crossentropy in logspace, assuming one-hot vectors.
    """
    return -T.sum(targets * log_predictions, axis=1)
