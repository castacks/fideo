
import numpy as np
import theano

import lasagne


class SgdUpdater(object):
    """ Helper for lasagne.updates.
    algorithm is one of the algorithm names in lasagne.updates
    """

    def __init__(self, algorithm='adam', init_lr=0.0001, norm_constraint=0.,
                 **kwargs):
        try:
            self.update_fun = getattr(lasagne.updates, algorithm)
        except AttributeError:
            raise ValueError('Unknown update algorithm: {}'.format(algorithm))
        self.init_lr = init_lr
        # dynamically adjustable
        self.lr_var = theano.shared(np.float32(init_lr))
        self.norm_constraint = norm_constraint
        self.update_args = kwargs

    def __call__(self, loss, net):
        params = net.get_all_params(trainable=True)
        updates = self.update_fun(loss, params, self.lr_var, **self.update_args)

        if self.norm_constraint > 0.:
            maxnorm_params = net.get_all_params(trainable=True, max_norm=True)
            for param, update in updates.iteritems():
                if param in maxnorm_params:
                    updates[param] = lasagne.updates.norm_constraint(update,
                                                                     self.norm_constraint)
        return updates
