
from spath import Path
import h5py
import click
import numpy as np

@click.command(context_settings={'help_option_names':['-h', '--help']})
@click.argument('src_h5', type=click.Path(exists=True))
@click.argument('dst_npz', type=click.Path(exists=False))
def main(src_h5, dst_npz):
    out_dict = {}
    with h5py.File(src_h5, 'r') as f:
        for k in f.keys():
            val = f[k].value
            click.echo('{} {}'.format(k, val.shape))
            out_dict[k] = val
    np.savez_compressed(dst_npz, **out_dict)

if __name__ == "__main__":
    main()
