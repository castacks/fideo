
from collections import OrderedDict
import cPickle as pickle
import gzip

from spath import Path
import click
import numpy as np


@click.command(context_settings={'help_option_names':['-h', '--help']})
@click.argument('src', type=click.Path(exists=True))
@click.argument('dst', type=click.Path(exists=False))
def main(src, dst):

    with gzip.open(src, 'rb') as f:
        inp = pickle.load(f)

    weights = inp['weights']

    for name, param in weights.iteritems():
        print(name, param.shape)
        if (name.startswith('conv') and
            name.endswith('.W') and
            param.ndim == 4 and
            param.shape[-1] == 1 and
            param.shape[-2] == 1):
            print('changing {}'.format(name))
            ninW = param.squeeze().T
            weights[name] = ninW

    outp = {}
    outp['state'] = inp['state']
    outp['weights'] = weights

    with gzip.open(dst, 'wb') as f:
        pickle.dump(outp, f, protocol=pickle.HIGHEST_PROTOCOL)



if __name__ == "__main__":
    main()
