
from collections import OrderedDict
import cPickle as pickle
import gzip


from spath import Path
import click
import numpy as np


@click.command(context_settings={'help_option_names':['-h', '--help']})
@click.argument('src_npz', type=click.Path(exists=True))
@click.argument('dst_pklgz', type=click.Path(exists=False))
def main(src_npz, dst_pklgz):
    out_dict = OrderedDict()
    params = np.load(src_npz)
    if 'names' in params:
        names = params['names']
    elif 'param_names' in params:
        names = params['param_names']
    else:
        names = params.keys()

    names = map(str, names)
    try:
        names.remove('layer_names')
    except ValueError:
        pass

    od = OrderedDict()
    for name in names:
        click.echo('{}'.format(name))
        od[name] = params[name]

    out = {'weights': od, 'state': { 'itr_ctr': 0 }}
    with gzip.open(dst_pklgz, 'wb') as f:

        pickle.dump(od, f, protocol=pickle.HIGHEST_PROTOCOL)


if __name__ == "__main__":
    main()
