
The easiest way is probably to call whichever update function you wish to use on the parameters of each layer separately, and then merge them. So instead of:

# ...
params = lasagne.layers.get_all_params(l_out)
updates = lasagne.updates.nesterov_momentum(loss, params, learning_rate)
# ...

You would do something like:

# ...
learning_rates = { l1: 0.1, l2: 0.3, l3: 0.15, l4: 0.2, l5: 0.05, ...}
updates = {}
for layer, learning_rate in learning_rates.items():
    updates.update(lasagne.updates.nesterov_momentum(loss, layer.get_params, learning_rate)
# ...

Note that this code is untested. Just to give you an idea :)

#########

If you need even finer control on the level of parameters rather than layers, another way would be to scale the gradients:

params = lasagne.layers.get_all_params(l_out)
grads = theano.grad(loss, params)
for idx, param in enumerate(params):
    grad_scale = ... # obtain multiplier for that parameter in some way
    if grad_scale != 1:
        grads[idx] *= grad_scale
updates = lasagne.updates.nesterov_momentum(grads, params, ...)

You can use whichever way you like for the "obtain multiplier" step -- maintain a dictionary of param -> multiplier, or set param.tag.grad_scale to some value when you build the model (every Theano expression has a tag attribute that can be used freely, e.g., layer.W.tag.grad_scale=.5).

Best, Jan
