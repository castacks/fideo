
import lasagne
import fideo
import fideo.net
from fideo.net_helpers import Namer2 as _
from fideo.net_helpers import BatchNorm as BN

net = fideo.net.Net()

# note layer construction is deferred
net['lin'] = _(lasagne.layers.InputLayer, (None, 3, 10, 10))
net['conv1'] = _(lasagne.layers.Conv2DLayer, net.tail, num_filters=4, filter_size=(3, 3))
net['conv2'] = BN(lasagne.layers.Conv2DLayer(net.tail, num_filters=4, filter_size=(3, 3)), epsilon=0.01)
#net['foo'] = _(lasagne.layers.Conv2DLayer(net.tail, num_filters=4, filter_size=(3, 3)))
