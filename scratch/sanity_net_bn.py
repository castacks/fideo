from __future__ import print_function

import fideo.net

from lasagne.layers import InputLayer
from lasagne.layers import DenseLayer

net = fideo.net.Net()
net.add_layer('lin', InputLayer(shape=(None, 1, 16, 16)))
net.add_layer('dense1', DenseLayer(net.last_layer, num_units=8))
net.add_layer_batch_norm('dense2', DenseLayer(net.last_layer, num_units=8), alpha=0.5)

print()
for k,v in net.items():
    print(k, v)

print(net['dense2+bn'].alpha)
