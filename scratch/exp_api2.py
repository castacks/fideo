

from fideo import TrainerApp

exp_id = str(Path(__file__).basename().stripext())

app = TrainerApp(exp_id)

# @app.build_trainer
def build_trainer(self, model, pipeline_runner, out_dir):
    from fideo.trainers import SgdTrainer
    from fideo.monitoring import TrainingLogger
    from fideo.monitoring import PastalogMonitoring
    from fideo.monitoring import DisplayLabels

    def learning_rate(itr):
        return 0.00005

    trainer = SgdTrainer(model, pipeline_runner, out_dir)
    trainer.reg = 0.0001
    trainer.save_every_nth = 400
    trainer.batch_size = 16
    trainer.batches_per_chunk = 8
    trainer.learning_rate = learning_rate
    trainer.train_metric_every_nth = 16
    trainer.momentum = 0.9
    trainer.norm_constraint = 3.
    trainer.disp_every_nth = 16
    #trainer.init_weights_fname = Path('/mnt/dms/results/mscoco_car_truck_resized/segnet_skip2/weights_420000.npz')
    #trainer.init_weights_fname = Path('/mnt/dms/results/mscoco_car_truck_resized/segnet_skip2b/weights_456000.npz')
    trainer.init_weights_fname = Path('/mnt/dms/results/mscoco_car_truck_resized/segnet_skip2c/weights_000000.npz')
    trainer.debug_callbacks.append(debug_cb1)

    trainer.iter_callbacks.append(TrainingLogger(output_dir/'metrics.msg'))
    trainer.iter_callbacks.append(PastalogMonitoring(exp_id, port=8120))

    trainer.display_callbacks.append(DisplayLabels())
    trainer.theano_init()

    return trainer


def build_net(self, num_classes, x_dims):
    from fideo.model import BaseModel
    import lasagne
    from fideo.lasagne_backports.init import Constant as ConstantInit
    from fideo.lasagne_backports.init import HeNormal as HeNormalInit
    from fideo.lasagne_backports.init import Normal as NormalInit
    from lasagne.layers import ElemwiseSumLayer, ConcatLayer
    from lasagne.layers import InputLayer, DenseLayer, DropoutLayer
    from lasagne.layers import MaxPool2DLayer as PoolLayer
    from lasagne.layers import NINLayer
    from lasagne.layers import NonlinearityLayer
    from lasagne.layers.dnn import Conv2DDNNLayer as ConvLayer
    from fideo.lasagne_backports.nonlinearities import elu
    from lasagne.utils import floatX

    import fideo
    from fideo.layers import SparseUpsample2dLayer

    nb = fideo.NetBuilder()

    M = 64

    nb['input'] = InputLayer((None, 3, 448, 448))
    nb['conv1'] = ConvLayer(nb.last, num_filters=64, filter_size=7, pad=3, W=HeNormalInit(), nonlinearity=elu, name='conv1')
    nb['pool1'] = PoolLayer(nb.last, pool_size=2, stride=2, name='pool1')

    nb['conv2'] = ConvLayer(nb.last, num_filters=64, filter_size=7, pad=3, W=HeNormalInit(), nonlinearity=elu, name='conv2')
    nb['pool2'] = PoolLayer(nb.last, pool_size=2, stride=2, name='pool2')

    nb['conv3'] = ConvLayer(nb.last, num_filters=64, filter_size=7, pad=3, W=HeNormalInit(), nonlinearity=elu, name='conv3')
    nb['pool3'] = PoolLayer(nb.last, pool_size=2, stride=2, name='pool3')

    nb['conv4'] = ConvLayer(nb.last, num_filters=64, filter_size=7, pad=3, W=HeNormalInit(), nonlinearity=elu, name='conv4')
    nb['pool4'] = PoolLayer(nb.last, pool_size=2, stride=2, name='pool4')

    nb['conv5'] = ConvLayer(nb.last, num_filters=64, filter_size=7, pad=3, W=HeNormalInit(), nonlinearity=elu, name='conv5')
    nb['pool5'] = PoolLayer(nb.last, pool_size=2, stride=2, name='pool5')

    nb['conv6'] = ConvLayer(nb.last, num_filters=128, filter_size=3, pad=1, W=HeNormalInit(), nonlinearity=elu, name='conv6')
    nb['pool6'] = PoolLayer(nb.last, pool_size=2, stride=2, name='pool6')

    nb['conv7'] = ConvLayer(nb['pool6'], num_filters=256, filter_size=3, pad=1, W=HeNormalInit(), nonlinearity=elu, name='conv7')
    nb['deconv7'] = ConvLayer(nb['conv7'], num_filters=256, filter_size=3, pad=1, W=HeNormalInit(), nonlinearity=None, name='deconv7')

    nb['nin_out7'] = NINLayer(nb['deconv7'], num_classes, W=HeNormalInit(), nonlinearity=None, name='nin_out7')

    nb['depool6'] = SparseUpsample2dLayer(nb['deconv7'], name='depool6')
    deconv6_W = fideo.layers.misc_layers.upsample_filt([128, 256, 3, 3], normalize=True)
    nb['deconv6'] = ConvLayer(nb['depool6'], num_filters=128, filter_size=3, pad=1, W=deconv6_W, nonlinearity=None, name='deconv6')
    nb['nin_deconv6'] = NINLayer(nb['deconv6'], M, W=HeNormalInit(), nonlinearity=elu, name='nin_deconv6')
    nb['nin_conv6'] = NINLayer(nb['conv6'], M, W=HeNormalInit(), nonlinearity=elu, name='nin_conv6')
    nb['fuse1'] = ElemwiseSumLayer([nb['nin_conv6'], nb['nin_deconv6']], name='fuse1')
    nb['nin_out6'] = NINLayer(nb['fuse1'], num_classes, W=HeNormalInit(), nonlinearity=None, name='nin_out6')


    nb['depool5'] = SparseUpsample2dLayer(nb['deconv6'], name='depool5')
    deconv5_W = fideo.layers.misc_layers.upsample_filt([64, 128, 5, 5], normalize=True)
    nb['deconv5'] = ConvLayer(nb['depool5'], num_filters=64, filter_size=5, pad=2, W=deconv5_W, nonlinearity=None, name='deconv5')
    nb['nin_deconv5'] = NINLayer(nb['deconv5'], M, W=HeNormalInit(), nonlinearity=elu, name='nin_deconv5')
    nb['nin_conv5'] = NINLayer(nb['conv5'], M, W=HeNormalInit(), nonlinearity=elu, name='nin_conv5')
    nb['fuse2'] = ElemwiseSumLayer([nb['nin_conv5'], nb['nin_deconv5']], name='fuse2')
    nb['nin_out5'] = NINLayer(nb['fuse2'], num_classes, W=HeNormalInit(), nonlinearity=None, name='nin_out5')

    #nb['l_in'] = nb['input']
    #nb['l_out'] = nb['nin_out5']

    net = nb.build()

    return net




app.build_trainer = build_trainer

app.run()
