
from fideo.apps.trainers import SupervisedTrainerApp

exp_id = str(Path(__file__).basename().stripext())

app = SupervisedTrainerApp('exp28')

data_base_dir = dirs_helper.get_data_base_dir()
results_dir = dirs_helper.get_results_base_dir()

data_dir = dirs_helper.get_package_path()/'data/cars_8020_train'
results_dir = results_dir/'yt_cars_icra16'
output_dir = results_dir/exp_id

x_key = 'rgb_image'
y_key = 'labels_image'
num_classes = 1
x_dims = (None, 3, 448, 448)
y_dims = (None, num_classes, 28, 28)
yhat_dims = (None, num_classes, 28, 28)
y_dtype = 'float32'


def build_trainer(self, model, pipeline_runner, out_dir):
    from fideo.trainers import SgdTrainer
    from fideo.monitoring import TrainingLogger
    from fideo.monitoring import PastalogMonitoring
    from fideo.monitoring import DisplayLabels

    trainer = SgdTrainer(model, pipeline_runner, out_dir)
    trainer.reg = 0.0001
    trainer.save_every_nth = 400
    trainer.batch_size = 16
    trainer.batches_per_chunk = 8
    trainer.learning_rate = learning_rate
    trainer.train_metric_every_nth = 16
    trainer.momentum = 0.9
    trainer.norm_constraint = 3.
    trainer.disp_every_nth = 16
    #trainer.init_weights_fname = Path('/mnt/dms/results/mscoco_car_truck_resized/segnet_skip2/weights_420000.npz')
    #trainer.init_weights_fname = Path('/mnt/dms/results/mscoco_car_truck_resized/segnet_skip2b/weights_456000.npz')
    trainer.init_weights_fname = Path('/mnt/dms/results/mscoco_car_truck_resized/segnet_skip2c/weights_000000.npz')
    trainer.debug_callbacks.append(debug_cb1)

    trainer.iter_callbacks.append(TrainingLogger(output_dir/'metrics.msg'))
    trainer.iter_callbacks.append(PastalogMonitoring(exp_id, port=8120))

    trainer.display_callbacks.append(DisplayLabels())
    trainer.theano_init()

    return trainer



if __name__ == "__main__":
    app.build_trainer = app.build_trainer
    app.cli()
