
nb = NetBuilder()
nb += ('input', Layer(nb.last))
nb += ('foo1', Layer(nb.last))
nb += ('foo2', Layer(nb.last))
nb += ('sum', Layer(nb['foo1'], nb['foo2']))

nb.batch_normalize('foo1')

nb += ('foo1', BatchNormalize(Layer(nb.last)))

class BatchNormalize(object):
    def __init__(self, layer, **kwargs):
        self.layer = layer
        self.eta = kwargs['eta']

    def __call__(self, name):
        # dot = layer.name
        # bla
        return [layer1, layer2]



def __iadd__(self, other):
    name, maybe_layer = other
    if not isinstance(maybe_layer, Layer):
        layers = maybe_layer()
    for layer in layers:
        self.add_named(layer)


nb = NetBuilder()
nb['input'] = Layer()
nb['foo'] = Layer(nb.last)
nb['bar'] = BatchNormalize(Layer(nb.last))

layers = nb.layers
