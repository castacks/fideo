
import os; os.environ['THEANO_FLAGS'] = 'device=cpu'

import warnings
from collections import OrderedDict

from spath import Path

import parafina.pipeline
from parafina.pipeline import BasePipeline

import fideo
from fideo.trainers import ChunkedTrainer
from fideo.model import Structured2dModel
from fideo.experiment import Experiment

exp_id = str(Path(__file__).basename().stripext())

x_key = 'rgb_image'
y_key = 'labels_image'
num_classes = 1
x_shape = (None, 3, 448, 448)
y_shape = (None, 1, 28, 28)
yhat_shape = (None, num_classes, 28, 28)
y_dtype = 'float32'


class Pipeline(BasePipeline):

    def __init__(self):
        from parafina.gen.instancedir import InstanceDirGen
        from parafina.gen.virt_instancedir import VirtInstanceDirGen
        from parafina.gen.packed_instancedir import PackedInstanceDirGen
        #from mavs_dnn3.pipelines import yt_aerial

        src1 = InstanceDirGen(data_dir,
                              fnames={'rgb_fname' : 'rgb.jpg',
                                      'labels_fname' : 'labels.png'},
                              include_pattern='*',
                              exclude_pattern='metadata',
                              loop=True)

        mode = 'train'
        #pipeline = yt_aerial.build_train_valid_pipeline(mode,
        #                                                x_key,
        #                                                y_key,
        #                                                x_shape,
        #                                                y_shape)

        ops = OrderedDict()

        # in zmq, gets run by ventilator
        self.src = src1
        # in zmq, gets run by aux workers
        self.ops = pipeline




class Trainer(ChunkedTrainer):

    def __init__(self, model, pipeline):
        super(ChunkedTrainer, self).__init__(model, pipeline)

        self.reg = 0.0001
        self.save_every_nth = 400
        self.batch_size = 16
        self.batches_per_chunk = 8
        self.learning_rate = lambda itr: 0.0001
        self.train_metric_every_nth = 16
        self.momentum = 0.9
        self.norm_constraint = 3.
        self.disp_every_nth = 16

        #self.init_weights_fname = Path('/mnt/dms/results/mscoco_car_truck_resized/segnet_skip2/weights_420000.npz')
        #self.init_weights_fname = Path('/mnt/dms/results/mscoco_car_truck_resized/segnet_skip2b/weights_456000.npz')
        self.init_weights_fname = Path('/mnt/dms/results/mscoco_car_truck_resized/segnet_skip2c/weights_000000.npz')


        #stream_chunker = StreamChunker(stream=blob_stream,
        #                               item_keys=['rgb_image', 'labels_image'],
        #                               item_shapes=[x_shape, y_shape],
        #                               item_dtypes=[x_dtype, y_dtype],
        #                               batch_size=batch_size,
        #                               batches_per_chunk=batches_per_chunk)
        #
        #chunked_trainer = ChunkedTrainer(inputs, outputs, updates, batch_size)
        ## chunked_trainer.callbacks()
        #chunk = stream_chunker.next()
        #chunked_trainer.train(chunk)




    def initialize(self):
        super(Trainer, self).initialize(model.inputs,
                                        model.outputs,
                                        model.updates,
                                        self.batch_size)






class Model(Structured2dModel):

    def __init__(self):
        super(Structured2dModel, self).__init__()

        self.num_classes = 1
        self.x_shape = x_shape
        self.y_shape = y_shape
        self.yhat_shape = yhat_shape
        self.y_dtype = y_dtype


    def build_net(self):
        pass


def main():
    experiment = Experiment(exp_id)
    experiment.model = Model()
    experiment.pipeline = Pipeline()
    experiment.trainer = Trainer(model, pipeline)
    experiment.cli()
