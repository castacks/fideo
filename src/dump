#!/usr/bin/env python

import sys
from collections import OrderedDict
import imp

import click
import lasagne
import numpy as np
import theano
import treedict
from spath import Path

import pyml_util

try:
    import pydisplay as disp
except ImportError:
    disp = None

from fideo import monitoring

from fideo import trainers
from parafina.pipeline import (SerialRunner, ZmqRunner2)

#from time import gmtime, strftime

log = pyml_util.get_logger(__name__)


class Application(object):

    def __init__(self, args):
        self.args = treedict.getTree('args')
        self.args.update(args)
        self.args.freeze(structure_only=True)

        self.expt = imp.load_source('expt', self.args['expt_module'])

        model = self.expt.build_model()

        if self.args.debug:
            # override this so memory doesn't run out
            # makes debugging easier
            self.args.workers = 1

        click.echo('making output directory: {}'.format(self.expt.output_dir))
        self.out_dir = Path(self.expt.output_dir).makedirs_p()

        pyml_util.logging_config(self.out_dir/('train.log'))

        self._dump_info()

        #tfunc, tvar = model.make_training_functions(debug=self.args.debug)

        self.data_src, self.data_pipeline = self.expt.build_data_pipeline('train')

        if self.args.workers <= 1:
            self.pipeline_runner = SerialRunner(self.data_src,
                                                self.data_pipeline,
                                                self.expt.x_key,
                                                self.expt.y_key)

        self.trainer = self.expt.build_trainer(model,
                                               self.pipeline_runner,
                                               self.out_dir)


    def _dump_info(self):
        log.info('theano version: {}'.format(theano.__version__))
        log.info('lasagne version: {}'.format(lasagne.__version__))
        log.info('cmd: {}'.format(' '.join(sys.argv)))
        log.info('out dir is {}'.format(self.expt.output_dir))

        # log.info('data dir is {}'.format(self.expt.data_dir))

        #log.info(model.repr_net_shapes())
        #log.info('layer_shape: {}'.format(layer_shape))
        #model.make_net_diagram(fname)


    def run(self):
        if self.args.sanity:
            click.echo(click.style('just sanity check. ending.', fg='red'))
            return
        self.trainer.train()



@click.command()
@click.argument('expt_module')
@click.option('--weights',
              type=click.Path(exists=True),
              help='fname for preinitialization')
@click.option('-s', '--sanity',
              is_flag=True,
              help='just sanity check')
@click.option('-r', '--resume',
              is_flag=True,
              help='resume from checkpoint')
@click.option('-d', '--debug',
              is_flag=True,
              help='debug out')
@click.option('-j', '--workers',
              type=int,
              default=4,
              help='num workers')
@click.option('--scratch',
              is_flag=True)
@click.option('--pastalog',
              is_flag=True,
              help='publish to pastalog')
@click.option('--pastalog-port',
              default=8120,
              help='pastalog port')
@click.option('--queue-size',
              default=200,
              help='queue size for data pipeline')
@click.option('--overwrite',
              default=True,
              help='Overwrite weight files')
def main(**args):
    app = Application(args)
    app.run()


if __name__ == "__main__":
    main()
